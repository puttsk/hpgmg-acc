/* ------------------------------------------------------------------------------------------------------------------------------ */
/* Samuel Williams */
/* SWWilliams@lbl.gov */
/* Lawrence Berkeley National Lab */
/* ------------------------------------------------------------------------------------------------------------------------------ */
/* calculate res_id = rhs_id - A(x_id) */

#ifndef __O2G_HEADER__ 

#define __O2G_HEADER__ 

/*******************************************/
/* Added codes for OpenACC2GPU translation */
/*******************************************/
#include <openacc.h>
#include <openaccrt.h>
#include <math.h>
#include <float.h>
#include <limits.h>
#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#ifdef __cplusplus
#define restrict __restrict__
#endif

/**********************************************************/
/* Maximum width of linear memory bound to texture memory */
/**********************************************************/
/* width in bytes */
#define LMAX_WIDTH    134217728
/**********************************/
/* Maximum memory pitch (in bytes)*/
/**********************************/
#define MAX_PITCH   262144
/****************************************/
/* Maximum allowed GPU global memory    */
/* (should be less than actual size ) */
/****************************************/
#define MAX_GMSIZE  1600000000
/****************************************/
/* Maximum allowed GPU shared memory    */
/****************************************/
#define MAX_SMSIZE  16384
/********************************************/
/* Maximum size of each dimension of a grid */
/********************************************/
#define MAX_GDIMENSION  65535

#define NUM_WORKERS  64


static int gpuNumThreads = NUM_WORKERS;
static int totalGpuNumThreads;
static int gpuNumBlocks;
static unsigned int gpuBytes = 0;

#endif 
/* End of __O2G_HEADER__ */



void residual(level_type * level, int res_id, int x_id, int rhs_id, double a, double b)
{
/* exchange the boundary for x in prep for Ax... */
exchange_boundary(level, x_id, stencil_is_star_shaped());
apply_BCs(level, x_id);
/* now do residualrestriction proper... */
uint64_t _timeStart;
_timeStart=CycleTime();
int box;
/* #pragma omp parallel for private(box) OMP_THREAD_ACROSS_BOXES(level->concurrent_boxes) */
#pragma omp parallel for if(level->concurrent_boxes) num_threads(level->concurrent_boxes) private(box)
for (box=0; box<level->num_my_boxes; box ++ )
{
int i;
int j;
int k;
int jStride;
jStride=level->my_boxes[box].jStride;
int kStride;
kStride=level->my_boxes[box].kStride;
int ghosts;
ghosts=level->my_boxes[box].ghosts;
int dim;
dim=level->my_boxes[box].dim;
double h2inv;
h2inv=1.0/(level->h*level->h);
const double * x;
x=level->my_boxes[box].vectors[x_id]+(ghosts*((1+jStride)+kStride));
/* i.e. [0] = first non ghost zone point */
const double * rhs;
rhs=level->my_boxes[box].vectors[rhs_id]+(ghosts*((1+jStride)+kStride));
/* cell centered coefficient */
const double * alpha;
alpha=level->my_boxes[box].vectors[5]+(ghosts*((1+jStride)+kStride));
/* face centered coefficient (n.b. element 0 is the left face of the ghost zone element) */
const double * beta_i;
beta_i=level->my_boxes[box].vectors[6]+(ghosts*((1+jStride)+kStride));
/* face centered coefficient (n.b. element 0 is the back face of the ghost zone element) */
const double * beta_j;
beta_j=level->my_boxes[box].vectors[7]+(ghosts*((1+jStride)+kStride));
/* face centered coefficient (n.b. element 0 is the bottom face of the ghost zone element) */
const double * beta_k;
beta_k=level->my_boxes[box].vectors[8]+(ghosts*((1+jStride)+kStride));
/* cell centered array noting which cells are actually present */
const double * valid;
valid=level->my_boxes[box].vectors[11]+(ghosts*((1+jStride)+kStride));
/* cell is inside the domain */
double * res;
res=level->my_boxes[box].vectors[res_id]+(ghosts*((1+jStride)+kStride));
/* #pragma omp parallel for private(k,j,i) OMP_THREAD_WITHIN_A_BOX(level->threads_per_box) */
#pragma omp parallel for if(level->threads_per_box>1) num_threads(level->threads_per_box) private(i, j, k) collapse(2)
for (k=0; k<dim; k ++ )
{
for (j=0; j<dim; j ++ )
{
for (i=0; i<dim; i ++ )
{
int ijk;
ijk=(i+(j*jStride))+(k*kStride);
double Ax;
Ax=((a*alpha[ijk])*x[ijk])-((b*h2inv)*((((((( + beta_i[ijk])*((valid[(ijk-1)]*(x[ijk]+x[(ijk-1)]))-(2.0*x[ijk])))+(beta_j[ijk]*((valid[(ijk-jStride)]*(x[ijk]+x[(ijk-jStride)]))-(2.0*x[ijk]))))+(beta_k[ijk]*((valid[(ijk-kStride)]*(x[ijk]+x[(ijk-kStride)]))-(2.0*x[ijk]))))+(beta_i[(ijk+1)]*((valid[(ijk+1)]*(x[ijk]+x[(ijk+1)]))-(2.0*x[ijk]))))+(beta_j[(ijk+jStride)]*((valid[(ijk+jStride)]*(x[ijk]+x[(ijk+jStride)]))-(2.0*x[ijk]))))+(beta_k[(ijk+kStride)]*((valid[(ijk+kStride)]*(x[ijk]+x[(ijk+kStride)]))-(2.0*x[ijk])))));
res[ijk]=(rhs[ijk]-Ax);
}
}
}
}
level->cycles.residual+=((uint64_t)(CycleTime()-_timeStart));
return ;
}

void residual_gpu(level_type * level, int res_id, int x_id, int rhs_id, double a, double b)
{
int box;

////////////////////////////////
// CUDA Device Initialization //
////////////////////////////////

/* [DEBUG] Below update will not be needed if all computations are ported to GPU. */
for (box=0; box<level->num_my_boxes; box ++ )
{
double * x_n;
double * gpu__x_n;
double * gpu__rhs_n;
x_n=level->my_boxes[box].vectors[x_id];
const uint64_t size = level->my_boxes[box].volume;
HI_set_async(box);
if ((HI_get_device_address(x_n, ((void * *)( & gpu__x_n)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, x_n, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  update async(box) device(x_n[0:size]) \n");
exit(1);
}
gpuBytes=(sizeof (double)*size);
HI_memcpy_async(gpu__x_n, x_n, gpuBytes, HI_MemcpyHostToDevice, 0, box);

double * rhs_n;
rhs_n=level->my_boxes[box].vectors[rhs_id];
HI_set_async(box);
if ((HI_get_device_address(rhs_n, ((void * *)( & gpu__rhs_n)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, rhs_n, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  update async(box) device(rhs_n[0:size]) \n");
exit(1);
}
gpuBytes=(sizeof (double)*size);
HI_memcpy_async(gpu__rhs_n, rhs_n, gpuBytes, HI_MemcpyHostToDevice, 0, box);

}
acc_wait_all();
/* exchange the boundary for x in prep for Ax... */
exchange_boundary_gpu(level, x_id, stencil_is_star_shaped());
apply_BCs(level, x_id);
/* now do residualrestriction proper... */
uint64_t _timeStart;
_timeStart=CycleTime();
/* #pragma omp parallel for private(box) OMP_THREAD_ACROSS_BOXES(level->concurrent_boxes) */
for (box=0; box<level->num_my_boxes; box ++ )
{
int i;
int j;
int k;
int jStride;
int k_0;
double * gpu__alpha;
double * gpu__beta_i;
double * gpu__beta_j;
double * gpu__beta_k;
double * gpu__res;
double * gpu__rhs;
double * gpu__valid;
double * gpu__x;
jStride=level->my_boxes[box].jStride;
int kStride;
kStride=level->my_boxes[box].kStride;
int ghosts;
ghosts=level->my_boxes[box].ghosts;
int dim;
dim=level->my_boxes[box].dim;
const uint64_t size = level->my_boxes[box].volume;
double h2inv;
h2inv=1.0/(level->h*level->h);
const double * x;
x=level->my_boxes[box].vectors[x_id];
/* + ghosts(1+jStride+kStride); i.e. [0] = first non ghost zone point */
const double * rhs;
rhs=level->my_boxes[box].vectors[rhs_id];
/* + ghosts(1+jStride+kStride); */
/* cell centered coefficient */
const double * alpha;
alpha=level->my_boxes[box].vectors[5];
/* + ghosts(1+jStride+kStride); */
/* face centered coefficient (n.b. element 0 is the left face of the ghost zone element) */
const double * beta_i;
beta_i=level->my_boxes[box].vectors[6];
/* + ghosts(1+jStride+kStride); */
/* face centered coefficient (n.b. element 0 is the back face of the ghost zone element) */
const double * beta_j;
beta_j=level->my_boxes[box].vectors[7];
/* + ghosts(1+jStride+kStride); */
/* face centered coefficient (n.b. element 0 is the bottom face of the ghost zone element) */
const double * beta_k;
beta_k=level->my_boxes[box].vectors[8];
/* + ghosts(1+jStride+kStride); */
/* cell centered array noting which cells are actually present */
const double * valid;
valid=level->my_boxes[box].vectors[11];
/* + ghosts(1+jStride+kStride); cell is inside the domain */
double * res;
res=level->my_boxes[box].vectors[res_id];
/* + ghosts(1+jStride+kStride); */
/* #pragma omp parallel for private(k,j,i) OMP_THREAD_WITHIN_A_BOX(level->threads_per_box) */
HI_set_async(box);
if ((HI_get_device_address(alpha, ((void * *)( & gpu__alpha)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, alpha, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  kernels loop async(box) gang(dim) copyin(a, b, dim, ghosts, h2inv, jStride, kStride) present(alpha[0:size], beta_i[0:size], beta_j[0:size], beta_k[0:size], res[0:size], rhs[0:size], valid[0:size], x[0:size]) private(i, k_0) \n");
exit(1);
}
if ((HI_get_device_address(beta_i, ((void * *)( & gpu__beta_i)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, beta_i, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  kernels loop async(box) gang(dim) copyin(a, b, dim, ghosts, h2inv, jStride, kStride) present(alpha[0:size], beta_i[0:size], beta_j[0:size], beta_k[0:size], res[0:size], rhs[0:size], valid[0:size], x[0:size]) private(i, k_0) \n");
exit(1);
}
if ((HI_get_device_address(beta_j, ((void * *)( & gpu__beta_j)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, beta_j, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  kernels loop async(box) gang(dim) copyin(a, b, dim, ghosts, h2inv, jStride, kStride) present(alpha[0:size], beta_i[0:size], beta_j[0:size], beta_k[0:size], res[0:size], rhs[0:size], valid[0:size], x[0:size]) private(i, k_0) \n");
exit(1);
}
if ((HI_get_device_address(beta_k, ((void * *)( & gpu__beta_k)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, beta_k, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  kernels loop async(box) gang(dim) copyin(a, b, dim, ghosts, h2inv, jStride, kStride) present(alpha[0:size], beta_i[0:size], beta_j[0:size], beta_k[0:size], res[0:size], rhs[0:size], valid[0:size], x[0:size]) private(i, k_0) \n");
exit(1);
}
if ((HI_get_device_address(res, ((void * *)( & gpu__res)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, res, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  kernels loop async(box) gang(dim) copyin(a, b, dim, ghosts, h2inv, jStride, kStride) present(alpha[0:size], beta_i[0:size], beta_j[0:size], beta_k[0:size], res[0:size], rhs[0:size], valid[0:size], x[0:size]) private(i, k_0) \n");
exit(1);
}
if ((HI_get_device_address(rhs, ((void * *)( & gpu__rhs)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, rhs, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  kernels loop async(box) gang(dim) copyin(a, b, dim, ghosts, h2inv, jStride, kStride) present(alpha[0:size], beta_i[0:size], beta_j[0:size], beta_k[0:size], res[0:size], rhs[0:size], valid[0:size], x[0:size]) private(i, k_0) \n");
exit(1);
}
if ((HI_get_device_address(valid, ((void * *)( & gpu__valid)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, valid, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  kernels loop async(box) gang(dim) copyin(a, b, dim, ghosts, h2inv, jStride, kStride) present(alpha[0:size], beta_i[0:size], beta_j[0:size], beta_k[0:size], res[0:size], rhs[0:size], valid[0:size], x[0:size]) private(i, k_0) \n");
exit(1);
}
if ((HI_get_device_address(x, ((void * *)( & gpu__x)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, x, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  kernels loop async(box) gang(dim) copyin(a, b, dim, ghosts, h2inv, jStride, kStride) present(alpha[0:size], beta_i[0:size], beta_j[0:size], beta_k[0:size], res[0:size], rhs[0:size], valid[0:size], x[0:size]) private(i, k_0) \n");
exit(1);
}
int dimGrid_residual_gpu_kernel0[3];
dimGrid_residual_gpu_kernel0[0]=dim;
dimGrid_residual_gpu_kernel0[1]=1;
dimGrid_residual_gpu_kernel0[2]=1;
int dimBlock_residual_gpu_kernel0[3];
dimBlock_residual_gpu_kernel0[0]=64;
dimBlock_residual_gpu_kernel0[1]=1;
dimBlock_residual_gpu_kernel0[2]=1;
gpuNumBlocks=dim;
gpuNumThreads=64;
totalGpuNumThreads=(64*dim);
HI_register_kernel_arg("residual_gpu_kernel0",0,sizeof(void*),( & gpu__alpha));
HI_register_kernel_arg("residual_gpu_kernel0",1,sizeof(void*),( & gpu__beta_i));
HI_register_kernel_arg("residual_gpu_kernel0",2,sizeof(void*),( & gpu__beta_j));
HI_register_kernel_arg("residual_gpu_kernel0",3,sizeof(void*),( & gpu__beta_k));
HI_register_kernel_arg("residual_gpu_kernel0",4,sizeof(void*),( & gpu__res));
HI_register_kernel_arg("residual_gpu_kernel0",5,sizeof(void*),( & gpu__rhs));
HI_register_kernel_arg("residual_gpu_kernel0",6,sizeof(void*),( & gpu__valid));
HI_register_kernel_arg("residual_gpu_kernel0",7,sizeof(void*),( & gpu__x));
HI_register_kernel_arg("residual_gpu_kernel0",8,sizeof(void*),( & a));
HI_register_kernel_arg("residual_gpu_kernel0",9,sizeof(void*),( & b));
HI_register_kernel_arg("residual_gpu_kernel0",10,sizeof(void*),( & dim));
HI_register_kernel_arg("residual_gpu_kernel0",11,sizeof(void*),( & ghosts));
HI_register_kernel_arg("residual_gpu_kernel0",12,sizeof(void*),( & h2inv));
HI_register_kernel_arg("residual_gpu_kernel0",13,sizeof(void*),( & jStride));
HI_register_kernel_arg("residual_gpu_kernel0",14,sizeof(void*),( & kStride));
HI_kernel_call("residual_gpu_kernel0",dimGrid_residual_gpu_kernel0,dimBlock_residual_gpu_kernel0,box);
;
gpuNumBlocks=dim;
HI_set_async(box);

if ((HI_get_device_address(res, ((void * *)( & gpu__res)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, res, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  update async(box) host(res[0:size]) \n");
exit(1);
}
gpuBytes=(sizeof (double)*size);
HI_memcpy_async(res, gpu__res, gpuBytes, HI_MemcpyDeviceToHost, 0, box);
}
acc_wait_all();
level->cycles.residual+=((uint64_t)(CycleTime()-_timeStart));
return ;
}

