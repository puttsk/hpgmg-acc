/* ------------------------------------------------------------------------------------------------------------------------------ */
/* Samuel Williams */
/* SWWilliams@lbl.gov */
/* Lawrence Berkeley National Lab */
/* ------------------------------------------------------------------------------------------------------------------------------ */
/* Based on Yousef Saad's Iterative Methods for Sparse Linear Algebra, Algorithm 12.1, page 399 */
/* ------------------------------------------------------------------------------------------------------------------------------ */

#ifndef __O2G_HEADER__ 

#define __O2G_HEADER__ 

/*******************************************/
/* Added codes for OpenACC2GPU translation */
/*******************************************/
#include <openacc.h>
#include <openaccrt.h>
#include <math.h>
#include <float.h>
#include <limits.h>
#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#ifdef __cplusplus
#define restrict __restrict__
#endif

/**********************************************************/
/* Maximum width of linear memory bound to texture memory */
/**********************************************************/
/* width in bytes */
#define LMAX_WIDTH    134217728
/**********************************/
/* Maximum memory pitch (in bytes)*/
/**********************************/
#define MAX_PITCH   262144
/****************************************/
/* Maximum allowed GPU global memory    */
/* (should be less than actual size ) */
/****************************************/
#define MAX_GMSIZE  1600000000
/****************************************/
/* Maximum allowed GPU shared memory    */
/****************************************/
#define MAX_SMSIZE  16384
/********************************************/
/* Maximum size of each dimension of a grid */
/********************************************/
#define MAX_GDIMENSION  65535

#define NUM_WORKERS  64


static int gpuNumThreads = NUM_WORKERS;
static int totalGpuNumThreads;
static int gpuNumBlocks;
static unsigned int gpuBytes = 0;

#endif 
/* End of __O2G_HEADER__ */



void smooth_gpu(level_type * level, int x_id, int rhs_id, double a, double b)
{
if (((CHEBYSHEV_DEGREE*NUM_SMOOTHS)&1))
{
printf("error... CHEBYSHEV_DEGREE*NUM_SMOOTHS must be even for the chebyshev smoother...\n");
exit(0);
}
if (((level->dominant_eigenvalue_of_DinvA<=0.0)&&(level->my_rank==0)))
{
printf("dominant_eigenvalue_of_DinvA <= 0.0 !\n");
}
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
int box;
int s;
/* compute the Chebyshev coefficients... */
double beta;
beta=1.0*level->dominant_eigenvalue_of_DinvA;
/* double alpha    = 0.300000beta; */
/* double alpha    = 0.250000beta; */
/* double alpha    = 0.166666beta; */
double alpha;
alpha=0.125*beta;
double theta;
theta=0.5*(beta+alpha);
/* center of the spectral ellipse */
double delta;
delta=0.5*(beta-alpha);
/* major axis? */
double sigma;
sigma=theta/delta;
double rho_n;
rho_n=1/sigma;
/* rho_0 */
double chebyshev_c1[CHEBYSHEV_DEGREE];
/* + c1(x_n-x_nm1) == rho_n*rho_nm1 */
double chebyshev_c2[CHEBYSHEV_DEGREE];
/* + c2(b-Ax_n) */
chebyshev_c1[0]=0.0;
chebyshev_c2[0]=(1/theta);
for (s=1; s<CHEBYSHEV_DEGREE; s ++ )
{
double rho_nm1;
rho_nm1=rho_n;
rho_n=(1.0/((2.0*sigma)-rho_nm1));
chebyshev_c1[s]=(rho_n*rho_nm1);
chebyshev_c2[s]=((rho_n*2.0)/delta);
}
for (box=0; box<level->num_my_boxes; box ++ )
{
double * x_n;
double * gpu__x_n;
double * gpu__x_np1;
x_n=level->my_boxes[box].vectors[x_id];
/*  */
double * x_np1;
x_np1=level->my_boxes[box].vectors[0];
const uint64_t size = level->my_boxes[box].volume;
HI_set_async(box);
if ((HI_get_device_address(x_n, ((void * *)( & gpu__x_n)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, x_n, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  update async(box) device(x_n[0:size], x_np1[0:size]) \n");
exit(1);
}
gpuBytes=(sizeof (double)*size);
HI_memcpy_async(gpu__x_n, x_n, gpuBytes, HI_MemcpyHostToDevice, 0, box);
if ((HI_get_device_address(x_np1, ((void * *)( & gpu__x_np1)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, x_np1, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  update async(box) device(x_n[0:size], x_np1[0:size]) \n");
exit(1);
}
gpuBytes=(sizeof (double)*size);
HI_memcpy_async(gpu__x_np1, x_np1, gpuBytes, HI_MemcpyHostToDevice, 0, box);

}
acc_wait_all();
for (s=0; s<(CHEBYSHEV_DEGREE*NUM_SMOOTHS); s ++ )
{
/* get ghost zone data... Chebyshev ping pongs between x_id and VECTOR_TEMP */
if (((s&1)==0))
{
exchange_boundary_gpu(level, x_id, stencil_is_star_shaped());
apply_BCs(level, x_id);
}
else
{
/*  */
exchange_boundary_gpu(level, 0, stencil_is_star_shaped());
/*  */
apply_BCs(level, 0);
}
/*    #pragma acc wait */
/* apply the smoother... Chebyshev ping pongs between x_id and VECTOR_TEMP */
uint64_t _timeStart;
_timeStart=CycleTime();
/* #pragma omp parallel for private(box) if(level->concurrent_boxes    >1) num_threads(level->concurrent_boxes ) */
for (box=0; box<level->num_my_boxes; box ++ )
{
int i;
int j;
int k;
int ghosts;
double * gpu__alpha;
double * gpu__beta_i;
double * gpu__beta_j;
double * gpu__beta_k;
double * gpu__rhs;
double * gpu__valid;
double * gpu__x_n;
double * gpu__x_np1;
ghosts=level->box_ghosts;
const int jStride = level->my_boxes[box].jStride;
const int kStride = level->my_boxes[box].kStride;
const int dim = level->my_boxes[box].dim;
/*      const int    size = (dim+2ghosts) + ((dim + 2*ghosts)*jStride) + ((dim + 2*ghosts)*kStride); */
const uint64_t size = level->my_boxes[box].volume;
const double h2inv = 1.0/(level->h*level->h);
const double * rhs;
rhs=level->my_boxes[box].vectors[rhs_id];
/* + ghosts(1+jStride+kStride); */
/* cell centered coefficient */
const double * alpha;
alpha=level->my_boxes[box].vectors[5];
/* + ghosts(1+jStride+kStride); */
/* face centered coefficient (n.b. element 0 is the left face of the ghost zone element) */
const double * beta_i;
beta_i=level->my_boxes[box].vectors[6];
/* + ghosts(1+jStride+kStride); */
/* face centered coefficient (n.b. element 0 is the back face of the ghost zone element) */
const double * beta_j;
beta_j=level->my_boxes[box].vectors[7];
/* + ghosts(1+jStride+kStride); */
/* face centered coefficient (n.b. element 0 is the bottom face of the ghost zone element) */
const double * beta_k;
beta_k=level->my_boxes[box].vectors[8];
/* + ghosts(1+jStride+kStride); */
/* cell centered relaxation parameter (e.g. inverse of the diagonal) */
const double * Dinv;
Dinv=level->my_boxes[box].vectors[9];
/* + ghosts(1+jStride+kStride); */
/* cell centered array noting which cells are actually present */
const double * valid;
valid=level->my_boxes[box].vectors[11];
/* + ghosts(1+jStride+kStride); cell is inside the domain */
double * x_np1;
const double * x_n;
const double * x_nm1;
/* + ghosts(1+jStride+kStride);} */
if (((s&1)==0))
{
x_n=level->my_boxes[box].vectors[x_id];
/* + ghosts(1+jStride+kStride); */
/*  */
x_nm1=level->my_boxes[box].vectors[0];
/* + ghosts(1+jStride+kStride);  */
/*  */
x_np1=level->my_boxes[box].vectors[0];
}
else
{
/*  */
x_n=level->my_boxes[box].vectors[0];
/* + ghosts(1+jStride+kStride); */
x_nm1=level->my_boxes[box].vectors[x_id];
/* + ghosts(1+jStride+kStride);  */
x_np1=level->my_boxes[box].vectors[x_id];
}
/* + ghosts(1+jStride+kStride);} */
const double c1 = chebyshev_c1[(s%CHEBYSHEV_DEGREE)];
/* limit polynomial to degree CHEBYSHEV_DEGREE. */
const double c2 = chebyshev_c2[(s%CHEBYSHEV_DEGREE)];
/* limit polynomial to degree CHEBYSHEV_DEGREE. */
/* #pragma omp parallel for private(k,j,i) OMP_THREAD_WITHIN_A_BOX(level->threads_per_box) */
HI_set_async(box);
if ((HI_get_device_address(alpha, ((void * *)( & gpu__alpha)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, alpha, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop async(box) num_workers(64) gang copyin(a, b, c1, c2, dim, ghosts, h2inv, jStride, kStride) present(alpha[0:size], beta_i[0:size], beta_j[0:size], beta_k[0:size], rhs[0:size], valid[0:size], x_n[0:size], x_np1[0:size]) private(i, j, k) num_gangs(dim) \n");
exit(1);
}
if ((HI_get_device_address(beta_i, ((void * *)( & gpu__beta_i)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, beta_i, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop async(box) num_workers(64) gang copyin(a, b, c1, c2, dim, ghosts, h2inv, jStride, kStride) present(alpha[0:size], beta_i[0:size], beta_j[0:size], beta_k[0:size], rhs[0:size], valid[0:size], x_n[0:size], x_np1[0:size]) private(i, j, k) num_gangs(dim) \n");
exit(1);
}
if ((HI_get_device_address(beta_j, ((void * *)( & gpu__beta_j)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, beta_j, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop async(box) num_workers(64) gang copyin(a, b, c1, c2, dim, ghosts, h2inv, jStride, kStride) present(alpha[0:size], beta_i[0:size], beta_j[0:size], beta_k[0:size], rhs[0:size], valid[0:size], x_n[0:size], x_np1[0:size]) private(i, j, k) num_gangs(dim) \n");
exit(1);
}
if ((HI_get_device_address(beta_k, ((void * *)( & gpu__beta_k)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, beta_k, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop async(box) num_workers(64) gang copyin(a, b, c1, c2, dim, ghosts, h2inv, jStride, kStride) present(alpha[0:size], beta_i[0:size], beta_j[0:size], beta_k[0:size], rhs[0:size], valid[0:size], x_n[0:size], x_np1[0:size]) private(i, j, k) num_gangs(dim) \n");
exit(1);
}
if ((HI_get_device_address(rhs, ((void * *)( & gpu__rhs)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, rhs, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop async(box) num_workers(64) gang copyin(a, b, c1, c2, dim, ghosts, h2inv, jStride, kStride) present(alpha[0:size], beta_i[0:size], beta_j[0:size], beta_k[0:size], rhs[0:size], valid[0:size], x_n[0:size], x_np1[0:size]) private(i, j, k) num_gangs(dim) \n");
exit(1);
}
if ((HI_get_device_address(valid, ((void * *)( & gpu__valid)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, valid, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop async(box) num_workers(64) gang copyin(a, b, c1, c2, dim, ghosts, h2inv, jStride, kStride) present(alpha[0:size], beta_i[0:size], beta_j[0:size], beta_k[0:size], rhs[0:size], valid[0:size], x_n[0:size], x_np1[0:size]) private(i, j, k) num_gangs(dim) \n");
exit(1);
}
if ((HI_get_device_address(x_n, ((void * *)( & gpu__x_n)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, x_n, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop async(box) num_workers(64) gang copyin(a, b, c1, c2, dim, ghosts, h2inv, jStride, kStride) present(alpha[0:size], beta_i[0:size], beta_j[0:size], beta_k[0:size], rhs[0:size], valid[0:size], x_n[0:size], x_np1[0:size]) private(i, j, k) num_gangs(dim) \n");
exit(1);
}
if ((HI_get_device_address(x_np1, ((void * *)( & gpu__x_np1)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, x_np1, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop async(box) num_workers(64) gang copyin(a, b, c1, c2, dim, ghosts, h2inv, jStride, kStride) present(alpha[0:size], beta_i[0:size], beta_j[0:size], beta_k[0:size], rhs[0:size], valid[0:size], x_n[0:size], x_np1[0:size]) private(i, j, k) num_gangs(dim) \n");
exit(1);
}
int dimGrid_smooth_gpu_kernel0[3];
dimGrid_smooth_gpu_kernel0[0]=dim;
dimGrid_smooth_gpu_kernel0[1]=1;
dimGrid_smooth_gpu_kernel0[2]=1;
int dimBlock_smooth_gpu_kernel0[3];
dimBlock_smooth_gpu_kernel0[0]=64;
dimBlock_smooth_gpu_kernel0[1]=1;
dimBlock_smooth_gpu_kernel0[2]=1;
gpuNumBlocks=dim;
gpuNumThreads=64;
totalGpuNumThreads=(64*dim);
HI_register_kernel_arg("smooth_gpu_kernel0",0,sizeof(void*),( & gpu__alpha));
HI_register_kernel_arg("smooth_gpu_kernel0",1,sizeof(void*),( & gpu__beta_i));
HI_register_kernel_arg("smooth_gpu_kernel0",2,sizeof(void*),( & gpu__beta_j));
HI_register_kernel_arg("smooth_gpu_kernel0",3,sizeof(void*),( & gpu__beta_k));
HI_register_kernel_arg("smooth_gpu_kernel0",4,sizeof(void*),( & gpu__rhs));
HI_register_kernel_arg("smooth_gpu_kernel0",5,sizeof(void*),( & gpu__valid));
HI_register_kernel_arg("smooth_gpu_kernel0",6,sizeof(void*),( & gpu__x_n));
HI_register_kernel_arg("smooth_gpu_kernel0",7,sizeof(void*),( & gpu__x_np1));
HI_register_kernel_arg("smooth_gpu_kernel0",8,sizeof(void*),( & a));
HI_register_kernel_arg("smooth_gpu_kernel0",9,sizeof(void*),( & b));
HI_register_kernel_arg("smooth_gpu_kernel0",10,sizeof(void*),((void *)( & c1)));
HI_register_kernel_arg("smooth_gpu_kernel0",11,sizeof(void*),((void *)( & c2)));
HI_register_kernel_arg("smooth_gpu_kernel0",12,sizeof(void*),((void *)( & dim)));
HI_register_kernel_arg("smooth_gpu_kernel0",13,sizeof(void*),( & ghosts));
HI_register_kernel_arg("smooth_gpu_kernel0",14,sizeof(void*),((void *)( & h2inv)));
HI_register_kernel_arg("smooth_gpu_kernel0",15,sizeof(void*),((void *)( & jStride)));
HI_register_kernel_arg("smooth_gpu_kernel0",16,sizeof(void*),((void *)( & kStride)));
HI_kernel_call("smooth_gpu_kernel0",dimGrid_smooth_gpu_kernel0,dimBlock_smooth_gpu_kernel0,box);
;
gpuNumBlocks=dim;
HI_set_async(box);

if ((HI_get_device_address(x_np1, ((void * *)( & gpu__x_np1)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, x_np1, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  update async(box) host(x_np1[0:size]) \n");
exit(1);
}
gpuBytes=(sizeof (double)*size);
HI_memcpy_async(x_np1, gpu__x_np1, gpuBytes, HI_MemcpyDeviceToHost, 0, box);
}
/* box-loop */
acc_wait_all();
level->cycles.smooth+=((uint64_t)(CycleTime()-_timeStart));
}
/* s-loop */
return ;
}

void smooth(level_type * level, int x_id, int rhs_id, double a, double b)
{

////////////////////////////////
// CUDA Device Initialization //
////////////////////////////////

if (((CHEBYSHEV_DEGREE*NUM_SMOOTHS)&1))
{
printf("error... CHEBYSHEV_DEGREE*NUM_SMOOTHS must be even for the chebyshev smoother...\n");
exit(0);
}
if (((level->dominant_eigenvalue_of_DinvA<=0.0)&&(level->my_rank==0)))
{
printf("dominant_eigenvalue_of_DinvA <= 0.0 !\n");
}
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
int box;
int s;
/* compute the Chebyshev coefficients... */
double beta;
beta=1.0*level->dominant_eigenvalue_of_DinvA;
/* double alpha    = 0.300000beta; */
/* double alpha    = 0.250000beta; */
/* double alpha    = 0.166666beta; */
double alpha;
alpha=0.125*beta;
double theta;
theta=0.5*(beta+alpha);
/* center of the spectral ellipse */
double delta;
delta=0.5*(beta-alpha);
/* major axis? */
double sigma;
sigma=theta/delta;
double rho_n;
rho_n=1/sigma;
/* rho_0 */
double chebyshev_c1[CHEBYSHEV_DEGREE];
/* + c1(x_n-x_nm1) == rho_n*rho_nm1 */
double chebyshev_c2[CHEBYSHEV_DEGREE];
/* + c2(b-Ax_n) */
chebyshev_c1[0]=0.0;
chebyshev_c2[0]=(1/theta);
for (s=1; s<CHEBYSHEV_DEGREE; s ++ )
{
double rho_nm1;
rho_nm1=rho_n;
rho_n=(1.0/((2.0*sigma)-rho_nm1));
chebyshev_c1[s]=(rho_n*rho_nm1);
chebyshev_c2[s]=((rho_n*2.0)/delta);
}
for (s=0; s<(CHEBYSHEV_DEGREE*NUM_SMOOTHS); s ++ )
{
/* get ghost zone data... Chebyshev ping pongs between x_id and VECTOR_TEMP */
if (((s&1)==0))
{
exchange_boundary(level, x_id, stencil_is_star_shaped());
apply_BCs(level, x_id);
}
else
{
/*  */
exchange_boundary(level, 0, stencil_is_star_shaped());
/*  */
apply_BCs(level, 0);
}
/* apply the smoother... Chebyshev ping pongs between x_id and VECTOR_TEMP */
uint64_t _timeStart;
_timeStart=CycleTime();
/* #pragma omp parallel for private(box) OMP_THREAD_ACROSS_BOXES(level->concurrent_boxes) */
#pragma omp parallel for if(level->concurrent_boxes) num_threads(level->concurrent_boxes) private(box)
for (box=0; box<level->num_my_boxes; box ++ )
{
int i;
int j;
int k;
int ghosts;
ghosts=level->box_ghosts;
const int jStride = level->my_boxes[box].jStride;
const int kStride = level->my_boxes[box].kStride;
const int dim = level->my_boxes[box].dim;
const double h2inv = 1.0/(level->h*level->h);
const double * rhs;
rhs=level->my_boxes[box].vectors[rhs_id]+(ghosts*((1+jStride)+kStride));
/* cell centered coefficient */
const double * alpha;
alpha=level->my_boxes[box].vectors[5]+(ghosts*((1+jStride)+kStride));
/* face centered coefficient (n.b. element 0 is the left face of the ghost zone element) */
const double * beta_i;
beta_i=level->my_boxes[box].vectors[6]+(ghosts*((1+jStride)+kStride));
/* face centered coefficient (n.b. element 0 is the back face of the ghost zone element) */
const double * beta_j;
beta_j=level->my_boxes[box].vectors[7]+(ghosts*((1+jStride)+kStride));
/* face centered coefficient (n.b. element 0 is the bottom face of the ghost zone element) */
const double * beta_k;
beta_k=level->my_boxes[box].vectors[8]+(ghosts*((1+jStride)+kStride));
/* cell centered relaxation parameter (e.g. inverse of the diagonal) */
const double * Dinv;
Dinv=level->my_boxes[box].vectors[9]+(ghosts*((1+jStride)+kStride));
/* cell centered array noting which cells are actually present */
const double * valid;
valid=level->my_boxes[box].vectors[11]+(ghosts*((1+jStride)+kStride));
/* cell is inside the domain */
double * x_np1;
const double * x_n;
const double * x_nm1;
if (((s&1)==0))
{
x_n=(level->my_boxes[box].vectors[x_id]+(ghosts*((1+jStride)+kStride)));
/*  */
x_nm1=(level->my_boxes[box].vectors[0]+(ghosts*((1+jStride)+kStride)));
/*  */
x_np1=(level->my_boxes[box].vectors[0]+(ghosts*((1+jStride)+kStride)));
}
else
{
/*  */
x_n=(level->my_boxes[box].vectors[0]+(ghosts*((1+jStride)+kStride)));
x_nm1=(level->my_boxes[box].vectors[x_id]+(ghosts*((1+jStride)+kStride)));
x_np1=(level->my_boxes[box].vectors[x_id]+(ghosts*((1+jStride)+kStride)));
}
const double c1 = chebyshev_c1[(s%CHEBYSHEV_DEGREE)];
/* limit polynomial to degree CHEBYSHEV_DEGREE. */
const double c2 = chebyshev_c2[(s%CHEBYSHEV_DEGREE)];
/* limit polynomial to degree CHEBYSHEV_DEGREE. */
/* #pragma omp parallel for private(k,j,i) OMP_THREAD_WITHIN_A_BOX(level->threads_per_box) */
#pragma omp parallel for if(level->threads_per_box>1) num_threads(level->threads_per_box) private(i, j, k) collapse(2)
for (k=0; k<dim; k ++ )
{
for (j=0; j<dim; j ++ )
{
for (i=0; i<dim; i ++ )
{
int ijk;
ijk=(i+(j*jStride))+(k*kStride);
/* According to Saad... but his was missing a Dinv[ijk] == D^{-1} !!! */
/*  x_{n+1} = x_{n} + rho_{n} [ rho_{n-1}(x_{n} - x_{n-1}) + (2delta)(b-Ax_{n}) ] */
/*  x_temp[ijk] = x_n[ijk] + c1(x_n[ijk]-x_temp[ijk]) + c2*Dinv[ijk]*(rhs[ijk]-Ax_n); */
double Ax_n;
Ax_n=((a*alpha[ijk])*x_n[ijk])-((b*h2inv)*((((((( + beta_i[ijk])*((valid[(ijk-1)]*(x_n[ijk]+x_n[(ijk-1)]))-(2.0*x_n[ijk])))+(beta_j[ijk]*((valid[(ijk-jStride)]*(x_n[ijk]+x_n[(ijk-jStride)]))-(2.0*x_n[ijk]))))+(beta_k[ijk]*((valid[(ijk-kStride)]*(x_n[ijk]+x_n[(ijk-kStride)]))-(2.0*x_n[ijk]))))+(beta_i[(ijk+1)]*((valid[(ijk+1)]*(x_n[ijk]+x_n[(ijk+1)]))-(2.0*x_n[ijk]))))+(beta_j[(ijk+jStride)]*((valid[(ijk+jStride)]*(x_n[ijk]+x_n[(ijk+jStride)]))-(2.0*x_n[ijk]))))+(beta_k[(ijk+kStride)]*((valid[(ijk+kStride)]*(x_n[ijk]+x_n[(ijk+kStride)]))-(2.0*x_n[ijk])))));
double lambda;
lambda=1.0/((a*alpha[ijk])-((b*h2inv)*((((((( + beta_i[ijk])*(valid[(ijk-1)]-2.0))+(beta_j[ijk]*(valid[(ijk-jStride)]-2.0)))+(beta_k[ijk]*(valid[(ijk-kStride)]-2.0)))+(beta_i[(ijk+1)]*(valid[(ijk+1)]-2.0)))+(beta_j[(ijk+jStride)]*(valid[(ijk+jStride)]-2.0)))+(beta_k[(ijk+kStride)]*(valid[(ijk+kStride)]-2.0)))));
x_np1[ijk]=((x_n[ijk]+(c1*(x_n[ijk]-x_nm1[ijk])))+((c2*lambda)*(rhs[ijk]-Ax_n)));
}
}
}
}
/* box-loop */
level->cycles.smooth+=((uint64_t)(CycleTime()-_timeStart));
}
/* s-loop */
return ;
}

