/* ------------------------------------------------------------------------------------------------------------------------------ */
/* Samuel Williams */
/* SWWilliams@lbl.gov */
/* Lawrence Berkeley National Lab */
/* ------------------------------------------------------------------------------------------------------------------------------ */

#ifndef __O2G_HEADER__ 

#define __O2G_HEADER__ 

/*******************************************/
/* Added codes for OpenACC2GPU translation */
/*******************************************/
#include <openacc.h>
#include <openaccrt.h>
#include <math.h>
#include <float.h>
#include <limits.h>
#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#ifdef __cplusplus
#define restrict __restrict__
#endif

/**********************************************************/
/* Maximum width of linear memory bound to texture memory */
/**********************************************************/
/* width in bytes */
#define LMAX_WIDTH    134217728
/**********************************/
/* Maximum memory pitch (in bytes)*/
/**********************************/
#define MAX_PITCH   262144
/****************************************/
/* Maximum allowed GPU global memory    */
/* (should be less than actual size ) */
/****************************************/
#define MAX_GMSIZE  1600000000
/****************************************/
/* Maximum allowed GPU shared memory    */
/****************************************/
#define MAX_SMSIZE  16384
/********************************************/
/* Maximum size of each dimension of a grid */
/********************************************/
#define MAX_GDIMENSION  65535

#define NUM_WORKERS  64


static int gpuNumThreads = NUM_WORKERS;
static int totalGpuNumThreads;
static int gpuNumBlocks;
static unsigned int gpuBytes = 0;

#endif 
/* End of __O2G_HEADER__ */



double * gpu__read_buffer;
/* static inline void CopyBlock(level_typelevel, int id, blockCopy_type *block){ */
/* Copy from read buffer in GPU to write buffer in GPU. */
void CopyBlockLocal(level_type * level, int id, blockCopy_type * block)
{
/* copy 3D array from read_i,j,k of read[] to write_i,j,k in write[] */
int dim_i;

////////////////////////////////
// CUDA Device Initialization //
////////////////////////////////

double * gpu__read;
double * gpu__write;
dim_i=block->dim.i;
int dim_j;
dim_j=block->dim.j;
int dim_k;
dim_k=block->dim.k;
int read_i;
read_i=block->read.i;
int read_j;
read_j=block->read.j;
int read_k;
read_k=block->read.k;
int read_jStride;
read_jStride=block->read.jStride;
int read_kStride;
read_kStride=block->read.kStride;
int write_i;
write_i=block->write.i;
int write_j;
write_j=block->write.j;
int write_k;
write_k=block->write.k;
int write_jStride;
write_jStride=block->write.jStride;
int write_kStride;
write_kStride=block->write.kStride;
double * read;
read=block->read.ptr;
double * write;
write=block->write.ptr;
int read_ghost = 0;
int write_ghost = 0;
int i;
int j;
int k;
int asyncID;
asyncID=level->num_my_boxes+1;
if ((block->read.box>=0))
{
read=level->my_boxes[block->read.box].vectors[id];
/* + level->my_boxes[ block->read.box].ghosts(1+level->my_boxes[ block->read.box].jStride+level->my_boxes[ block->read.box].kStride); */
read_ghost=level->my_boxes[block->read.box].ghosts;
}
if ((block->write.box>=0))
{
write=level->my_boxes[block->write.box].vectors[id];
/* + level->my_boxes[block->write.box].ghosts(1+level->my_boxes[block->write.box].jStride+level->my_boxes[block->write.box].kStride); */
write_ghost=level->my_boxes[block->write.box].ghosts;
}
int dimGrid_CopyBlockLocal_kernel0[3];
dimGrid_CopyBlockLocal_kernel0[0]=dim_k;
dimGrid_CopyBlockLocal_kernel0[1]=1;
dimGrid_CopyBlockLocal_kernel0[2]=1;
int dimBlock_CopyBlockLocal_kernel0[3];
dimBlock_CopyBlockLocal_kernel0[0]=64;
dimBlock_CopyBlockLocal_kernel0[1]=1;
dimBlock_CopyBlockLocal_kernel0[2]=1;
gpuNumBlocks=dim_k;
gpuNumThreads=64;
totalGpuNumThreads=(64*dim_k);
int dimGrid_CopyBlockLocal_kernel1[3];
dimGrid_CopyBlockLocal_kernel1[0]=dim_k;
dimGrid_CopyBlockLocal_kernel1[1]=1;
dimGrid_CopyBlockLocal_kernel1[2]=1;
int dimBlock_CopyBlockLocal_kernel1[3];
dimBlock_CopyBlockLocal_kernel1[0]=64;
dimBlock_CopyBlockLocal_kernel1[1]=1;
dimBlock_CopyBlockLocal_kernel1[2]=1;
gpuNumBlocks=dim_k;
gpuNumThreads=64;
totalGpuNumThreads=(64*dim_k);
int dimGrid_CopyBlockLocal_kernel2[3];
dimGrid_CopyBlockLocal_kernel2[0]=dim_j;
dimGrid_CopyBlockLocal_kernel2[1]=1;
dimGrid_CopyBlockLocal_kernel2[2]=1;
int dimBlock_CopyBlockLocal_kernel2[3];
dimBlock_CopyBlockLocal_kernel2[0]=64;
dimBlock_CopyBlockLocal_kernel2[1]=1;
dimBlock_CopyBlockLocal_kernel2[2]=1;
gpuNumBlocks=dim_j;
gpuNumThreads=64;
totalGpuNumThreads=(64*dim_j);
int dimGrid_CopyBlockLocal_kernel3[3];
dimGrid_CopyBlockLocal_kernel3[0]=dim_k;
dimGrid_CopyBlockLocal_kernel3[1]=1;
dimGrid_CopyBlockLocal_kernel3[2]=1;
int dimBlock_CopyBlockLocal_kernel3[3];
dimBlock_CopyBlockLocal_kernel3[0]=64;
dimBlock_CopyBlockLocal_kernel3[1]=1;
dimBlock_CopyBlockLocal_kernel3[2]=1;
gpuNumBlocks=dim_k;
gpuNumThreads=64;
totalGpuNumThreads=(64*dim_k);
if ((dim_i==1))
{
/* be smart and don't have an inner loop from 0 to 0 */
HI_set_async(asyncID);
if ((HI_get_device_address(read, ((void * *)( & gpu__read)), asyncID)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, read, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop async(asyncID) num_workers(64) gang copyin(dim_j, dim_k, read_ghost, read_i, read_j, read_jStride, read_k, read_kStride, write_ghost, write_i, write_j, write_jStride, write_k, write_kStride) present(read, write) private(j, k) num_gangs(dim_k) \n");
exit(1);
}
if ((HI_get_device_address(write, ((void * *)( & gpu__write)), asyncID)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, write, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop async(asyncID) num_workers(64) gang copyin(dim_j, dim_k, read_ghost, read_i, read_j, read_jStride, read_k, read_kStride, write_ghost, write_i, write_j, write_jStride, write_k, write_kStride) present(read, write) private(j, k) num_gangs(dim_k) \n");
exit(1);
}
HI_register_kernel_arg("CopyBlockLocal_kernel0",0,sizeof(void*),( & gpu__read));
HI_register_kernel_arg("CopyBlockLocal_kernel0",1,sizeof(void*),( & gpu__write));
HI_register_kernel_arg("CopyBlockLocal_kernel0",2,sizeof(void*),( & dim_j));
HI_register_kernel_arg("CopyBlockLocal_kernel0",3,sizeof(void*),( & dim_k));
HI_register_kernel_arg("CopyBlockLocal_kernel0",4,sizeof(void*),( & read_ghost));
HI_register_kernel_arg("CopyBlockLocal_kernel0",5,sizeof(void*),( & read_i));
HI_register_kernel_arg("CopyBlockLocal_kernel0",6,sizeof(void*),( & read_j));
HI_register_kernel_arg("CopyBlockLocal_kernel0",7,sizeof(void*),( & read_jStride));
HI_register_kernel_arg("CopyBlockLocal_kernel0",8,sizeof(void*),( & read_k));
HI_register_kernel_arg("CopyBlockLocal_kernel0",9,sizeof(void*),( & read_kStride));
HI_register_kernel_arg("CopyBlockLocal_kernel0",10,sizeof(void*),( & write_ghost));
HI_register_kernel_arg("CopyBlockLocal_kernel0",11,sizeof(void*),( & write_i));
HI_register_kernel_arg("CopyBlockLocal_kernel0",12,sizeof(void*),( & write_j));
HI_register_kernel_arg("CopyBlockLocal_kernel0",13,sizeof(void*),( & write_jStride));
HI_register_kernel_arg("CopyBlockLocal_kernel0",14,sizeof(void*),( & write_k));
HI_register_kernel_arg("CopyBlockLocal_kernel0",15,sizeof(void*),( & write_kStride));
HI_kernel_call("CopyBlockLocal_kernel0",dimGrid_CopyBlockLocal_kernel0,dimBlock_CopyBlockLocal_kernel0,asyncID);
;
gpuNumBlocks=dim_k;
}
else
{
if ((dim_j==1))
{
/* don't have a 0..0 loop */
HI_set_async(asyncID);
if ((HI_get_device_address(read, ((void * *)( & gpu__read)), asyncID)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, read, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop async(asyncID) num_workers(64) gang copyin(dim_i, dim_k, read_ghost, read_i, read_j, read_jStride, read_k, read_kStride, write_ghost, write_i, write_j, write_jStride, write_k, write_kStride) present(read, write) private(i, k) num_gangs(dim_k) \n");
exit(1);
}
if ((HI_get_device_address(write, ((void * *)( & gpu__write)), asyncID)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, write, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop async(asyncID) num_workers(64) gang copyin(dim_i, dim_k, read_ghost, read_i, read_j, read_jStride, read_k, read_kStride, write_ghost, write_i, write_j, write_jStride, write_k, write_kStride) present(read, write) private(i, k) num_gangs(dim_k) \n");
exit(1);
}
HI_register_kernel_arg("CopyBlockLocal_kernel1",0,sizeof(void*),( & gpu__read));
HI_register_kernel_arg("CopyBlockLocal_kernel1",1,sizeof(void*),( & gpu__write));
HI_register_kernel_arg("CopyBlockLocal_kernel1",2,sizeof(void*),( & dim_i));
HI_register_kernel_arg("CopyBlockLocal_kernel1",3,sizeof(void*),( & dim_k));
HI_register_kernel_arg("CopyBlockLocal_kernel1",4,sizeof(void*),( & read_ghost));
HI_register_kernel_arg("CopyBlockLocal_kernel1",5,sizeof(void*),( & read_i));
HI_register_kernel_arg("CopyBlockLocal_kernel1",6,sizeof(void*),( & read_j));
HI_register_kernel_arg("CopyBlockLocal_kernel1",7,sizeof(void*),( & read_jStride));
HI_register_kernel_arg("CopyBlockLocal_kernel1",8,sizeof(void*),( & read_k));
HI_register_kernel_arg("CopyBlockLocal_kernel1",9,sizeof(void*),( & read_kStride));
HI_register_kernel_arg("CopyBlockLocal_kernel1",10,sizeof(void*),( & write_ghost));
HI_register_kernel_arg("CopyBlockLocal_kernel1",11,sizeof(void*),( & write_i));
HI_register_kernel_arg("CopyBlockLocal_kernel1",12,sizeof(void*),( & write_j));
HI_register_kernel_arg("CopyBlockLocal_kernel1",13,sizeof(void*),( & write_jStride));
HI_register_kernel_arg("CopyBlockLocal_kernel1",14,sizeof(void*),( & write_k));
HI_register_kernel_arg("CopyBlockLocal_kernel1",15,sizeof(void*),( & write_kStride));
HI_kernel_call("CopyBlockLocal_kernel1",dimGrid_CopyBlockLocal_kernel1,dimBlock_CopyBlockLocal_kernel1,asyncID);
;
gpuNumBlocks=dim_k;
}
else
{
if ((dim_k==1))
{
/* don't have a 0..0 loop */
HI_set_async(asyncID);
if ((HI_get_device_address(read, ((void * *)( & gpu__read)), asyncID)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, read, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop async(asyncID) num_workers(64) gang copyin(dim_i, dim_j, read_ghost, read_i, read_j, read_jStride, read_k, read_kStride, write_ghost, write_i, write_j, write_jStride, write_k, write_kStride) present(read, write) private(i, j) num_gangs(dim_j) \n");
exit(1);
}
if ((HI_get_device_address(write, ((void * *)( & gpu__write)), asyncID)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, write, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop async(asyncID) num_workers(64) gang copyin(dim_i, dim_j, read_ghost, read_i, read_j, read_jStride, read_k, read_kStride, write_ghost, write_i, write_j, write_jStride, write_k, write_kStride) present(read, write) private(i, j) num_gangs(dim_j) \n");
exit(1);
}
HI_register_kernel_arg("CopyBlockLocal_kernel2",0,sizeof(void*),( & gpu__read));
HI_register_kernel_arg("CopyBlockLocal_kernel2",1,sizeof(void*),( & gpu__write));
HI_register_kernel_arg("CopyBlockLocal_kernel2",2,sizeof(void*),( & dim_i));
HI_register_kernel_arg("CopyBlockLocal_kernel2",3,sizeof(void*),( & dim_j));
HI_register_kernel_arg("CopyBlockLocal_kernel2",4,sizeof(void*),( & read_ghost));
HI_register_kernel_arg("CopyBlockLocal_kernel2",5,sizeof(void*),( & read_i));
HI_register_kernel_arg("CopyBlockLocal_kernel2",6,sizeof(void*),( & read_j));
HI_register_kernel_arg("CopyBlockLocal_kernel2",7,sizeof(void*),( & read_jStride));
HI_register_kernel_arg("CopyBlockLocal_kernel2",8,sizeof(void*),( & read_k));
HI_register_kernel_arg("CopyBlockLocal_kernel2",9,sizeof(void*),( & read_kStride));
HI_register_kernel_arg("CopyBlockLocal_kernel2",10,sizeof(void*),( & write_ghost));
HI_register_kernel_arg("CopyBlockLocal_kernel2",11,sizeof(void*),( & write_i));
HI_register_kernel_arg("CopyBlockLocal_kernel2",12,sizeof(void*),( & write_j));
HI_register_kernel_arg("CopyBlockLocal_kernel2",13,sizeof(void*),( & write_jStride));
HI_register_kernel_arg("CopyBlockLocal_kernel2",14,sizeof(void*),( & write_k));
HI_register_kernel_arg("CopyBlockLocal_kernel2",15,sizeof(void*),( & write_kStride));
HI_kernel_call("CopyBlockLocal_kernel2",dimGrid_CopyBlockLocal_kernel2,dimBlock_CopyBlockLocal_kernel2,asyncID);
;
gpuNumBlocks=dim_j;
}
else
{
HI_set_async(asyncID);
if ((HI_get_device_address(read, ((void * *)( & gpu__read)), asyncID)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, read, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop async(asyncID) num_workers(64) gang copyin(dim_i, dim_j, dim_k, read_ghost, read_i, read_j, read_jStride, read_k, read_kStride, write_ghost, write_i, write_j, write_jStride, write_k, write_kStride) present(read, write) private(i, j, k) num_gangs(dim_k) \n");
exit(1);
}
if ((HI_get_device_address(write, ((void * *)( & gpu__write)), asyncID)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, write, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop async(asyncID) num_workers(64) gang copyin(dim_i, dim_j, dim_k, read_ghost, read_i, read_j, read_jStride, read_k, read_kStride, write_ghost, write_i, write_j, write_jStride, write_k, write_kStride) present(read, write) private(i, j, k) num_gangs(dim_k) \n");
exit(1);
}
HI_register_kernel_arg("CopyBlockLocal_kernel3",0,sizeof(void*),( & gpu__read));
HI_register_kernel_arg("CopyBlockLocal_kernel3",1,sizeof(void*),( & gpu__write));
HI_register_kernel_arg("CopyBlockLocal_kernel3",2,sizeof(void*),( & dim_i));
HI_register_kernel_arg("CopyBlockLocal_kernel3",3,sizeof(void*),( & dim_j));
HI_register_kernel_arg("CopyBlockLocal_kernel3",4,sizeof(void*),( & dim_k));
HI_register_kernel_arg("CopyBlockLocal_kernel3",5,sizeof(void*),( & read_ghost));
HI_register_kernel_arg("CopyBlockLocal_kernel3",6,sizeof(void*),( & read_i));
HI_register_kernel_arg("CopyBlockLocal_kernel3",7,sizeof(void*),( & read_j));
HI_register_kernel_arg("CopyBlockLocal_kernel3",8,sizeof(void*),( & read_jStride));
HI_register_kernel_arg("CopyBlockLocal_kernel3",9,sizeof(void*),( & read_k));
HI_register_kernel_arg("CopyBlockLocal_kernel3",10,sizeof(void*),( & read_kStride));
HI_register_kernel_arg("CopyBlockLocal_kernel3",11,sizeof(void*),( & write_ghost));
HI_register_kernel_arg("CopyBlockLocal_kernel3",12,sizeof(void*),( & write_i));
HI_register_kernel_arg("CopyBlockLocal_kernel3",13,sizeof(void*),( & write_j));
HI_register_kernel_arg("CopyBlockLocal_kernel3",14,sizeof(void*),( & write_jStride));
HI_register_kernel_arg("CopyBlockLocal_kernel3",15,sizeof(void*),( & write_k));
HI_register_kernel_arg("CopyBlockLocal_kernel3",16,sizeof(void*),( & write_kStride));
HI_kernel_call("CopyBlockLocal_kernel3",dimGrid_CopyBlockLocal_kernel3,dimBlock_CopyBlockLocal_kernel3,asyncID);
;
gpuNumBlocks=dim_k;
}
}
}
/* #pragma acc wait(asyncID) */
return ;
}

/* Transfer read buffer to GPU and then copy it to writer buffer. */
void CopyBlockToGPU(level_type * level, int id, blockCopy_type * block)
{
/* copy 3D array from read_i,j,k of read[] to write_i,j,k in write[] */
int dim_i;
int _ti_100_0;
double * gpu__write;
dim_i=block->dim.i;
int dim_j;
dim_j=block->dim.j;
int dim_k;
dim_k=block->dim.k;
int read_i;
read_i=block->read.i;
int read_j;
read_j=block->read.j;
int read_k;
read_k=block->read.k;
int read_jStride;
read_jStride=block->read.jStride;
int read_kStride;
read_kStride=block->read.kStride;
int write_i;
write_i=block->write.i;
int write_j;
write_j=block->write.j;
int write_k;
write_k=block->write.k;
int write_jStride;
write_jStride=block->write.jStride;
int write_kStride;
write_kStride=block->write.kStride;
double * read;
read=block->read.ptr;
double * write;
write=block->write.ptr;
int read_ghost = 0;
int write_ghost = 0;
int i;
int j;
int k;
int asyncID;
asyncID=level->num_my_boxes+1;
if ((block->read.box>=0))
{
read=level->my_boxes[block->read.box].vectors[id];
/* + level->my_boxes[ block->read.box].ghosts(1+level->my_boxes[ block->read.box].jStride+level->my_boxes[ block->read.box].kStride); */
read_ghost=level->my_boxes[block->read.box].ghosts;
}
if ((block->write.box>=0))
{
write=level->my_boxes[block->write.box].vectors[id];
/* + level->my_boxes[block->write.box].ghosts(1+level->my_boxes[block->write.box].jStride+level->my_boxes[block->write.box].kStride); */
write_ghost=level->my_boxes[block->write.box].ghosts;
}
int dimGrid_CopyBlockToGPU_kernel0[3];
dimGrid_CopyBlockToGPU_kernel0[0]=dim_k;
dimGrid_CopyBlockToGPU_kernel0[1]=1;
dimGrid_CopyBlockToGPU_kernel0[2]=1;
int dimBlock_CopyBlockToGPU_kernel0[3];
dimBlock_CopyBlockToGPU_kernel0[0]=64;
dimBlock_CopyBlockToGPU_kernel0[1]=1;
dimBlock_CopyBlockToGPU_kernel0[2]=1;
gpuNumBlocks=dim_k;
gpuNumThreads=64;
totalGpuNumThreads=(64*dim_k);
int dimGrid_CopyBlockToGPU_kernel1[3];
dimGrid_CopyBlockToGPU_kernel1[0]=dim_k;
dimGrid_CopyBlockToGPU_kernel1[1]=1;
dimGrid_CopyBlockToGPU_kernel1[2]=1;
int dimBlock_CopyBlockToGPU_kernel1[3];
dimBlock_CopyBlockToGPU_kernel1[0]=64;
dimBlock_CopyBlockToGPU_kernel1[1]=1;
dimBlock_CopyBlockToGPU_kernel1[2]=1;
gpuNumBlocks=dim_k;
gpuNumThreads=64;
totalGpuNumThreads=(64*dim_k);
int dimGrid_CopyBlockToGPU_kernel2[3];
dimGrid_CopyBlockToGPU_kernel2[0]=dim_j;
dimGrid_CopyBlockToGPU_kernel2[1]=1;
dimGrid_CopyBlockToGPU_kernel2[2]=1;
int dimBlock_CopyBlockToGPU_kernel2[3];
dimBlock_CopyBlockToGPU_kernel2[0]=64;
dimBlock_CopyBlockToGPU_kernel2[1]=1;
dimBlock_CopyBlockToGPU_kernel2[2]=1;
gpuNumBlocks=dim_j;
gpuNumThreads=64;
totalGpuNumThreads=(64*dim_j);
int dimGrid_CopyBlockToGPU_kernel3[3];
dimGrid_CopyBlockToGPU_kernel3[0]=((int)ceil((((float)((dim_i*dim_j)*dim_k))/64.0F)));
dimGrid_CopyBlockToGPU_kernel3[1]=1;
dimGrid_CopyBlockToGPU_kernel3[2]=1;
int dimBlock_CopyBlockToGPU_kernel3[3];
dimBlock_CopyBlockToGPU_kernel3[0]=64;
dimBlock_CopyBlockToGPU_kernel3[1]=1;
dimBlock_CopyBlockToGPU_kernel3[2]=1;
gpuNumBlocks=((int)ceil((((float)((dim_i*dim_j)*dim_k))/64.0F)));
gpuNumThreads=64;
totalGpuNumThreads=(((int)ceil((((float)((dim_i*dim_j)*dim_k))/64.0F)))*64);
if ((dim_i==1))
{
/* be smart and don't have an inner loop from 0 to 0 */
/*    #pragma omp parallel for  */
for (k=0; k<dim_k; k ++ )
{
for (j=0; j<dim_j; j ++ )
{
int read_ijk;
read_ijk=((read_i+read_ghost)+(((j+read_ghost)+read_j)*read_jStride))+(((k+read_ghost)+read_k)*read_kStride);
int write_ijk;
write_ijk=j+(k*dim_j);
read_buffer[write_ijk]=read[read_ijk];
}
}
HI_set_async(asyncID);
if ((HI_get_device_address(read_buffer, ((void * *)( & gpu__read_buffer)), asyncID)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, read_buffer, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  update async(asyncID) device(read_buffer[0:(dim_j*dim_k)]) \n");
exit(1);
}
gpuBytes=(sizeof (double)*(dim_j*dim_k));
HI_memcpy_async(gpu__read_buffer, read_buffer, gpuBytes, HI_MemcpyHostToDevice, 0, asyncID);

HI_set_async(asyncID);
if ((HI_get_device_address(read_buffer, ((void * *)( & gpu__read_buffer)), asyncID)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, read_buffer, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop async(asyncID) num_workers(64) gang copyin(dim_j, dim_k, write_ghost, write_i, write_j, write_jStride, write_k, write_kStride) present(read_buffer, write) private(j, k) num_gangs(dim_k) \n");
exit(1);
}
if ((HI_get_device_address(write, ((void * *)( & gpu__write)), asyncID)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, write, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop async(asyncID) num_workers(64) gang copyin(dim_j, dim_k, write_ghost, write_i, write_j, write_jStride, write_k, write_kStride) present(read_buffer, write) private(j, k) num_gangs(dim_k) \n");
exit(1);
}
HI_register_kernel_arg("CopyBlockToGPU_kernel0",0,sizeof(void*),( & gpu__read_buffer));
HI_register_kernel_arg("CopyBlockToGPU_kernel0",1,sizeof(void*),( & gpu__write));
HI_register_kernel_arg("CopyBlockToGPU_kernel0",2,sizeof(void*),( & dim_j));
HI_register_kernel_arg("CopyBlockToGPU_kernel0",3,sizeof(void*),( & dim_k));
HI_register_kernel_arg("CopyBlockToGPU_kernel0",4,sizeof(void*),( & write_ghost));
HI_register_kernel_arg("CopyBlockToGPU_kernel0",5,sizeof(void*),( & write_i));
HI_register_kernel_arg("CopyBlockToGPU_kernel0",6,sizeof(void*),( & write_j));
HI_register_kernel_arg("CopyBlockToGPU_kernel0",7,sizeof(void*),( & write_jStride));
HI_register_kernel_arg("CopyBlockToGPU_kernel0",8,sizeof(void*),( & write_k));
HI_register_kernel_arg("CopyBlockToGPU_kernel0",9,sizeof(void*),( & write_kStride));
HI_kernel_call("CopyBlockToGPU_kernel0",dimGrid_CopyBlockToGPU_kernel0,dimBlock_CopyBlockToGPU_kernel0,asyncID);
;
gpuNumBlocks=dim_k;
}
else
{
if ((dim_j==1))
{
/* don't have a 0..0 loop */
/*    #pragma omp parallel for */
for (k=0; k<dim_k; k ++ )
{
for (i=0; i<dim_i; i ++ )
{
int read_ijk;
read_ijk=(((i+read_ghost)+read_i)+((read_j+read_ghost)*read_jStride))+(((k+read_ghost)+read_k)*read_kStride);
int write_ijk;
write_ijk=i+(k*dim_i);
read_buffer[write_ijk]=read[read_ijk];
}
}
HI_set_async(asyncID);
if ((HI_get_device_address(read_buffer, ((void * *)( & gpu__read_buffer)), asyncID)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, read_buffer, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  update async(asyncID) device(read_buffer[0:(dim_i*dim_k)]) \n");
exit(1);
}
gpuBytes=(sizeof (double)*(dim_i*dim_k));
HI_memcpy_async(gpu__read_buffer, read_buffer, gpuBytes, HI_MemcpyHostToDevice, 0, asyncID);

HI_set_async(asyncID);
if ((HI_get_device_address(read_buffer, ((void * *)( & gpu__read_buffer)), asyncID)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, read_buffer, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop async(asyncID) num_workers(64) gang copyin(dim_i, dim_k, write_ghost, write_i, write_j, write_jStride, write_k, write_kStride) present(read_buffer, write) private(i, k) num_gangs(dim_k) \n");
exit(1);
}
if ((HI_get_device_address(write, ((void * *)( & gpu__write)), asyncID)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, write, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop async(asyncID) num_workers(64) gang copyin(dim_i, dim_k, write_ghost, write_i, write_j, write_jStride, write_k, write_kStride) present(read_buffer, write) private(i, k) num_gangs(dim_k) \n");
exit(1);
}
HI_register_kernel_arg("CopyBlockToGPU_kernel1",0,sizeof(void*),( & gpu__read_buffer));
HI_register_kernel_arg("CopyBlockToGPU_kernel1",1,sizeof(void*),( & gpu__write));
HI_register_kernel_arg("CopyBlockToGPU_kernel1",2,sizeof(void*),( & dim_i));
HI_register_kernel_arg("CopyBlockToGPU_kernel1",3,sizeof(void*),( & dim_k));
HI_register_kernel_arg("CopyBlockToGPU_kernel1",4,sizeof(void*),( & write_ghost));
HI_register_kernel_arg("CopyBlockToGPU_kernel1",5,sizeof(void*),( & write_i));
HI_register_kernel_arg("CopyBlockToGPU_kernel1",6,sizeof(void*),( & write_j));
HI_register_kernel_arg("CopyBlockToGPU_kernel1",7,sizeof(void*),( & write_jStride));
HI_register_kernel_arg("CopyBlockToGPU_kernel1",8,sizeof(void*),( & write_k));
HI_register_kernel_arg("CopyBlockToGPU_kernel1",9,sizeof(void*),( & write_kStride));
HI_kernel_call("CopyBlockToGPU_kernel1",dimGrid_CopyBlockToGPU_kernel1,dimBlock_CopyBlockToGPU_kernel1,asyncID);
;
gpuNumBlocks=dim_k;
}
else
{
if ((dim_k==1))
{
/* don't have a 0..0 loop */
/*    #pragma omp parallel for */
for (j=0; j<dim_j; j ++ )
{
for (i=0; i<dim_i; i ++ )
{
int read_ijk;
read_ijk=(((i+read_ghost)+read_i)+(((j+read_ghost)+read_j)*read_jStride))+((read_k+read_ghost)*read_kStride);
int write_ijk;
write_ijk=i+(j*dim_i);
read_buffer[write_ijk]=read[read_ijk];
}
}
HI_set_async(asyncID);
if ((HI_get_device_address(read_buffer, ((void * *)( & gpu__read_buffer)), asyncID)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, read_buffer, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  update async(asyncID) device(read_buffer[0:(dim_i*dim_j)]) \n");
exit(1);
}
gpuBytes=(sizeof (double)*(dim_i*dim_j));
HI_memcpy_async(gpu__read_buffer, read_buffer, gpuBytes, HI_MemcpyHostToDevice, 0, asyncID);

HI_set_async(asyncID);
if ((HI_get_device_address(read_buffer, ((void * *)( & gpu__read_buffer)), asyncID)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, read_buffer, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop async(asyncID) num_workers(64) gang copyin(dim_i, dim_j, write_ghost, write_i, write_j, write_jStride, write_k, write_kStride) present(read_buffer, write) private(i, j) num_gangs(dim_j) \n");
exit(1);
}
if ((HI_get_device_address(write, ((void * *)( & gpu__write)), asyncID)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, write, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop async(asyncID) num_workers(64) gang copyin(dim_i, dim_j, write_ghost, write_i, write_j, write_jStride, write_k, write_kStride) present(read_buffer, write) private(i, j) num_gangs(dim_j) \n");
exit(1);
}
HI_register_kernel_arg("CopyBlockToGPU_kernel2",0,sizeof(void*),( & gpu__read_buffer));
HI_register_kernel_arg("CopyBlockToGPU_kernel2",1,sizeof(void*),( & gpu__write));
HI_register_kernel_arg("CopyBlockToGPU_kernel2",2,sizeof(void*),( & dim_i));
HI_register_kernel_arg("CopyBlockToGPU_kernel2",3,sizeof(void*),( & dim_j));
HI_register_kernel_arg("CopyBlockToGPU_kernel2",4,sizeof(void*),( & write_ghost));
HI_register_kernel_arg("CopyBlockToGPU_kernel2",5,sizeof(void*),( & write_i));
HI_register_kernel_arg("CopyBlockToGPU_kernel2",6,sizeof(void*),( & write_j));
HI_register_kernel_arg("CopyBlockToGPU_kernel2",7,sizeof(void*),( & write_jStride));
HI_register_kernel_arg("CopyBlockToGPU_kernel2",8,sizeof(void*),( & write_k));
HI_register_kernel_arg("CopyBlockToGPU_kernel2",9,sizeof(void*),( & write_kStride));
HI_kernel_call("CopyBlockToGPU_kernel2",dimGrid_CopyBlockToGPU_kernel2,dimBlock_CopyBlockToGPU_kernel2,asyncID);
;
gpuNumBlocks=dim_j;
}
else
{
/*    #pragma omp parallel for */
for (k=0; k<dim_k; k ++ )
{
for (j=0; j<dim_j; j ++ )
{
for (i=0; i<dim_i; i ++ )
{
int read_ijk;
read_ijk=(((i+read_ghost)+read_i)+(((j+read_ghost)+read_j)*read_jStride))+(((k+read_ghost)+read_k)*read_kStride);
int write_ijk;
write_ijk=(i+(j*dim_i))+((k*dim_i)*dim_j);
read_buffer[write_ijk]=read[read_ijk];
}
}
}
HI_set_async(asyncID);
if ((HI_get_device_address(read_buffer, ((void * *)( & gpu__read_buffer)), asyncID)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, read_buffer, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  update async(asyncID) device(read_buffer[0:(dim_i*(dim_j*dim_k))]) \n");
exit(1);
}
gpuBytes=(sizeof (double)*((dim_i*dim_j)*dim_k));
HI_memcpy_async(gpu__read_buffer, read_buffer, gpuBytes, HI_MemcpyHostToDevice, 0, asyncID);

HI_set_async(asyncID);
if ((HI_get_device_address(read_buffer, ((void * *)( & gpu__read_buffer)), asyncID)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, read_buffer, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop async(asyncID) num_workers(64) collapse(3) gang worker copyin(dim_i, dim_j, dim_k, write_ghost, write_i, write_j, write_jStride, write_k, write_kStride) present(read_buffer, write) private(_ti_100_0, i, j, k) num_gangs(((int)ceil((((float)((dim_i*dim_j)*dim_k))/64.0F)))) \n");
exit(1);
}
if ((HI_get_device_address(write, ((void * *)( & gpu__write)), asyncID)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, write, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop async(asyncID) num_workers(64) collapse(3) gang worker copyin(dim_i, dim_j, dim_k, write_ghost, write_i, write_j, write_jStride, write_k, write_kStride) present(read_buffer, write) private(_ti_100_0, i, j, k) num_gangs(((int)ceil((((float)((dim_i*dim_j)*dim_k))/64.0F)))) \n");
exit(1);
}
HI_register_kernel_arg("CopyBlockToGPU_kernel3",0,sizeof(void*),( & gpu__read_buffer));
HI_register_kernel_arg("CopyBlockToGPU_kernel3",1,sizeof(void*),( & gpu__write));
HI_register_kernel_arg("CopyBlockToGPU_kernel3",2,sizeof(void*),( & dim_i));
HI_register_kernel_arg("CopyBlockToGPU_kernel3",3,sizeof(void*),( & dim_j));
HI_register_kernel_arg("CopyBlockToGPU_kernel3",4,sizeof(void*),( & dim_k));
HI_register_kernel_arg("CopyBlockToGPU_kernel3",5,sizeof(void*),( & write_ghost));
HI_register_kernel_arg("CopyBlockToGPU_kernel3",6,sizeof(void*),( & write_i));
HI_register_kernel_arg("CopyBlockToGPU_kernel3",7,sizeof(void*),( & write_j));
HI_register_kernel_arg("CopyBlockToGPU_kernel3",8,sizeof(void*),( & write_jStride));
HI_register_kernel_arg("CopyBlockToGPU_kernel3",9,sizeof(void*),( & write_k));
HI_register_kernel_arg("CopyBlockToGPU_kernel3",10,sizeof(void*),( & write_kStride));
HI_kernel_call("CopyBlockToGPU_kernel3",dimGrid_CopyBlockToGPU_kernel3,dimBlock_CopyBlockToGPU_kernel3,asyncID);
;
gpuNumBlocks=((int)ceil((((float)((dim_i*dim_j)*dim_k))/64.0F)));
}
}
}
acc_wait(asyncID);
return ;
}

/* Transfer read buffer from GPU and then copy it to writer buffer. */
void CopyBlockFromGPU(level_type * level, int id, blockCopy_type * block)
{
/* copy 3D array from read_i,j,k of read[] to write_i,j,k in write[] */
int dim_i;
int _ti_100_0;
double * gpu__read;
dim_i=block->dim.i;
int dim_j;
dim_j=block->dim.j;
int dim_k;
dim_k=block->dim.k;
int read_i;
read_i=block->read.i;
int read_j;
read_j=block->read.j;
int read_k;
read_k=block->read.k;
int read_jStride;
read_jStride=block->read.jStride;
int read_kStride;
read_kStride=block->read.kStride;
int write_i;
write_i=block->write.i;
int write_j;
write_j=block->write.j;
int write_k;
write_k=block->write.k;
int write_jStride;
write_jStride=block->write.jStride;
int write_kStride;
write_kStride=block->write.kStride;
double * read;
read=block->read.ptr;
double * write;
write=block->write.ptr;
int read_ghost = 0;
int write_ghost = 0;
int i;
int j;
int k;
int asyncID;
asyncID=level->num_my_boxes+1;
if ((block->read.box>=0))
{
read=level->my_boxes[block->read.box].vectors[id];
/* + level->my_boxes[ block->read.box].ghosts(1+level->my_boxes[ block->read.box].jStride+level->my_boxes[ block->read.box].kStride); */
read_ghost=level->my_boxes[block->read.box].ghosts;
}
if ((block->write.box>=0))
{
write=level->my_boxes[block->write.box].vectors[id];
/* + level->my_boxes[block->write.box].ghosts(1+level->my_boxes[block->write.box].jStride+level->my_boxes[block->write.box].kStride); */
write_ghost=level->my_boxes[block->write.box].ghosts;
}
int dimGrid_CopyBlockFromGPU_kernel0[3];
dimGrid_CopyBlockFromGPU_kernel0[0]=dim_k;
dimGrid_CopyBlockFromGPU_kernel0[1]=1;
dimGrid_CopyBlockFromGPU_kernel0[2]=1;
int dimBlock_CopyBlockFromGPU_kernel0[3];
dimBlock_CopyBlockFromGPU_kernel0[0]=64;
dimBlock_CopyBlockFromGPU_kernel0[1]=1;
dimBlock_CopyBlockFromGPU_kernel0[2]=1;
gpuNumBlocks=dim_k;
gpuNumThreads=64;
totalGpuNumThreads=(64*dim_k);
int dimGrid_CopyBlockFromGPU_kernel1[3];
dimGrid_CopyBlockFromGPU_kernel1[0]=dim_k;
dimGrid_CopyBlockFromGPU_kernel1[1]=1;
dimGrid_CopyBlockFromGPU_kernel1[2]=1;
int dimBlock_CopyBlockFromGPU_kernel1[3];
dimBlock_CopyBlockFromGPU_kernel1[0]=64;
dimBlock_CopyBlockFromGPU_kernel1[1]=1;
dimBlock_CopyBlockFromGPU_kernel1[2]=1;
gpuNumBlocks=dim_k;
gpuNumThreads=64;
totalGpuNumThreads=(64*dim_k);
int dimGrid_CopyBlockFromGPU_kernel2[3];
dimGrid_CopyBlockFromGPU_kernel2[0]=dim_j;
dimGrid_CopyBlockFromGPU_kernel2[1]=1;
dimGrid_CopyBlockFromGPU_kernel2[2]=1;
int dimBlock_CopyBlockFromGPU_kernel2[3];
dimBlock_CopyBlockFromGPU_kernel2[0]=64;
dimBlock_CopyBlockFromGPU_kernel2[1]=1;
dimBlock_CopyBlockFromGPU_kernel2[2]=1;
gpuNumBlocks=dim_j;
gpuNumThreads=64;
totalGpuNumThreads=(64*dim_j);
int dimGrid_CopyBlockFromGPU_kernel3[3];
dimGrid_CopyBlockFromGPU_kernel3[0]=((int)ceil((((float)((dim_i*dim_j)*dim_k))/64.0F)));
dimGrid_CopyBlockFromGPU_kernel3[1]=1;
dimGrid_CopyBlockFromGPU_kernel3[2]=1;
int dimBlock_CopyBlockFromGPU_kernel3[3];
dimBlock_CopyBlockFromGPU_kernel3[0]=64;
dimBlock_CopyBlockFromGPU_kernel3[1]=1;
dimBlock_CopyBlockFromGPU_kernel3[2]=1;
gpuNumBlocks=((int)ceil((((float)((dim_i*dim_j)*dim_k))/64.0F)));
gpuNumThreads=64;
totalGpuNumThreads=(((int)ceil((((float)((dim_i*dim_j)*dim_k))/64.0F)))*64);
if ((dim_i==1))
{
/* be smart and don't have an inner loop from 0 to 0 */
if ((HI_get_device_address(read, ((void * *)( & gpu__read)), DEFAULT_QUEUE)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, read, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop num_workers(64) gang copyin(dim_j, dim_k, read_ghost, read_i, read_j, read_jStride, read_k, read_kStride) present(read, read_buffer) private(j, k) num_gangs(dim_k) \n");
exit(1);
}
if ((HI_get_device_address(read_buffer, ((void * *)( & gpu__read_buffer)), DEFAULT_QUEUE)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, read_buffer, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop num_workers(64) gang copyin(dim_j, dim_k, read_ghost, read_i, read_j, read_jStride, read_k, read_kStride) present(read, read_buffer) private(j, k) num_gangs(dim_k) \n");
exit(1);
}
HI_register_kernel_arg("CopyBlockFromGPU_kernel0",0,sizeof(void*),( & gpu__read));
HI_register_kernel_arg("CopyBlockFromGPU_kernel0",1,sizeof(void*),( & gpu__read_buffer));
HI_register_kernel_arg("CopyBlockFromGPU_kernel0",2,sizeof(void*),( & dim_j));
HI_register_kernel_arg("CopyBlockFromGPU_kernel0",3,sizeof(void*),( & dim_k));
HI_register_kernel_arg("CopyBlockFromGPU_kernel0",4,sizeof(void*),( & read_ghost));
HI_register_kernel_arg("CopyBlockFromGPU_kernel0",5,sizeof(void*),( & read_i));
HI_register_kernel_arg("CopyBlockFromGPU_kernel0",6,sizeof(void*),( & read_j));
HI_register_kernel_arg("CopyBlockFromGPU_kernel0",7,sizeof(void*),( & read_jStride));
HI_register_kernel_arg("CopyBlockFromGPU_kernel0",8,sizeof(void*),( & read_k));
HI_register_kernel_arg("CopyBlockFromGPU_kernel0",9,sizeof(void*),( & read_kStride));
HI_kernel_call("CopyBlockFromGPU_kernel0",dimGrid_CopyBlockFromGPU_kernel0,dimBlock_CopyBlockFromGPU_kernel0);
HI_synchronize();
gpuNumBlocks=dim_k;

if ((HI_get_device_address(read_buffer, ((void * *)( & gpu__read_buffer)), DEFAULT_QUEUE)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, read_buffer, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  update host(read_buffer[0:(dim_j*dim_k)]) \n");
exit(1);
}
gpuBytes=(sizeof (double)*(dim_j*dim_k));
HI_memcpy(read_buffer, gpu__read_buffer, gpuBytes, HI_MemcpyDeviceToHost, 0);
for (k=0; k<dim_k; k ++ )
{
for (j=0; j<dim_j; j ++ )
{
int read_ijk;
read_ijk=j+(k*dim_j);
int write_ijk;
write_ijk=((write_i+write_ghost)+(((j+write_ghost)+write_j)*write_jStride))+(((k+write_ghost)+write_k)*write_kStride);
write[write_ijk]=read_buffer[read_ijk];
}
}
}
else
{
if ((dim_j==1))
{
/* don't have a 0..0 loop */
if ((HI_get_device_address(read, ((void * *)( & gpu__read)), DEFAULT_QUEUE)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, read, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop num_workers(64) gang copyin(dim_i, dim_k, read_ghost, read_i, read_j, read_jStride, read_k, read_kStride) present(read, read_buffer) private(i, k) num_gangs(dim_k) \n");
exit(1);
}
if ((HI_get_device_address(read_buffer, ((void * *)( & gpu__read_buffer)), DEFAULT_QUEUE)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, read_buffer, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop num_workers(64) gang copyin(dim_i, dim_k, read_ghost, read_i, read_j, read_jStride, read_k, read_kStride) present(read, read_buffer) private(i, k) num_gangs(dim_k) \n");
exit(1);
}
HI_register_kernel_arg("CopyBlockFromGPU_kernel1",0,sizeof(void*),( & gpu__read));
HI_register_kernel_arg("CopyBlockFromGPU_kernel1",1,sizeof(void*),( & gpu__read_buffer));
HI_register_kernel_arg("CopyBlockFromGPU_kernel1",2,sizeof(void*),( & dim_i));
HI_register_kernel_arg("CopyBlockFromGPU_kernel1",3,sizeof(void*),( & dim_k));
HI_register_kernel_arg("CopyBlockFromGPU_kernel1",4,sizeof(void*),( & read_ghost));
HI_register_kernel_arg("CopyBlockFromGPU_kernel1",5,sizeof(void*),( & read_i));
HI_register_kernel_arg("CopyBlockFromGPU_kernel1",6,sizeof(void*),( & read_j));
HI_register_kernel_arg("CopyBlockFromGPU_kernel1",7,sizeof(void*),( & read_jStride));
HI_register_kernel_arg("CopyBlockFromGPU_kernel1",8,sizeof(void*),( & read_k));
HI_register_kernel_arg("CopyBlockFromGPU_kernel1",9,sizeof(void*),( & read_kStride));
HI_kernel_call("CopyBlockFromGPU_kernel1",dimGrid_CopyBlockFromGPU_kernel1,dimBlock_CopyBlockFromGPU_kernel1);
HI_synchronize();
gpuNumBlocks=dim_k;

if ((HI_get_device_address(read_buffer, ((void * *)( & gpu__read_buffer)), DEFAULT_QUEUE)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, read_buffer, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  update host(read_buffer[0:(dim_i*dim_k)]) \n");
exit(1);
}
gpuBytes=(sizeof (double)*(dim_i*dim_k));
HI_memcpy(read_buffer, gpu__read_buffer, gpuBytes, HI_MemcpyDeviceToHost, 0);
for (k=0; k<dim_k; k ++ )
{
for (i=0; i<dim_i; i ++ )
{
int read_ijk;
read_ijk=i+(k*dim_i);
int write_ijk;
write_ijk=(((i+write_ghost)+write_i)+((write_j+write_ghost)*write_jStride))+(((k+write_ghost)+write_k)*write_kStride);
write[write_ijk]=read_buffer[read_ijk];
}
}
}
else
{
if ((dim_k==1))
{
/* don't have a 0..0 loop */
if ((HI_get_device_address(read, ((void * *)( & gpu__read)), DEFAULT_QUEUE)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, read, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop num_workers(64) gang copyin(dim_i, dim_j, read_ghost, read_i, read_j, read_jStride, read_k, read_kStride) present(read, read_buffer) private(i, j) num_gangs(dim_j) \n");
exit(1);
}
if ((HI_get_device_address(read_buffer, ((void * *)( & gpu__read_buffer)), DEFAULT_QUEUE)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, read_buffer, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop num_workers(64) gang copyin(dim_i, dim_j, read_ghost, read_i, read_j, read_jStride, read_k, read_kStride) present(read, read_buffer) private(i, j) num_gangs(dim_j) \n");
exit(1);
}
HI_register_kernel_arg("CopyBlockFromGPU_kernel2",0,sizeof(void*),( & gpu__read));
HI_register_kernel_arg("CopyBlockFromGPU_kernel2",1,sizeof(void*),( & gpu__read_buffer));
HI_register_kernel_arg("CopyBlockFromGPU_kernel2",2,sizeof(void*),( & dim_i));
HI_register_kernel_arg("CopyBlockFromGPU_kernel2",3,sizeof(void*),( & dim_j));
HI_register_kernel_arg("CopyBlockFromGPU_kernel2",4,sizeof(void*),( & read_ghost));
HI_register_kernel_arg("CopyBlockFromGPU_kernel2",5,sizeof(void*),( & read_i));
HI_register_kernel_arg("CopyBlockFromGPU_kernel2",6,sizeof(void*),( & read_j));
HI_register_kernel_arg("CopyBlockFromGPU_kernel2",7,sizeof(void*),( & read_jStride));
HI_register_kernel_arg("CopyBlockFromGPU_kernel2",8,sizeof(void*),( & read_k));
HI_register_kernel_arg("CopyBlockFromGPU_kernel2",9,sizeof(void*),( & read_kStride));
HI_kernel_call("CopyBlockFromGPU_kernel2",dimGrid_CopyBlockFromGPU_kernel2,dimBlock_CopyBlockFromGPU_kernel2);
HI_synchronize();
gpuNumBlocks=dim_j;

if ((HI_get_device_address(read_buffer, ((void * *)( & gpu__read_buffer)), DEFAULT_QUEUE)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, read_buffer, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  update host(read_buffer[0:(dim_i*dim_j)]) \n");
exit(1);
}
gpuBytes=(sizeof (double)*(dim_i*dim_j));
HI_memcpy(read_buffer, gpu__read_buffer, gpuBytes, HI_MemcpyDeviceToHost, 0);
for (j=0; j<dim_j; j ++ )
{
for (i=0; i<dim_i; i ++ )
{
int read_ijk;
read_ijk=i+(j*dim_i);
int write_ijk;
write_ijk=(((i+write_ghost)+write_i)+(((j+write_ghost)+write_j)*write_jStride))+((write_k+write_ghost)*write_kStride);
write[write_ijk]=read_buffer[read_ijk];
}
}
}
else
{
if ((HI_get_device_address(read, ((void * *)( & gpu__read)), DEFAULT_QUEUE)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, read, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop num_workers(64) collapse(3) gang worker copyin(dim_i, dim_j, dim_k, read_ghost, read_i, read_j, read_jStride, read_k, read_kStride) present(read, read_buffer) private(_ti_100_0, i, j, k) num_gangs(((int)ceil((((float)((dim_i*dim_j)*dim_k))/64.0F)))) \n");
exit(1);
}
if ((HI_get_device_address(read_buffer, ((void * *)( & gpu__read_buffer)), DEFAULT_QUEUE)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, read_buffer, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  parallel loop num_workers(64) collapse(3) gang worker copyin(dim_i, dim_j, dim_k, read_ghost, read_i, read_j, read_jStride, read_k, read_kStride) present(read, read_buffer) private(_ti_100_0, i, j, k) num_gangs(((int)ceil((((float)((dim_i*dim_j)*dim_k))/64.0F)))) \n");
exit(1);
}
HI_register_kernel_arg("CopyBlockFromGPU_kernel3",0,sizeof(void*),( & gpu__read));
HI_register_kernel_arg("CopyBlockFromGPU_kernel3",1,sizeof(void*),( & gpu__read_buffer));
HI_register_kernel_arg("CopyBlockFromGPU_kernel3",2,sizeof(void*),( & dim_i));
HI_register_kernel_arg("CopyBlockFromGPU_kernel3",3,sizeof(void*),( & dim_j));
HI_register_kernel_arg("CopyBlockFromGPU_kernel3",4,sizeof(void*),( & dim_k));
HI_register_kernel_arg("CopyBlockFromGPU_kernel3",5,sizeof(void*),( & read_ghost));
HI_register_kernel_arg("CopyBlockFromGPU_kernel3",6,sizeof(void*),( & read_i));
HI_register_kernel_arg("CopyBlockFromGPU_kernel3",7,sizeof(void*),( & read_j));
HI_register_kernel_arg("CopyBlockFromGPU_kernel3",8,sizeof(void*),( & read_jStride));
HI_register_kernel_arg("CopyBlockFromGPU_kernel3",9,sizeof(void*),( & read_k));
HI_register_kernel_arg("CopyBlockFromGPU_kernel3",10,sizeof(void*),( & read_kStride));
HI_kernel_call("CopyBlockFromGPU_kernel3",dimGrid_CopyBlockFromGPU_kernel3,dimBlock_CopyBlockFromGPU_kernel3);
HI_synchronize();
gpuNumBlocks=((int)ceil((((float)((dim_i*dim_j)*dim_k))/64.0F)));

if ((HI_get_device_address(read_buffer, ((void * *)( & gpu__read_buffer)), DEFAULT_QUEUE)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, read_buffer, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  update host(read_buffer[0:(dim_i*(dim_j*dim_k))]) \n");
exit(1);
}
gpuBytes=(sizeof (double)*((dim_i*dim_j)*dim_k));
HI_memcpy(read_buffer, gpu__read_buffer, gpuBytes, HI_MemcpyDeviceToHost, 0);
for (k=0; k<dim_k; k ++ )
{
for (j=0; j<dim_j; j ++ )
{
for (i=0; i<dim_i; i ++ )
{
int read_ijk;
read_ijk=(i+(j*dim_i))+((k*dim_i)*dim_j);
int write_ijk;
write_ijk=(((i+write_ghost)+write_i)+(((j+write_ghost)+write_j)*write_jStride))+(((k+write_ghost)+write_k)*write_kStride);
write[write_ijk]=read_buffer[read_ijk];
}
}
}
}
}
}
return ;
}

static inline void CopyBlock(level_type * level, int id, blockCopy_type * block)
{
/* copy 3D array from read_i,j,k of read[] to write_i,j,k in write[] */
int dim_i;
dim_i=block->dim.i;
int dim_j;
dim_j=block->dim.j;
int dim_k;
dim_k=block->dim.k;
int read_i;
read_i=block->read.i;
int read_j;
read_j=block->read.j;
int read_k;
read_k=block->read.k;
int read_jStride;
read_jStride=block->read.jStride;
int read_kStride;
read_kStride=block->read.kStride;
int write_i;
write_i=block->write.i;
int write_j;
write_j=block->write.j;
int write_k;
write_k=block->write.k;
int write_jStride;
write_jStride=block->write.jStride;
int write_kStride;
write_kStride=block->write.kStride;
double * read;
read=block->read.ptr;
double * write;
write=block->write.ptr;
if ((block->read.box>=0))
{
read=(level->my_boxes[block->read.box].vectors[id]+(level->my_boxes[block->read.box].ghosts*((1+level->my_boxes[block->read.box].jStride)+level->my_boxes[block->read.box].kStride)));
}
if ((block->write.box>=0))
{
write=(level->my_boxes[block->write.box].vectors[id]+(level->my_boxes[block->write.box].ghosts*((1+level->my_boxes[block->write.box].jStride)+level->my_boxes[block->write.box].kStride)));
}
int i;
int j;
int k;
if ((dim_i==1))
{
/* be smart and don't have an inner loop from 0 to 0 */
for (k=0; k<dim_k; k ++ )
{
for (j=0; j<dim_j; j ++ )
{
int read_ijk;
read_ijk=(read_i+((j+read_j)*read_jStride))+((k+read_k)*read_kStride);
int write_ijk;
write_ijk=(write_i+((j+write_j)*write_jStride))+((k+write_k)*write_kStride);
write[write_ijk]=read[read_ijk];
}
}
}
else
{
if ((dim_j==1))
{
/* don't have a 0..0 loop */
for (k=0; k<dim_k; k ++ )
{
for (i=0; i<dim_i; i ++ )
{
int read_ijk;
read_ijk=((i+read_i)+(read_j*read_jStride))+((k+read_k)*read_kStride);
int write_ijk;
write_ijk=((i+write_i)+(write_j*write_jStride))+((k+write_k)*write_kStride);
write[write_ijk]=read[read_ijk];
}
}
}
else
{
if ((dim_k==1))
{
/* don't have a 0..0 loop */
for (j=0; j<dim_j; j ++ )
{
for (i=0; i<dim_i; i ++ )
{
int read_ijk;
read_ijk=((i+read_i)+((j+read_j)*read_jStride))+(read_k*read_kStride);
int write_ijk;
write_ijk=((i+write_i)+((j+write_j)*write_jStride))+(write_k*write_kStride);
write[write_ijk]=read[read_ijk];
}
}
}
else
{
if ((dim_i==4))
{
/* be smart and don't have an inner loop from 0 to 3 */
for (k=0; k<dim_k; k ++ )
{
for (j=0; j<dim_j; j ++ )
{
int read_ijk;
read_ijk=(read_i+((j+read_j)*read_jStride))+((k+read_k)*read_kStride);
int write_ijk;
write_ijk=(write_i+((j+write_j)*write_jStride))+((k+write_k)*write_kStride);
write[(write_ijk+0)]=read[(read_ijk+0)];
write[(write_ijk+1)]=read[(read_ijk+1)];
write[(write_ijk+2)]=read[(read_ijk+2)];
write[(write_ijk+3)]=read[(read_ijk+3)];
}
}
}
else
{
for (k=0; k<dim_k; k ++ )
{
for (j=0; j<dim_j; j ++ )
{
for (i=0; i<dim_i; i ++ )
{
int read_ijk;
read_ijk=((i+read_i)+((j+read_j)*read_jStride))+((k+read_k)*read_kStride);
int write_ijk;
write_ijk=((i+write_i)+((j+write_j)*write_jStride))+((k+write_k)*write_kStride);
write[write_ijk]=read[read_ijk];
}
}
}
}
}
}
}
return ;
}

/* ------------------------------------------------------------------------------------------------------------------------------ */
/* static inline void IncrementBlock(level_typelevel, int id, double prescale, blockCopy_type *block){ */
void IncrementBlock(level_type * level, int id, double prescale, blockCopy_type * block)
{
/* copy 3D array from read_i,j,k of read[] to write_i,j,k in write[] */
int dim_i;
dim_i=block->dim.i;
int dim_j;
dim_j=block->dim.j;
int dim_k;
dim_k=block->dim.k;
int read_i;
read_i=block->read.i;
int read_j;
read_j=block->read.j;
int read_k;
read_k=block->read.k;
int read_jStride;
read_jStride=block->read.jStride;
int read_kStride;
read_kStride=block->read.kStride;
int write_i;
write_i=block->write.i;
int write_j;
write_j=block->write.j;
int write_k;
write_k=block->write.k;
int write_jStride;
write_jStride=block->write.jStride;
int write_kStride;
write_kStride=block->write.kStride;
int i;
int j;
int k;
double * read;
read=block->read.ptr;
double * write;
write=block->write.ptr;
if ((block->read.box>=0))
{
read=(level->my_boxes[block->read.box].vectors[id]+(level->my_boxes[block->read.box].ghosts*((1+level->my_boxes[block->read.box].jStride)+level->my_boxes[block->read.box].kStride)));
read_jStride=level->my_boxes[block->read.box].jStride;
read_kStride=level->my_boxes[block->read.box].kStride;
}
if ((block->write.box>=0))
{
write=(level->my_boxes[block->write.box].vectors[id]+(level->my_boxes[block->write.box].ghosts*((1+level->my_boxes[block->write.box].jStride)+level->my_boxes[block->write.box].kStride)));
write_jStride=level->my_boxes[block->write.box].jStride;
write_kStride=level->my_boxes[block->write.box].kStride;
}
for (k=0; k<dim_k; k ++ )
{
for (j=0; j<dim_j; j ++ )
{
for (i=0; i<dim_i; i ++ )
{
int read_ijk;
read_ijk=((i+read_i)+((j+read_j)*read_jStride))+((k+read_k)*read_kStride);
int write_ijk;
write_ijk=((i+write_i)+((j+write_j)*write_jStride))+((k+write_k)*write_kStride);
write[write_ijk]=((prescale*write[write_ijk])+read[read_ijk]);
/* CAREFUL !!!  you must guarantee you zero'd the MPI buffers(write[]) and destination boxes at some point to avoid 0.0NaN or 0.0*inf */
}
}
}
return ;
}

