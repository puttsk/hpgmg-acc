
#ifndef __CUDA_KERNELHEADER__ 

#define __CUDA_KERNELHEADER__ 

/********************************************/
/* Added codes for OpenACC2CUDA translation */
/********************************************/
#ifdef __cplusplus
#define restrict __restrict__
#endif
#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#ifndef DBL_MAX
#define DBL_MAX 1.7976931348623158e+308
#endif
#ifndef DBL_MIN
#define DBL_MIN 2.2250738585072014e-308
#endif
#ifndef FLT_MAX
#define FLT_MAX 3.402823466e+38
#endif
#ifndef FLT_MIN
#define FLT_MIN 1.175494351e-38
#endif

#endif


extern "C" __global__ void CopyBlockLocal_kernel0(double * read, double * write, int dim_j, int dim_k, int read_ghost, int read_i, int read_j, int read_jStride, int read_k, int read_kStride, int write_ghost, int write_i, int write_j, int write_jStride, int write_k, int write_kStride)
{
int lgpriv__j;
int lgpriv__k;
int _ti_100_201;
lgpriv__k=blockIdx.x;
if (lgpriv__k<dim_k)
{
_ti_100_201=threadIdx.x;
{
for (lgpriv__j=(_ti_100_201+0); lgpriv__j<dim_j; (lgpriv__j+=64))
{
int read_ijk;
read_ijk=((read_i+read_ghost)+(((lgpriv__j+read_ghost)+read_j)*read_jStride))+(((lgpriv__k+read_ghost)+read_k)*read_kStride);
int write_ijk;
write_ijk=((write_i+write_ghost)+(((lgpriv__j+write_ghost)+write_j)*write_jStride))+(((lgpriv__k+write_ghost)+write_k)*write_kStride);
write[write_ijk]=read[read_ijk];
}
}
}
}

extern "C" __global__ void CopyBlockLocal_kernel1(double * read, double * write, int dim_i, int dim_k, int read_ghost, int read_i, int read_j, int read_jStride, int read_k, int read_kStride, int write_ghost, int write_i, int write_j, int write_jStride, int write_k, int write_kStride)
{
int lgpriv__i;
int lgpriv__k;
int _ti_100_201;
lgpriv__k=blockIdx.x;
if (lgpriv__k<dim_k)
{
_ti_100_201=threadIdx.x;
{
for (lgpriv__i=(_ti_100_201+0); lgpriv__i<dim_i; (lgpriv__i+=64))
{
int read_ijk;
read_ijk=(((lgpriv__i+read_ghost)+read_i)+((read_j+read_ghost)*read_jStride))+(((lgpriv__k+read_ghost)+read_k)*read_kStride);
int write_ijk;
write_ijk=(((lgpriv__i+write_ghost)+write_i)+((write_j+write_ghost)*write_jStride))+(((lgpriv__k+write_ghost)+write_k)*write_kStride);
write[write_ijk]=read[read_ijk];
}
}
}
}

extern "C" __global__ void CopyBlockLocal_kernel2(double * read, double * write, int dim_i, int dim_j, int read_ghost, int read_i, int read_j, int read_jStride, int read_k, int read_kStride, int write_ghost, int write_i, int write_j, int write_jStride, int write_k, int write_kStride)
{
int lgpriv__i;
int lgpriv__j;
int _ti_100_201;
lgpriv__j=blockIdx.x;
if (lgpriv__j<dim_j)
{
_ti_100_201=threadIdx.x;
{
for (lgpriv__i=(_ti_100_201+0); lgpriv__i<dim_i; (lgpriv__i+=64))
{
int read_ijk;
read_ijk=(((lgpriv__i+read_ghost)+read_i)+(((lgpriv__j+read_ghost)+read_j)*read_jStride))+((read_k+read_ghost)*read_kStride);
int write_ijk;
write_ijk=(((lgpriv__i+write_ghost)+write_i)+(((lgpriv__j+write_ghost)+write_j)*write_jStride))+((write_k+write_ghost)*write_kStride);
write[write_ijk]=read[read_ijk];
}
}
}
}

extern "C" __global__ void CopyBlockLocal_kernel3(double * read, double * write, int dim_i, int dim_j, int dim_k, int read_ghost, int read_i, int read_j, int read_jStride, int read_k, int read_kStride, int write_ghost, int write_i, int write_j, int write_jStride, int write_k, int write_kStride)
{
int lgpriv__i;
int lgpriv__j;
int lgpriv__k;
int _ti_100_201;
lgpriv__k=blockIdx.x;
if (lgpriv__k<dim_k)
{
_ti_100_201=threadIdx.x;
{
for (lgpriv__i=(_ti_100_201+0); lgpriv__i<dim_i; (lgpriv__i+=64))
{
for (lgpriv__j=0; lgpriv__j<dim_j; lgpriv__j ++ )
{
int read_ijk;
read_ijk=(((lgpriv__i+read_ghost)+read_i)+(((lgpriv__j+read_ghost)+read_j)*read_jStride))+(((lgpriv__k+read_ghost)+read_k)*read_kStride);
int write_ijk;
write_ijk=(((lgpriv__i+write_ghost)+write_i)+(((lgpriv__j+write_ghost)+write_j)*write_jStride))+(((lgpriv__k+write_ghost)+write_k)*write_kStride);
write[write_ijk]=read[read_ijk];
}
}
}
}
}

extern "C" __global__ void CopyBlockToGPU_kernel0(double * read_buffer, double * write, int dim_j, int dim_k, int write_ghost, int write_i, int write_j, int write_jStride, int write_k, int write_kStride)
{
int lgpriv__j;
int lgpriv__k;
int _ti_100_201;
lgpriv__k=blockIdx.x;
if (lgpriv__k<dim_k)
{
_ti_100_201=threadIdx.x;
{
for (lgpriv__j=(_ti_100_201+0); lgpriv__j<dim_j; (lgpriv__j+=64))
{
int read_ijk;
read_ijk=lgpriv__j+(lgpriv__k*dim_j);
int write_ijk;
write_ijk=((write_i+write_ghost)+(((lgpriv__j+write_ghost)+write_j)*write_jStride))+(((lgpriv__k+write_ghost)+write_k)*write_kStride);
write[write_ijk]=read_buffer[read_ijk];
}
}
}
}

extern "C" __global__ void CopyBlockToGPU_kernel1(double * read_buffer, double * write, int dim_i, int dim_k, int write_ghost, int write_i, int write_j, int write_jStride, int write_k, int write_kStride)
{
int lgpriv__i;
int lgpriv__k;
int _ti_100_201;
lgpriv__k=blockIdx.x;
if (lgpriv__k<dim_k)
{
_ti_100_201=threadIdx.x;
{
for (lgpriv__i=(_ti_100_201+0); lgpriv__i<dim_i; (lgpriv__i+=64))
{
int read_ijk;
read_ijk=lgpriv__i+(lgpriv__k*dim_i);
int write_ijk;
write_ijk=(((lgpriv__i+write_ghost)+write_i)+((write_j+write_ghost)*write_jStride))+(((lgpriv__k+write_ghost)+write_k)*write_kStride);
write[write_ijk]=read_buffer[read_ijk];
}
}
}
}

extern "C" __global__ void CopyBlockToGPU_kernel2(double * read_buffer, double * write, int dim_i, int dim_j, int write_ghost, int write_i, int write_j, int write_jStride, int write_k, int write_kStride)
{
int lgpriv__i;
int lgpriv__j;
int _ti_100_201;
lgpriv__j=blockIdx.x;
if (lgpriv__j<dim_j)
{
_ti_100_201=threadIdx.x;
{
for (lgpriv__i=(_ti_100_201+0); lgpriv__i<dim_i; (lgpriv__i+=64))
{
int read_ijk;
read_ijk=lgpriv__i+(lgpriv__j*dim_i);
int write_ijk;
write_ijk=(((lgpriv__i+write_ghost)+write_i)+(((lgpriv__j+write_ghost)+write_j)*write_jStride))+((write_k+write_ghost)*write_kStride);
write[write_ijk]=read_buffer[read_ijk];
}
}
}
}

extern "C" __global__ void CopyBlockToGPU_kernel3(double * read_buffer, double * write, int dim_i, int dim_j, int dim_k, int write_ghost, int write_i, int write_j, int write_jStride, int write_k, int write_kStride)
{
int read_ijk;
int write_ijk;
int lwpriv___ti_100_0;
int lwpriv__i;
int lwpriv__j;
int lwpriv__k;
lwpriv___ti_100_0=(threadIdx.x+(blockIdx.x*64));
if ((lwpriv___ti_100_0<((dim_i*dim_j)*dim_k)))
{
lwpriv__i=(lwpriv___ti_100_0%dim_i);
lwpriv__k=(lwpriv___ti_100_0/dim_i);
lwpriv__j=(lwpriv__k%dim_j);
lwpriv__k=(lwpriv__k/dim_j);
read_ijk=(lwpriv__i+(lwpriv__j*dim_i))+((lwpriv__k*dim_i)*dim_j);
write_ijk=(((lwpriv__i+write_ghost)+write_i)+(((lwpriv__j+write_ghost)+write_j)*write_jStride))+(((lwpriv__k+write_ghost)+write_k)*write_kStride);
write[write_ijk]=read_buffer[read_ijk];
}
}

extern "C" __global__ void CopyBlockFromGPU_kernel0(double * read, double * read_buffer, int dim_j, int dim_k, int read_ghost, int read_i, int read_j, int read_jStride, int read_k, int read_kStride)
{
int lgpriv__j;
int lgpriv__k;
int _ti_100_201;
lgpriv__k=blockIdx.x;
if (lgpriv__k<dim_k)
{
_ti_100_201=threadIdx.x;
{
for (lgpriv__j=(_ti_100_201+0); lgpriv__j<dim_j; (lgpriv__j+=64))
{
int read_ijk;
read_ijk=((read_i+read_ghost)+(((lgpriv__j+read_ghost)+read_j)*read_jStride))+(((lgpriv__k+read_ghost)+read_k)*read_kStride);
int write_ijk;
write_ijk=lgpriv__j+(lgpriv__k*dim_j);
read_buffer[write_ijk]=read[read_ijk];
}
}
}
}

extern "C" __global__ void CopyBlockFromGPU_kernel1(double * read, double * read_buffer, int dim_i, int dim_k, int read_ghost, int read_i, int read_j, int read_jStride, int read_k, int read_kStride)
{
int lgpriv__i;
int lgpriv__k;
int _ti_100_201;
lgpriv__k=blockIdx.x;
if (lgpriv__k<dim_k)
{
_ti_100_201=threadIdx.x;
{
for (lgpriv__i=(_ti_100_201+0); lgpriv__i<dim_i; (lgpriv__i+=64))
{
int read_ijk;
read_ijk=(((lgpriv__i+read_ghost)+read_i)+((read_j+read_ghost)*read_jStride))+(((lgpriv__k+read_ghost)+read_k)*read_kStride);
int write_ijk;
write_ijk=lgpriv__i+(lgpriv__k*dim_i);
read_buffer[write_ijk]=read[read_ijk];
}
}
}
}

extern "C" __global__ void CopyBlockFromGPU_kernel2(double * read, double * read_buffer, int dim_i, int dim_j, int read_ghost, int read_i, int read_j, int read_jStride, int read_k, int read_kStride)
{
int lgpriv__i;
int lgpriv__j;
int _ti_100_201;
lgpriv__j=blockIdx.x;
if (lgpriv__j<dim_j)
{
_ti_100_201=threadIdx.x;
{
for (lgpriv__i=(_ti_100_201+0); lgpriv__i<dim_i; (lgpriv__i+=64))
{
int read_ijk;
read_ijk=(((lgpriv__i+read_ghost)+read_i)+(((lgpriv__j+read_ghost)+read_j)*read_jStride))+((read_k+read_ghost)*read_kStride);
int write_ijk;
write_ijk=lgpriv__i+(lgpriv__j*dim_i);
read_buffer[write_ijk]=read[read_ijk];
}
}
}
}

extern "C" __global__ void CopyBlockFromGPU_kernel3(double * read, double * read_buffer, int dim_i, int dim_j, int dim_k, int read_ghost, int read_i, int read_j, int read_jStride, int read_k, int read_kStride)
{
int read_ijk;
int write_ijk;
int lwpriv___ti_100_0;
int lwpriv__i;
int lwpriv__j;
int lwpriv__k;
lwpriv___ti_100_0=(threadIdx.x+(blockIdx.x*64));
if ((lwpriv___ti_100_0<((dim_i*dim_j)*dim_k)))
{
lwpriv__i=(lwpriv___ti_100_0%dim_i);
lwpriv__k=(lwpriv___ti_100_0/dim_i);
lwpriv__j=(lwpriv__k%dim_j);
lwpriv__k=(lwpriv__k/dim_j);
read_ijk=(((lwpriv__i+read_ghost)+read_i)+(((lwpriv__j+read_ghost)+read_j)*read_jStride))+(((lwpriv__k+read_ghost)+read_k)*read_kStride);
write_ijk=(lwpriv__i+(lwpriv__j*dim_i))+((lwpriv__k*dim_i)*dim_j);
read_buffer[write_ijk]=read[read_ijk];
}
}


#ifndef __CUDA_KERNELHEADER__ 

#define __CUDA_KERNELHEADER__ 

/********************************************/
/* Added codes for OpenACC2CUDA translation */
/********************************************/
#ifdef __cplusplus
#define restrict __restrict__
#endif
#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#ifndef DBL_MAX
#define DBL_MAX 1.7976931348623158e+308
#endif
#ifndef DBL_MIN
#define DBL_MIN 2.2250738585072014e-308
#endif
#ifndef FLT_MAX
#define FLT_MAX 3.402823466e+38
#endif
#ifndef FLT_MIN
#define FLT_MIN 1.175494351e-38
#endif

#endif


extern "C" __global__ void smooth_gpu_kernel0(double * alpha, double * beta_i, double * beta_j, double * beta_k, double * rhs, double * valid, double * x_n, double * x_np1, double a, double b, double c1, double c2, int dim, int ghosts, double h2inv, int jStride, int kStride)
{
int lgpriv__i;
int lgpriv__j;
int lgpriv__k;
int _ti_100_201;
lgpriv__k=(blockIdx.x+ghosts);
if (lgpriv__k<(dim+ghosts))
{
_ti_100_201=threadIdx.x;
{
for (lgpriv__i=(_ti_100_201+ghosts); lgpriv__i<(dim+ghosts); (lgpriv__i+=64))
{
for (lgpriv__j=ghosts; lgpriv__j<(dim+ghosts); lgpriv__j ++ )
{
int ijk;
ijk=(lgpriv__i+(lgpriv__j*jStride))+(lgpriv__k*kStride);
/* According to Saad... but his was missing a Dinv[ijk] == D^{-1} !!! */
/*  x_{n+1} = x_{n} + rho_{n} [ rho_{n-1}(x_{n} - x_{n-1}) + (2delta)(b-Ax_{n}) ] */
/*  x_temp[ijk] = x_n[ijk] + c1(x_n[ijk]-x_temp[ijk]) + c2*Dinv[ijk]*(rhs[ijk]-Ax_n); */
double Ax_n;
Ax_n=((a*alpha[ijk])*x_n[ijk])-((b*h2inv)*((((((( + beta_i[ijk])*((valid[(ijk-1)]*(x_n[ijk]+x_n[(ijk-1)]))-(2.0*x_n[ijk])))+(beta_j[ijk]*((valid[(ijk-jStride)]*(x_n[ijk]+x_n[(ijk-jStride)]))-(2.0*x_n[ijk]))))+(beta_k[ijk]*((valid[(ijk-kStride)]*(x_n[ijk]+x_n[(ijk-kStride)]))-(2.0*x_n[ijk]))))+(beta_i[(ijk+1)]*((valid[(ijk+1)]*(x_n[ijk]+x_n[(ijk+1)]))-(2.0*x_n[ijk]))))+(beta_j[(ijk+jStride)]*((valid[(ijk+jStride)]*(x_n[ijk]+x_n[(ijk+jStride)]))-(2.0*x_n[ijk]))))+(beta_k[(ijk+kStride)]*((valid[(ijk+kStride)]*(x_n[ijk]+x_n[(ijk+kStride)]))-(2.0*x_n[ijk])))));
double lambda;
lambda=1.0/((a*alpha[ijk])-((b*h2inv)*((((((( + beta_i[ijk])*(valid[(ijk-1)]-2.0))+(beta_j[ijk]*(valid[(ijk-jStride)]-2.0)))+(beta_k[ijk]*(valid[(ijk-kStride)]-2.0)))+(beta_i[(ijk+1)]*(valid[(ijk+1)]-2.0)))+(beta_j[(ijk+jStride)]*(valid[(ijk+jStride)]-2.0)))+(beta_k[(ijk+kStride)]*(valid[(ijk+kStride)]-2.0)))));
/* x_np1[ijk] = x_n[ijk] + c1(x_n[ijk]-x_nm1[ijk]) + c2*lambda*(rhs[ijk]-Ax_n); */
x_np1[ijk]=((x_n[ijk]+(c1*(x_n[ijk]-x_np1[ijk])))+((c2*lambda)*(rhs[ijk]-Ax_n)));
}
}
}
}
}


#ifndef __CUDA_KERNELHEADER__ 

#define __CUDA_KERNELHEADER__ 

/********************************************/
/* Added codes for OpenACC2CUDA translation */
/********************************************/
#ifdef __cplusplus
#define restrict __restrict__
#endif
#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#ifndef DBL_MAX
#define DBL_MAX 1.7976931348623158e+308
#endif
#ifndef DBL_MIN
#define DBL_MIN 2.2250738585072014e-308
#endif
#ifndef FLT_MAX
#define FLT_MAX 3.402823466e+38
#endif
#ifndef FLT_MIN
#define FLT_MIN 1.175494351e-38
#endif

#endif


extern "C" __global__ void zero_vector_gpu_kernel0(double * grid, int dim, int ghosts, int jStride, int kStride)
{
int ijk;
int lwpriv___ti_100_0;
int lwpriv__i;
int lwpriv__j;
int lwpriv__k;
lwpriv___ti_100_0=(threadIdx.x+(blockIdx.x*64));
if ((lwpriv___ti_100_0<(((((dim*dim)*dim)+(((6*dim)*dim)*ghosts))+(((12*dim)*ghosts)*ghosts))+(((8*ghosts)*ghosts)*ghosts))))
{
lwpriv__i=(lwpriv___ti_100_0%(dim+(2*ghosts)));
lwpriv__k=(lwpriv___ti_100_0/(dim+(2*ghosts)));
lwpriv__j=(lwpriv__k%(dim+(2*ghosts)));
lwpriv__k=(lwpriv__k/(dim+(2*ghosts)));
ijk=(lwpriv__i+(lwpriv__j*jStride))+(lwpriv__k*kStride);
grid[ijk]=0.0;
}
return ;
}

extern "C" __global__ void mul_vectors_gpu_kernel0(double * grid_a, double * grid_b, double * grid_c, int dim, int ghosts, int jStride, int kStride, double scale)
{
int ijk;
int lwpriv___ti_100_0;
int lwpriv__i;
int lwpriv__j;
int lwpriv__k_0;
lwpriv___ti_100_0=(threadIdx.x+(blockIdx.x*64));
/* Normalized Loop */
if ((lwpriv___ti_100_0<((dim*dim)*dim)))
{
lwpriv__i=((lwpriv___ti_100_0%dim)+ghosts);
lwpriv__k_0=(lwpriv___ti_100_0/dim);
lwpriv__j=((lwpriv__k_0%dim)+ghosts);
lwpriv__k_0=(lwpriv__k_0/dim);
ijk=(lwpriv__i+(lwpriv__j*jStride))+((ghosts+lwpriv__k_0)*kStride);
grid_c[ijk]=((scale*grid_a[ijk])*grid_b[ijk]);
}
}

extern "C" __global__ void scale_vector_gpu_kernel0(double * grid_a, double * grid_c, int dim, int ghosts, int jStride, int kStride, double scale_a)
{
int ijk;
int lwpriv___ti_100_0;
int lwpriv__i;
int lwpriv__j;
int lwpriv__k_0;
lwpriv___ti_100_0=(threadIdx.x+(blockIdx.x*64));
/* Normalized Loop */
if ((lwpriv___ti_100_0<((dim*dim)*dim)))
{
lwpriv__i=((lwpriv___ti_100_0%dim)+ghosts);
lwpriv__k_0=(lwpriv___ti_100_0/dim);
lwpriv__j=((lwpriv__k_0%dim)+ghosts);
lwpriv__k_0=(lwpriv__k_0/dim);
ijk=(lwpriv__i+(lwpriv__j*jStride))+((ghosts+lwpriv__k_0)*kStride);
grid_c[ijk]=(scale_a*grid_a[ijk]);
}
}

extern "C" __global__ void norm_gpu_kernel0(double * lgred__box_norm, double * grid, int dim, int ghosts, int jStride, int kStride)
{
int _bid;
int _bsize;
int _tid;
int ijk;
double fabs_grid_ijk;
int lwpriv___ti_100_0;
int lwpriv__i;
int lwpriv__j;
int lwpriv__k;
volatile double __shared__ lwreds__box_norm[64];
int _ti_100_1001;
int _ti_100_1002;
int _ti_100_1003;
_tid=((threadIdx.x+(threadIdx.y*blockDim.x))+(threadIdx.z*(blockDim.x*blockDim.y)));
_bsize=((blockDim.x*blockDim.y)*blockDim.z);
_bid=((blockIdx.x+(blockIdx.y*gridDim.x))+(blockIdx.z*(gridDim.x*gridDim.y)));
lwreds__box_norm[_tid]=DBL_MIN;
lwpriv___ti_100_0=(threadIdx.x+(blockIdx.x*64));
if ((lwpriv___ti_100_0<((dim*dim)*dim)))
{
lwpriv__i=((lwpriv___ti_100_0%dim)+ghosts);
lwpriv__k=(lwpriv___ti_100_0/dim);
lwpriv__j=((lwpriv__k%dim)+ghosts);
lwpriv__k=((lwpriv__k/dim)+ghosts);
ijk=(lwpriv__i+(lwpriv__j*jStride))+(lwpriv__k*kStride);
fabs_grid_ijk=fabs(grid[ijk]);
if ((fabs_grid_ijk>lwreds__box_norm[_tid]))
{
lwreds__box_norm[_tid]=fabs_grid_ijk;
}
/* max norm */
}
__syncthreads();
_ti_100_1002=_bsize;
for (_ti_100_1001=(_bsize>>1); _ti_100_1001>0; _ti_100_1001>>=1)
{
if ((_tid<_ti_100_1001))
{
lwreds__box_norm[_tid]=MAX(lwreds__box_norm[_tid], lwreds__box_norm[(_tid+_ti_100_1001)]);
}
_ti_100_1003=(_ti_100_1002&1);
if ((_ti_100_1003==1))
{
if ((_tid==0))
{
lwreds__box_norm[_tid]=MAX(lwreds__box_norm[_tid], lwreds__box_norm[(_tid+(_ti_100_1002-1))]);
}
}
_ti_100_1002=_ti_100_1001;
if ((_ti_100_1001>32))
{
__syncthreads();
}
}
if ((_tid==0))
{
lgred__box_norm[_bid]=lwreds__box_norm[_tid];
}
}

extern "C" __global__ void mean_gpu_kernel0(double * lgred__sum_box, double * grid_a, int dim, int ghosts, int jStride, int kStride)
{
int _bid;
int _bsize;
int _tid;
int ijk;
int lwpriv___ti_100_0;
int lwpriv__i;
int lwpriv__j;
int lwpriv__k;
volatile double __shared__ lwreds__sum_box[64];
int _ti_100_1001;
int _ti_100_1002;
int _ti_100_1003;
_tid=((threadIdx.x+(threadIdx.y*blockDim.x))+(threadIdx.z*(blockDim.x*blockDim.y)));
_bsize=((blockDim.x*blockDim.y)*blockDim.z);
_bid=((blockIdx.x+(blockIdx.y*gridDim.x))+(blockIdx.z*(gridDim.x*gridDim.y)));
lwreds__sum_box[_tid]=0.0F;
lwpriv___ti_100_0=(threadIdx.x+(blockIdx.x*64));
if ((lwpriv___ti_100_0<((dim*dim)*dim)))
{
lwpriv__i=((lwpriv___ti_100_0%dim)+ghosts);
lwpriv__k=(lwpriv___ti_100_0/dim);
lwpriv__j=((lwpriv__k%dim)+ghosts);
lwpriv__k=((lwpriv__k/dim)+ghosts);
ijk=(lwpriv__i+(lwpriv__j*jStride))+(lwpriv__k*kStride);
lwreds__sum_box[_tid]+=grid_a[ijk];
}
__syncthreads();
_ti_100_1002=_bsize;
for (_ti_100_1001=(_bsize>>1); _ti_100_1001>0; _ti_100_1001>>=1)
{
if ((_tid<_ti_100_1001))
{
lwreds__sum_box[_tid]+=lwreds__sum_box[(_tid+_ti_100_1001)];
}
_ti_100_1003=(_ti_100_1002&1);
if ((_ti_100_1003==1))
{
if ((_tid==0))
{
lwreds__sum_box[_tid]+=lwreds__sum_box[(_tid+(_ti_100_1002-1))];
}
}
_ti_100_1002=_ti_100_1001;
if ((_ti_100_1001>32))
{
__syncthreads();
}
}
if ((_tid==0))
{
lgred__sum_box[_bid]=lwreds__sum_box[_tid];
}
}

extern "C" __global__ void shift_vector_gpu_kernel0(double * grid_a, double * grid_c, int dim, int ghosts, int jStride, int kStride, double shift_a)
{
int ijk;
int lwpriv___ti_100_0;
int lwpriv__i;
int lwpriv__j;
int lwpriv__k;
lwpriv___ti_100_0=(threadIdx.x+(blockIdx.x*64));
if ((lwpriv___ti_100_0<((dim*dim)*dim)))
{
lwpriv__i=((lwpriv___ti_100_0%dim)+ghosts);
lwpriv__k=(lwpriv___ti_100_0/dim);
lwpriv__j=((lwpriv__k%dim)+ghosts);
lwpriv__k=((lwpriv__k/dim)+ghosts);
ijk=(lwpriv__i+(lwpriv__j*jStride))+(lwpriv__k*kStride);
grid_c[ijk]=(grid_a[ijk]+shift_a);
}
}


#ifndef __CUDA_KERNELHEADER__ 

#define __CUDA_KERNELHEADER__ 

/********************************************/
/* Added codes for OpenACC2CUDA translation */
/********************************************/
#ifdef __cplusplus
#define restrict __restrict__
#endif
#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#ifndef DBL_MAX
#define DBL_MAX 1.7976931348623158e+308
#endif
#ifndef DBL_MIN
#define DBL_MIN 2.2250738585072014e-308
#endif
#ifndef FLT_MAX
#define FLT_MAX 3.402823466e+38
#endif
#ifndef FLT_MIN
#define FLT_MIN 1.175494351e-38
#endif

#endif


extern "C" __global__ void residual_gpu_kernel0(double * alpha, double * beta_i, double * beta_j, double * beta_k, double * res, double * rhs, double * valid, double * x, double a, double b, int dim, int ghosts, double h2inv, int jStride, int kStride)
{
int j_0;
int lgpriv__i;
int lgpriv__k_0;
int _ti_100_201;
lgpriv__k_0=blockIdx.x;
/* Normalized Loop */
{
_ti_100_201=threadIdx.x;
/* Normalized Loop */
{
for (lgpriv__i=(_ti_100_201+ghosts); lgpriv__i<(dim+ghosts); (lgpriv__i+=64))
{
for (j_0=0; j_0<=(-1+dim); j_0 ++ )
{
int ijk;
ijk=(lgpriv__i+((ghosts+j_0)*jStride))+((ghosts+lgpriv__k_0)*kStride);
double Ax;
Ax=((a*alpha[ijk])*x[ijk])-((b*h2inv)*((((((( + beta_i[ijk])*((valid[(ijk-1)]*(x[ijk]+x[(ijk-1)]))-(2.0*x[ijk])))+(beta_j[ijk]*((valid[(ijk-jStride)]*(x[ijk]+x[(ijk-jStride)]))-(2.0*x[ijk]))))+(beta_k[ijk]*((valid[(ijk-kStride)]*(x[ijk]+x[(ijk-kStride)]))-(2.0*x[ijk]))))+(beta_i[(ijk+1)]*((valid[(ijk+1)]*(x[ijk]+x[(ijk+1)]))-(2.0*x[ijk]))))+(beta_j[(ijk+jStride)]*((valid[(ijk+jStride)]*(x[ijk]+x[(ijk+jStride)]))-(2.0*x[ijk]))))+(beta_k[(ijk+kStride)]*((valid[(ijk+kStride)]*(x[ijk]+x[(ijk+kStride)]))-(2.0*x[ijk])))));
res[ijk]=(rhs[ijk]-Ax);
}
}
}
}
}


#ifndef __CUDA_KERNELHEADER__ 

#define __CUDA_KERNELHEADER__ 

/********************************************/
/* Added codes for OpenACC2CUDA translation */
/********************************************/
#ifdef __cplusplus
#define restrict __restrict__
#endif
#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#ifndef DBL_MAX
#define DBL_MAX 1.7976931348623158e+308
#endif
#ifndef DBL_MIN
#define DBL_MIN 2.2250738585072014e-308
#endif
#ifndef FLT_MAX
#define FLT_MAX 3.402823466e+38
#endif
#ifndef FLT_MIN
#define FLT_MIN 1.175494351e-38
#endif

#endif


extern "C" __global__ void RestrictBlockLocal_kernel0(double * read, double * write, int dim_i, int dim_j, int dim_k, int read_ghost, int read_i, int read_j, int read_jStride, int read_k, int read_kStride, int write_ghost, int write_i, int write_j, int write_jStride, int write_k, int write_kStride)
{
int lgpriv__i;
int lgpriv__j;
int lgpriv__k;
int _ti_100_201;
lgpriv__k=blockIdx.x;
if (lgpriv__k<dim_k)
{
_ti_100_201=threadIdx.x;
{
for (lgpriv__i=(_ti_100_201+0); lgpriv__i<dim_i; (lgpriv__i+=64))
{
for (lgpriv__j=0; lgpriv__j<dim_j; lgpriv__j ++ )
{
int write_ijk;
write_ijk=(((lgpriv__i+write_ghost)+write_i)+(((lgpriv__j+write_ghost)+write_j)*write_jStride))+(((lgpriv__k+write_ghost)+write_k)*write_kStride);
int read_ijk;
read_ijk=((((lgpriv__i<<1)+read_ghost)+read_i)+((((lgpriv__j<<1)+read_ghost)+read_j)*read_jStride))+((((lgpriv__k<<1)+read_ghost)+read_k)*read_kStride);
write[write_ijk]=((((((((read[read_ijk]+read[(read_ijk+1)])+read[(read_ijk+read_jStride)])+read[((read_ijk+1)+read_jStride)])+read[(read_ijk+read_kStride)])+read[((read_ijk+1)+read_kStride)])+read[((read_ijk+read_jStride)+read_kStride)])+read[(((read_ijk+1)+read_jStride)+read_kStride)])*0.125);
}
}
}
}
}

extern "C" __global__ void RestrictBlockLocal_kernel1(double * read, double * write, int dim_i, int dim_j, int dim_k, int read_ghost, int read_i, int read_j, int read_jStride, int read_k, int read_kStride, int write_ghost, int write_i, int write_j, int write_jStride, int write_k, int write_kStride)
{
int lgpriv__i;
int lgpriv__j;
int lgpriv__k;
int _ti_100_201;
lgpriv__k=blockIdx.x;
if (lgpriv__k<dim_k)
{
_ti_100_201=threadIdx.x;
{
for (lgpriv__i=(_ti_100_201+0); lgpriv__i<dim_i; (lgpriv__i+=64))
{
for (lgpriv__j=0; lgpriv__j<dim_j; lgpriv__j ++ )
{
int write_ijk;
write_ijk=(((lgpriv__i+write_ghost)+write_i)+(((lgpriv__j+write_ghost)+write_j)*write_jStride))+(((lgpriv__k+write_ghost)+write_k)*write_kStride);
int read_ijk;
read_ijk=((((lgpriv__i<<1)+read_ghost)+read_i)+((((lgpriv__j<<1)+read_ghost)+read_j)*read_jStride))+((((lgpriv__k<<1)+read_ghost)+read_k)*read_kStride);
write[write_ijk]=((((read[read_ijk]+read[(read_ijk+read_jStride)])+read[(read_ijk+read_kStride)])+read[((read_ijk+read_jStride)+read_kStride)])*0.25);
}
}
}
}
}

extern "C" __global__ void RestrictBlockLocal_kernel2(double * read, double * write, int dim_i, int dim_j, int dim_k, int read_ghost, int read_i, int read_j, int read_jStride, int read_k, int read_kStride, int write_ghost, int write_i, int write_j, int write_jStride, int write_k, int write_kStride)
{
int lgpriv__i;
int lgpriv__j;
int lgpriv__k;
int _ti_100_201;
lgpriv__k=blockIdx.x;
if (lgpriv__k<dim_k)
{
_ti_100_201=threadIdx.x;
{
for (lgpriv__i=(_ti_100_201+0); lgpriv__i<dim_i; (lgpriv__i+=64))
{
for (lgpriv__j=0; lgpriv__j<dim_j; lgpriv__j ++ )
{
int write_ijk;
write_ijk=(((lgpriv__i+write_ghost)+write_i)+(((lgpriv__j+write_ghost)+write_j)*write_jStride))+(((lgpriv__k+write_ghost)+write_k)*write_kStride);
int read_ijk;
read_ijk=((((lgpriv__i<<1)+read_ghost)+read_i)+((((lgpriv__j<<1)+read_ghost)+read_j)*read_jStride))+((((lgpriv__k<<1)+read_ghost)+read_k)*read_kStride);
write[write_ijk]=((((read[read_ijk]+read[(read_ijk+1)])+read[(read_ijk+read_kStride)])+read[((read_ijk+1)+read_kStride)])*0.25);
}
}
}
}
}

extern "C" __global__ void RestrictBlockLocal_kernel3(double * read, double * write, int dim_i, int dim_j, int dim_k, int read_ghost, int read_i, int read_j, int read_jStride, int read_k, int read_kStride, int write_ghost, int write_i, int write_j, int write_jStride, int write_k, int write_kStride)
{
int lgpriv__i;
int lgpriv__j;
int lgpriv__k;
int _ti_100_201;
lgpriv__k=blockIdx.x;
if (lgpriv__k<dim_k)
{
_ti_100_201=threadIdx.x;
{
for (lgpriv__i=(_ti_100_201+0); lgpriv__i<dim_i; (lgpriv__i+=64))
{
for (lgpriv__j=0; lgpriv__j<dim_j; lgpriv__j ++ )
{
int write_ijk;
write_ijk=(((lgpriv__i+write_ghost)+write_i)+(((lgpriv__j+write_ghost)+write_j)*write_jStride))+(((lgpriv__k+write_ghost)+write_k)*write_kStride);
int read_ijk;
read_ijk=((((lgpriv__i<<1)+read_ghost)+read_i)+((((lgpriv__j<<1)+read_ghost)+read_j)*read_jStride))+((((lgpriv__k<<1)+read_ghost)+read_k)*read_kStride);
write[write_ijk]=((((read[read_ijk]+read[(read_ijk+1)])+read[(read_ijk+read_jStride)])+read[((read_ijk+1)+read_jStride)])*0.25);
}
}
}
}
}

extern "C" __global__ void RestrictBlockFromGPU_kernel0(double * read, double * read_buffer, int dim_i, int dim_j, int dim_k, int read_ghost, int read_i, int read_j, int read_jStride, int read_k, int read_kStride)
{
int lgpriv__i;
int lgpriv__j;
int lgpriv__k;
int _ti_100_201;
lgpriv__k=blockIdx.x;
if (lgpriv__k<dim_k)
{
_ti_100_201=threadIdx.x;
{
for (lgpriv__i=(_ti_100_201+0); lgpriv__i<dim_i; (lgpriv__i+=64))
{
for (lgpriv__j=0; lgpriv__j<dim_j; lgpriv__j ++ )
{
int write_ijk;
write_ijk=(lgpriv__i+(lgpriv__j*dim_i))+((lgpriv__k*dim_i)*dim_j);
int read_ijk;
read_ijk=((((lgpriv__i<<1)+read_ghost)+read_i)+((((lgpriv__j<<1)+read_ghost)+read_j)*read_jStride))+((((lgpriv__k<<1)+read_ghost)+read_k)*read_kStride);
read_buffer[write_ijk]=((((((((read[read_ijk]+read[(read_ijk+1)])+read[(read_ijk+read_jStride)])+read[((read_ijk+1)+read_jStride)])+read[(read_ijk+read_kStride)])+read[((read_ijk+1)+read_kStride)])+read[((read_ijk+read_jStride)+read_kStride)])+read[(((read_ijk+1)+read_jStride)+read_kStride)])*0.125);
}
}
}
}
}

extern "C" __global__ void RestrictBlockFromGPU_kernel1(double * read, double * read_buffer, int dim_i, int dim_j, int dim_k, int read_ghost, int read_i, int read_j, int read_jStride, int read_k, int read_kStride)
{
int lgpriv__i;
int lgpriv__j;
int lgpriv__k;
int _ti_100_201;
lgpriv__k=blockIdx.x;
if (lgpriv__k<dim_k)
{
_ti_100_201=threadIdx.x;
{
for (lgpriv__i=(_ti_100_201+0); lgpriv__i<dim_i; (lgpriv__i+=64))
{
for (lgpriv__j=0; lgpriv__j<dim_j; lgpriv__j ++ )
{
int write_ijk;
write_ijk=(lgpriv__i+(lgpriv__j*dim_i))+((lgpriv__k*dim_i)*dim_j);
int read_ijk;
read_ijk=((((lgpriv__i<<1)+read_ghost)+read_i)+((((lgpriv__j<<1)+read_ghost)+read_j)*read_jStride))+((((lgpriv__k<<1)+read_ghost)+read_k)*read_kStride);
read_buffer[write_ijk]=((((read[read_ijk]+read[(read_ijk+read_jStride)])+read[(read_ijk+read_kStride)])+read[((read_ijk+read_jStride)+read_kStride)])*0.25);
}
}
}
}
}

extern "C" __global__ void RestrictBlockFromGPU_kernel2(double * read, double * read_buffer, int dim_i, int dim_j, int dim_k, int read_ghost, int read_i, int read_j, int read_jStride, int read_k, int read_kStride)
{
int lgpriv__i;
int lgpriv__j;
int lgpriv__k;
int _ti_100_201;
lgpriv__k=blockIdx.x;
if (lgpriv__k<dim_k)
{
_ti_100_201=threadIdx.x;
{
for (lgpriv__i=(_ti_100_201+0); lgpriv__i<dim_i; (lgpriv__i+=64))
{
for (lgpriv__j=0; lgpriv__j<dim_j; lgpriv__j ++ )
{
int write_ijk;
write_ijk=(lgpriv__i+(lgpriv__j*dim_i))+((lgpriv__k*dim_i)*dim_j);
int read_ijk;
read_ijk=((((lgpriv__i<<1)+read_ghost)+read_i)+((((lgpriv__j<<1)+read_ghost)+read_j)*read_jStride))+((((lgpriv__k<<1)+read_ghost)+read_k)*read_kStride);
read_buffer[write_ijk]=((((read[read_ijk]+read[(read_ijk+1)])+read[(read_ijk+read_kStride)])+read[((read_ijk+1)+read_kStride)])*0.25);
}
}
}
}
}

extern "C" __global__ void RestrictBlockFromGPU_kernel3(double * read, double * read_buffer, int dim_i, int dim_j, int dim_k, int read_ghost, int read_i, int read_j, int read_jStride, int read_k, int read_kStride)
{
int lgpriv__i;
int lgpriv__j;
int lgpriv__k;
int _ti_100_201;
lgpriv__k=blockIdx.x;
if (lgpriv__k<dim_k)
{
_ti_100_201=threadIdx.x;
{
for (lgpriv__i=(_ti_100_201+0); lgpriv__i<dim_i; (lgpriv__i+=64))
{
for (lgpriv__j=0; lgpriv__j<dim_j; lgpriv__j ++ )
{
int write_ijk;
write_ijk=(lgpriv__i+(lgpriv__j*dim_i))+((lgpriv__k*dim_i)*dim_j);
int read_ijk;
read_ijk=((((lgpriv__i<<1)+read_ghost)+read_i)+((((lgpriv__j<<1)+read_ghost)+read_j)*read_jStride))+((((lgpriv__k<<1)+read_ghost)+read_k)*read_kStride);
read_buffer[write_ijk]=((((read[read_ijk]+read[(read_ijk+1)])+read[(read_ijk+read_jStride)])+read[((read_ijk+1)+read_jStride)])*0.25);
}
}
}
}
}

