//------------------------------------------------------------------------------------------------------------------------------
// Samuel Williams
// SWWilliams@lbl.gov
// Lawrence Berkeley National Lab
//------------------------------------------------------------------------------------------------------------------------------
#include "../../level.h"

//static inline void CopyBlock(level_type *level, int id, blockCopy_type *block){
//Copy from read buffer in GPU to write buffer in GPU.
void CopyBlockLocal(level_type *level, int id, blockCopy_type *block)
{
  // copy 3D array from read_i,j,k of read[] to write_i,j,k in write[]
  int   dim_i       = block->dim.i;
  int   dim_j       = block->dim.j;
  int   dim_k       = block->dim.k;

  int  read_i       = block->read.i;
  int  read_j       = block->read.j;
  int  read_k       = block->read.k;
  int  read_jStride = block->read.jStride;
  int  read_kStride = block->read.kStride;

  int write_i       = block->write.i;
  int write_j       = block->write.j;
  int write_k       = block->write.k;
  int write_jStride = block->write.jStride;
  int write_kStride = block->write.kStride;

  double * __restrict__   read = block->read.ptr;
  double * __restrict__  write = block->write.ptr;
  int read_ghost = 0;
  int write_ghost = 0;
  int i,j,k;
  int asyncID = level->num_my_boxes + 1;

  if(block->read.box >=0) {
	read = level->my_boxes[ block->read.box].vectors[id]; // + level->my_boxes[ block->read.box].ghosts*(1+level->my_boxes[ block->read.box].jStride+level->my_boxes[ block->read.box].kStride);
    read_ghost = level->my_boxes[ block->read.box].ghosts;
  }
  if(block->write.box>=0) {
	write = level->my_boxes[block->write.box].vectors[id]; // + level->my_boxes[block->write.box].ghosts*(1+level->my_boxes[block->write.box].jStride+level->my_boxes[block->write.box].kStride);
    write_ghost = level->my_boxes[ block->write.box].ghosts;
  }

  if(dim_i==1){ // be smart and don't have an inner loop from 0 to 0
    #pragma acc parallel loop gang present(read, write) private(k,j)
    for(k=0;k<dim_k;k++){
    #pragma acc loop worker
    for(j=0;j<dim_j;j++){
      int  read_ijk = ( read_i + read_ghost) + (j + read_ghost + read_j)* read_jStride + (k + read_ghost + read_k)* read_kStride;
      int write_ijk = (write_i + write_ghost) + (j + write_ghost +write_j)*write_jStride + (k + write_ghost +write_k)*write_kStride;
      write[write_ijk] = read[read_ijk];
    }}
  }else if(dim_j==1){ // don't have a 0..0 loop
    #pragma acc parallel loop gang present(read, write) private(k,i)
    for(k=0;k<dim_k;k++){
    #pragma acc loop worker
    for(i=0;i<dim_i;i++){
      int  read_ijk = (i + read_ghost + read_i) + ( read_j+read_ghost)* read_jStride + (k + read_ghost + read_k)* read_kStride;
      int write_ijk = (i + write_ghost +write_i) + (write_j + write_ghost)*write_jStride + (k + write_ghost +write_k)*write_kStride;
      write[write_ijk] = read[read_ijk];
    }}
  }else if(dim_k==1){ // don't have a 0..0 loop
    #pragma acc parallel loop gang present(read, write) private(j,i)
    for(j=0;j<dim_j;j++){
    #pragma acc loop worker
    for(i=0;i<dim_i;i++){
      int  read_ijk = (i + read_ghost + read_i) + (j + read_ghost + read_j)* read_jStride + ( read_k+read_ghost)* read_kStride;
      int write_ijk = (i + write_ghost +write_i) + (j + write_ghost +write_j)*write_jStride + (write_k + write_ghost)*write_kStride;
      write[write_ijk] = read[read_ijk];
    }}
  }else{
    #pragma acc parallel loop gang present(read, write) private(k,j,i)
    for(k=0;k<dim_k;k++){
    #pragma acc loop worker
    #pragma openarc transform permute(i,j)
    for(j=0;j<dim_j;j++){
    for(i=0;i<dim_i;i++){
      int  read_ijk = (i + read_ghost + read_i) + (j + read_ghost + read_j)* read_jStride + (k + read_ghost + read_k)* read_kStride;
      int write_ijk = (i + write_ghost +write_i) + (j + write_ghost +write_j)*write_jStride + (k + write_ghost +write_k)*write_kStride;
      write[write_ijk] = read[read_ijk];
    }}}
  }
}

//Transfer read buffer to GPU and then copy it to writer buffer.
void CopyBlockToGPU(level_type *level, int id, blockCopy_type *block)
{
  // copy 3D array from read_i,j,k of read[] to write_i,j,k in write[]
  int   dim_i       = block->dim.i;
  int   dim_j       = block->dim.j;
  int   dim_k       = block->dim.k;

  int  read_i       = block->read.i;
  int  read_j       = block->read.j;
  int  read_k       = block->read.k;
  int  read_jStride = block->read.jStride;
  int  read_kStride = block->read.kStride;

  int write_i       = block->write.i;
  int write_j       = block->write.j;
  int write_k       = block->write.k;
  int write_jStride = block->write.jStride;
  int write_kStride = block->write.kStride;

  double * __restrict__   read = block->read.ptr;
  double * __restrict__  write = block->write.ptr;
  int read_ghost = 0;
  int write_ghost = 0;
  int i,j,k;
  int asyncID = level->num_my_boxes + 1;

  if(block->read.box >=0) {
	read = level->my_boxes[ block->read.box].vectors[id]; // + level->my_boxes[ block->read.box].ghosts*(1+level->my_boxes[ block->read.box].jStride+level->my_boxes[ block->read.box].kStride);
    read_ghost = level->my_boxes[ block->read.box].ghosts;
  }
  if(block->write.box>=0) {
	write = level->my_boxes[block->write.box].vectors[id]; // + level->my_boxes[block->write.box].ghosts*(1+level->my_boxes[block->write.box].jStride+level->my_boxes[block->write.box].kStride);
    write_ghost = level->my_boxes[ block->write.box].ghosts;
  }

  if(dim_i==1){ // be smart and don't have an inner loop from 0 to 0
//    #pragma omp parallel for 
    for(k=0;k<dim_k;k++){
    for(j=0;j<dim_j;j++){
      int  read_ijk = ( read_i + read_ghost) + (j + read_ghost + read_j)* read_jStride + (k + read_ghost + read_k)* read_kStride;
      int  write_ijk = j + k*dim_j;
      read_buffer[write_ijk] = read[read_ijk];
    }}
	#pragma acc update device(read_buffer[0:dim_j*dim_k])

    #pragma acc parallel loop gang present(read_buffer, write) private(k,j)
    for(k=0;k<dim_k;k++){
    #pragma acc loop worker
    for(j=0;j<dim_j;j++){
      int  read_ijk = j + k*dim_j;
      int write_ijk = (write_i + write_ghost) + (j + write_ghost +write_j)*write_jStride + (k + write_ghost +write_k)*write_kStride;
      write[write_ijk] = read_buffer[read_ijk];
    }}
  }else if(dim_j==1){ // don't have a 0..0 loop
//    #pragma omp parallel for
    for(k=0;k<dim_k;k++){
    for(i=0;i<dim_i;i++){
      int  read_ijk = (i + read_ghost + read_i) + ( read_j+read_ghost)* read_jStride + (k + read_ghost + read_k)* read_kStride;
      int  write_ijk = i + k*dim_i;
      read_buffer[write_ijk] = read[read_ijk];
    }}
	#pragma acc update device(read_buffer[0:dim_i*dim_k])

    #pragma acc parallel loop gang present(read_buffer, write) private(k,i)
    for(k=0;k<dim_k;k++){
    #pragma acc loop worker
    for(i=0;i<dim_i;i++){
      int  read_ijk = i + k*dim_i;
      int write_ijk = (i + write_ghost +write_i) + (write_j + write_ghost)*write_jStride + (k + write_ghost +write_k)*write_kStride;
      write[write_ijk] = read_buffer[read_ijk];
    }}
  }else if(dim_k==1){ // don't have a 0..0 loop
//    #pragma omp parallel for
    for(j=0;j<dim_j;j++){
    for(i=0;i<dim_i;i++){
      int  read_ijk = (i + read_ghost + read_i) + (j + read_ghost + read_j)* read_jStride + ( read_k + read_ghost)* read_kStride;
      int  write_ijk = i + j*dim_i;
      read_buffer[write_ijk] = read[read_ijk];
    }}
	#pragma acc update device(read_buffer[0:dim_i*dim_j])

    #pragma acc parallel loop gang present(read_buffer, write) private(j,i)
    for(j=0;j<dim_j;j++){
    #pragma acc loop worker
    for(i=0;i<dim_i;i++){
      int  read_ijk = i + j*dim_i;
      int write_ijk = (i + write_ghost +write_i) + (j + write_ghost +write_j)*write_jStride + (write_k + write_ghost)*write_kStride;
      write[write_ijk] = read_buffer[read_ijk];
    }}
  }else{
//    #pragma omp parallel for
    for(k=0;k<dim_k;k++){
    for(j=0;j<dim_j;j++){
    for(i=0;i<dim_i;i++){
      int  read_ijk = (i + read_ghost + read_i) + (j + read_ghost + read_j)* read_jStride + (k + read_ghost + read_k)* read_kStride;
      int write_ijk = i + j*dim_i + k*dim_i*dim_j;
      read_buffer[write_ijk] = read[read_ijk];
    }}}
	#pragma acc update device(read_buffer[0:dim_i*dim_j*dim_k])

    #pragma acc parallel loop gang worker collapse(3) present(read_buffer, write) private(k,j,i)
    for(k=0;k<dim_k;k++){
    for(j=0;j<dim_j;j++){
    for(i=0;i<dim_i;i++){
      int read_ijk = i + j*dim_i + k*dim_i*dim_j;
      int write_ijk = (i + write_ghost +write_i) + (j + write_ghost +write_j)*write_jStride + (k + write_ghost +write_k)*write_kStride;
      write[write_ijk] = read_buffer[read_ijk];
    }}}
  }
}

//Transfer read buffer from GPU and then copy it to writer buffer.
void CopyBlockFromGPU(level_type *level, int id, blockCopy_type *block)
{
  // copy 3D array from read_i,j,k of read[] to write_i,j,k in write[]
  int   dim_i       = block->dim.i;
  int   dim_j       = block->dim.j;
  int   dim_k       = block->dim.k;

  int  read_i       = block->read.i;
  int  read_j       = block->read.j;
  int  read_k       = block->read.k;
  int  read_jStride = block->read.jStride;
  int  read_kStride = block->read.kStride;

  int write_i       = block->write.i;
  int write_j       = block->write.j;
  int write_k       = block->write.k;
  int write_jStride = block->write.jStride;
  int write_kStride = block->write.kStride;

  double * __restrict__   read = block->read.ptr;
  double * __restrict__  write = block->write.ptr;
  int read_ghost = 0;
  int write_ghost = 0;
  int i,j,k;
  int asyncID = level->num_my_boxes + 1;

  if(block->read.box >=0) {
	read = level->my_boxes[ block->read.box].vectors[id]; // + level->my_boxes[ block->read.box].ghosts*(1+level->my_boxes[ block->read.box].jStride+level->my_boxes[ block->read.box].kStride);
    read_ghost = level->my_boxes[ block->read.box].ghosts;
  }
  if(block->write.box>=0) {
	write = level->my_boxes[block->write.box].vectors[id]; // + level->my_boxes[block->write.box].ghosts*(1+level->my_boxes[block->write.box].jStride+level->my_boxes[block->write.box].kStride);
    write_ghost = level->my_boxes[ block->write.box].ghosts;
  }

  if(dim_i==1){ // be smart and don't have an inner loop from 0 to 0
    #pragma acc parallel loop gang present(read_buffer, read) private(k,j)
    for(k=0;k<dim_k;k++){
    #pragma acc loop worker
    for(j=0;j<dim_j;j++){
      int  read_ijk = ( read_i + read_ghost) + (j + read_ghost + read_j)* read_jStride + (k + read_ghost + read_k)* read_kStride;
      int  write_ijk = j + k*dim_j;
      read_buffer[write_ijk] = read[read_ijk];
    }}
	#pragma acc update host(read_buffer[0:dim_j*dim_k])

    for(k=0;k<dim_k;k++){
    for(j=0;j<dim_j;j++){
      int  read_ijk = j + k*dim_j;
      int write_ijk = (write_i + write_ghost) + (j + write_ghost +write_j)*write_jStride + (k + write_ghost +write_k)*write_kStride;
      write[write_ijk] = read_buffer[read_ijk];
    }}
  }else if(dim_j==1){ // don't have a 0..0 loop
    #pragma acc parallel loop gang present(read_buffer, read) private(k,i)
    for(k=0;k<dim_k;k++){
    #pragma acc loop worker
    for(i=0;i<dim_i;i++){
      int  read_ijk = (i + read_ghost + read_i) + ( read_j+read_ghost)* read_jStride + (k + read_ghost + read_k)* read_kStride;
      int  write_ijk = i + k*dim_i;
      read_buffer[write_ijk] = read[read_ijk];
    }}
	#pragma acc update host(read_buffer[0:dim_i*dim_k]) 

    for(k=0;k<dim_k;k++){
    for(i=0;i<dim_i;i++){
      int  read_ijk = i + k*dim_i;
      int write_ijk = (i + write_ghost +write_i) + (write_j + write_ghost)*write_jStride + (k + write_ghost +write_k)*write_kStride;
      write[write_ijk] = read_buffer[read_ijk];
    }}
  }else if(dim_k==1){ // don't have a 0..0 loop
    #pragma acc parallel loop gang present(read_buffer, read) private(j,i)
    for(j=0;j<dim_j;j++){
    #pragma acc loop worker
    for(i=0;i<dim_i;i++){
      int  read_ijk = (i + read_ghost + read_i) + (j + read_ghost + read_j)* read_jStride + ( read_k + read_ghost)* read_kStride;
      int  write_ijk = i + j*dim_i;
      read_buffer[write_ijk] = read[read_ijk];
    }}
	#pragma acc update host(read_buffer[0:dim_i*dim_j])

    for(j=0;j<dim_j;j++){
    for(i=0;i<dim_i;i++){
      int  read_ijk = i + j*dim_i;
      int write_ijk = (i + write_ghost +write_i) + (j + write_ghost +write_j)*write_jStride + (write_k + write_ghost)*write_kStride;
      write[write_ijk] = read_buffer[read_ijk];
    }}
  }else{
    #pragma acc parallel loop gang worker collapse(3) present(read_buffer, read) private(k,j,i)
    for(k=0;k<dim_k;k++){
    for(j=0;j<dim_j;j++){
    for(i=0;i<dim_i;i++){
      int  read_ijk = (i + read_ghost + read_i) + (j + read_ghost + read_j)* read_jStride + (k + read_ghost + read_k)* read_kStride;
      int write_ijk = i + j*dim_i + k*dim_i*dim_j;
      read_buffer[write_ijk] = read[read_ijk];
    }}}
	#pragma acc update host(read_buffer[0:dim_i*dim_j*dim_k])

    for(k=0;k<dim_k;k++){
    for(j=0;j<dim_j;j++){
    for(i=0;i<dim_i;i++){
      int read_ijk = i + j*dim_i + k*dim_i*dim_j;
      int write_ijk = (i + write_ghost +write_i) + (j + write_ghost +write_j)*write_jStride + (k + write_ghost +write_k)*write_kStride;
      write[write_ijk] = read_buffer[read_ijk];
    }}}
  }
}

static inline void CopyBlock(level_type *level, int id, blockCopy_type *block){
  // copy 3D array from read_i,j,k of read[] to write_i,j,k in write[]
  int   dim_i       = block->dim.i;
  int   dim_j       = block->dim.j;
  int   dim_k       = block->dim.k;

  int  read_i       = block->read.i;
  int  read_j       = block->read.j;
  int  read_k       = block->read.k;
  int  read_jStride = block->read.jStride;
  int  read_kStride = block->read.kStride;

  int write_i       = block->write.i;
  int write_j       = block->write.j;
  int write_k       = block->write.k;
  int write_jStride = block->write.jStride;
  int write_kStride = block->write.kStride;

  double * __restrict__  read = block->read.ptr;
  double * __restrict__ write = block->write.ptr;
  if(block->read.box >=0) read = level->my_boxes[ block->read.box].vectors[id] + level->my_boxes[ block->read.box].ghosts*(1+level->my_boxes[ block->read.box].jStride+level->my_boxes[ block->read.box].kStride);
  if(block->write.box>=0)write = level->my_boxes[block->write.box].vectors[id] + level->my_boxes[block->write.box].ghosts*(1+level->my_boxes[block->write.box].jStride+level->my_boxes[block->write.box].kStride);


  int i,j,k;
  if(dim_i==1){ // be smart and don't have an inner loop from 0 to 0
    for(k=0;k<dim_k;k++){
    for(j=0;j<dim_j;j++){
      int  read_ijk = ( read_i) + (j+ read_j)* read_jStride + (k+ read_k)* read_kStride;
      int write_ijk = (write_i) + (j+write_j)*write_jStride + (k+write_k)*write_kStride;
      write[write_ijk] = read[read_ijk];
    }}
  }else if(dim_j==1){ // don't have a 0..0 loop
    for(k=0;k<dim_k;k++){
    for(i=0;i<dim_i;i++){
      int  read_ijk = (i+ read_i) + ( read_j)* read_jStride + (k+ read_k)* read_kStride;
      int write_ijk = (i+write_i) + (write_j)*write_jStride + (k+write_k)*write_kStride;
      write[write_ijk] = read[read_ijk];
    }}
  }else if(dim_k==1){ // don't have a 0..0 loop
    for(j=0;j<dim_j;j++){
    for(i=0;i<dim_i;i++){
      int  read_ijk = (i+ read_i) + (j+ read_j)* read_jStride + ( read_k)* read_kStride;
      int write_ijk = (i+write_i) + (j+write_j)*write_jStride + (write_k)*write_kStride;
      write[write_ijk] = read[read_ijk];
    }}
  }else if(dim_i==4){ // be smart and don't have an inner loop from 0 to 3
    for(k=0;k<dim_k;k++){
    for(j=0;j<dim_j;j++){
      int  read_ijk = ( read_i) + (j+ read_j)* read_jStride + (k+ read_k)* read_kStride;
      int write_ijk = (write_i) + (j+write_j)*write_jStride + (k+write_k)*write_kStride;
      write[write_ijk+0] = read[read_ijk+0];
      write[write_ijk+1] = read[read_ijk+1];
      write[write_ijk+2] = read[read_ijk+2];
      write[write_ijk+3] = read[read_ijk+3];
    }}
  }else{
    for(k=0;k<dim_k;k++){
    for(j=0;j<dim_j;j++){
    for(i=0;i<dim_i;i++){
      int  read_ijk = (i+ read_i) + (j+ read_j)* read_jStride + (k+ read_k)* read_kStride;
      int write_ijk = (i+write_i) + (j+write_j)*write_jStride + (k+write_k)*write_kStride;
      write[write_ijk] = read[read_ijk];
    }}}
  }

}


//------------------------------------------------------------------------------------------------------------------------------
//static inline void IncrementBlock(level_type *level, int id, double prescale, blockCopy_type *block){
void IncrementBlock(level_type *level, int id, double prescale, blockCopy_type *block){
  // copy 3D array from read_i,j,k of read[] to write_i,j,k in write[]
  int   dim_i       = block->dim.i;
  int   dim_j       = block->dim.j;
  int   dim_k       = block->dim.k;

  int  read_i       = block->read.i;
  int  read_j       = block->read.j;
  int  read_k       = block->read.k;
  int  read_jStride = block->read.jStride;
  int  read_kStride = block->read.kStride;

  int write_i       = block->write.i;
  int write_j       = block->write.j;
  int write_k       = block->write.k;
  int write_jStride = block->write.jStride;
  int write_kStride = block->write.kStride;

  int i,j,k;

  double * __restrict__  read = block->read.ptr;
  double *  __restrict__ write = block->write.ptr;
  if(block->read.box >=0){
     read = level->my_boxes[ block->read.box].vectors[id] + level->my_boxes[ block->read.box].ghosts*(1+level->my_boxes[ block->read.box].jStride+level->my_boxes[ block->read.box].kStride);
     read_jStride = level->my_boxes[block->read.box ].jStride;
     read_kStride = level->my_boxes[block->read.box ].kStride;
  }
  if(block->write.box>=0){
    write = level->my_boxes[block->write.box].vectors[id] + level->my_boxes[block->write.box].ghosts*(1+level->my_boxes[block->write.box].jStride+level->my_boxes[block->write.box].kStride);
    write_jStride = level->my_boxes[block->write.box].jStride;
    write_kStride = level->my_boxes[block->write.box].kStride;
  }

  for(k=0;k<dim_k;k++){
  for(j=0;j<dim_j;j++){
  for(i=0;i<dim_i;i++){
    int  read_ijk = (i+ read_i) + (j+ read_j)* read_jStride + (k+ read_k)* read_kStride;
    int write_ijk = (i+write_i) + (j+write_j)*write_jStride + (k+write_k)*write_kStride;
    write[write_ijk] = prescale*write[write_ijk] + read[read_ijk]; // CAREFUL !!!  you must guarantee you zero'd the MPI buffers(write[]) and destination boxes at some point to avoid 0.0*NaN or 0.0*inf
  }}}

}



