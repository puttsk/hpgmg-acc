/* ------------------------------------------------------------------------------------------------------------------------------ */
/* Samuel Williams */
/* SWWilliams@lbl.gov */
/* Lawrence Berkeley National Lab */
/* ------------------------------------------------------------------------------------------------------------------------------ */

#ifndef __O2G_HEADER__ 

#define __O2G_HEADER__ 

/*******************************************/
/* Added codes for OpenACC2GPU translation */
/*******************************************/
#include <openacc.h>
#include <openaccrt.h>
#include <math.h>
#include <float.h>
#include <limits.h>
#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#ifdef __cplusplus
#define restrict __restrict__
#endif

/**********************************************************/
/* Maximum width of linear memory bound to texture memory */
/**********************************************************/
/* width in bytes */
#define LMAX_WIDTH    134217728
/**********************************/
/* Maximum memory pitch (in bytes)*/
/**********************************/
#define MAX_PITCH   262144
/****************************************/
/* Maximum allowed GPU global memory    */
/* (should be less than actual size ) */
/****************************************/
#define MAX_GMSIZE  1600000000
/****************************************/
/* Maximum allowed GPU shared memory    */
/****************************************/
#define MAX_SMSIZE  16384
/********************************************/
/* Maximum size of each dimension of a grid */
/********************************************/
#define MAX_GDIMENSION  65535

#define NUM_WORKERS  64


static int gpuNumThreads = NUM_WORKERS;
static int totalGpuNumThreads;
static int gpuNumBlocks;
static unsigned int gpuBytes = 0;

#endif 
/* End of __O2G_HEADER__ */



void zero_vector(level_type * level, int component_id)
{
/* zero's the entire grid INCLUDING ghost zones... */
uint64_t _timeStart;
_timeStart=CycleTime();
int box;
/* [FIXME] GCC does not replace macros in directives. */
/* #pragma omp parallel for private(box) OMP_THREAD_ACROSS_BOXES(level->concurrent_boxes) */
#pragma omp parallel for if(level->concurrent_boxes>1) num_threads(level->concurrent_boxes) private(box)
for (box=0; box<level->num_my_boxes; box ++ )
{
int i;
int j;
int k;
int jStride;
jStride=level->my_boxes[box].jStride;
int kStride;
kStride=level->my_boxes[box].kStride;
int ghosts;
ghosts=level->my_boxes[box].ghosts;
int dim;
dim=level->my_boxes[box].dim;
double * grid;
grid=level->my_boxes[box].vectors[component_id]+(ghosts*((1+jStride)+kStride));
/* [FIXME] GCC does not replace macros in directives. */
/* #pragma omp parallel for private(k,j,i) OMP_THREAD_WITHIN_A_BOX(level->threads_per_box) */
#pragma omp parallel for if(level->threads_per_box>1) num_threads(level->threads_per_box) private(i, j, k) collapse(2)
for (k=( - ghosts); k<(dim+ghosts); k ++ )
{
for (j=( - ghosts); j<(dim+ghosts); j ++ )
{
for (i=( - ghosts); i<(dim+ghosts); i ++ )
{
int ijk;
ijk=(i+(j*jStride))+(k*kStride);
grid[ijk]=0.0;
}
}
}
}
level->cycles.blas1+=((uint64_t)(CycleTime()-_timeStart));
return ;
}

void zero_vector_gpu(level_type * level, int component_id)
{
/* zero's the entire grid INCLUDING ghost zones... */
uint64_t _timeStart;
int _ti_100_0;

////////////////////////////////
// CUDA Device Initialization //
////////////////////////////////

_timeStart=CycleTime();
int box;
/* #pragma omp parallel for private(box) OMP_THREAD_ACROSS_BOXES(level->concurrent_boxes) */
for (box=0; box<level->num_my_boxes; box ++ )
{
int i;
int j;
int k;
int jStride;
double * gpu__grid;
jStride=level->my_boxes[box].jStride;
int kStride;
kStride=level->my_boxes[box].kStride;
int ghosts;
ghosts=level->my_boxes[box].ghosts;
int dim;
dim=level->my_boxes[box].dim;
double * grid;
grid=level->my_boxes[box].vectors[component_id];
/* + ghosts(1+jStride+kStride); */
const uint64_t tsize = level->my_boxes[box].volume;
/* #pragma omp parallel for private(k,j,i) OMP_THREAD_WITHIN_A_BOX(level->threads_per_box) */
HI_set_async(box);
if ((HI_get_device_address(grid, ((void * *)( & gpu__grid)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, grid, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  kernels loop async(box) collapse(3) gang(((int)ceil((((float)(((((dim*dim)*dim)+(((6*dim)*dim)*ghosts))+(((12*dim)*ghosts)*ghosts))+(((8*ghosts)*ghosts)*ghosts)))/64.0F)))) worker(64) copyin(dim, ghosts, jStride, kStride) present(grid[0:tsize]) private(_ti_100_0, i, j, k) \n");
exit(1);
}
int dimGrid_zero_vector_gpu_kernel0[3];
dimGrid_zero_vector_gpu_kernel0[0]=((int)ceil((((float)(((((dim*dim)*dim)+(((6*dim)*dim)*ghosts))+(((12*dim)*ghosts)*ghosts))+(((8*ghosts)*ghosts)*ghosts)))/64.0F)));
dimGrid_zero_vector_gpu_kernel0[1]=1;
dimGrid_zero_vector_gpu_kernel0[2]=1;
int dimBlock_zero_vector_gpu_kernel0[3];
dimBlock_zero_vector_gpu_kernel0[0]=64;
dimBlock_zero_vector_gpu_kernel0[1]=1;
dimBlock_zero_vector_gpu_kernel0[2]=1;
gpuNumBlocks=((int)ceil((((float)(((((dim*dim)*dim)+(((6*dim)*dim)*ghosts))+(((12*dim)*ghosts)*ghosts))+(((8*ghosts)*ghosts)*ghosts)))/64.0F)));
gpuNumThreads=64;
totalGpuNumThreads=(((int)ceil((((float)(((((dim*dim)*dim)+(((6*dim)*dim)*ghosts))+(((12*dim)*ghosts)*ghosts))+(((8*ghosts)*ghosts)*ghosts)))/64.0F)))*64);
HI_register_kernel_arg("zero_vector_gpu_kernel0",0,sizeof(void*),( & gpu__grid));
HI_register_kernel_arg("zero_vector_gpu_kernel0",1,sizeof(void*),( & dim));
HI_register_kernel_arg("zero_vector_gpu_kernel0",2,sizeof(void*),( & ghosts));
HI_register_kernel_arg("zero_vector_gpu_kernel0",3,sizeof(void*),( & jStride));
HI_register_kernel_arg("zero_vector_gpu_kernel0",4,sizeof(void*),( & kStride));
HI_kernel_call("zero_vector_gpu_kernel0",dimGrid_zero_vector_gpu_kernel0,dimBlock_zero_vector_gpu_kernel0,box);
;
gpuNumBlocks=((int)ceil((((float)(((((dim*dim)*dim)+(((6*dim)*dim)*ghosts))+(((12*dim)*ghosts)*ghosts))+(((8*ghosts)*ghosts)*ghosts)))/64.0F)));
/* [DEBUG} will not need this if all computations are on GPU. */
HI_set_async(box);

if ((HI_get_device_address(grid, ((void * *)( & gpu__grid)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, grid, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  update async(box) host(grid[0:tsize]) \n");
exit(1);
}
gpuBytes=(sizeof (double)*tsize);
HI_memcpy_async(grid, gpu__grid, gpuBytes, HI_MemcpyDeviceToHost, 0, box);
}
acc_wait_all();
level->cycles.blas1+=((uint64_t)(CycleTime()-_timeStart));
return ;
}

/* ------------------------------------------------------------------------------------------------------------------------------ */
void initialize_valid_region(level_type * level)
{
uint64_t _timeStart;
_timeStart=CycleTime();
int box;
/* #pragma omp parallel for private(box) OMP_THREAD_ACROSS_BOXES(level->concurrent_boxes) */
#pragma omp parallel for if(level->concurrent_boxes>1) num_threads(level->concurrent_boxes) private(box)
for (box=0; box<level->num_my_boxes; box ++ )
{
int i;
int j;
int k;
int jStride;
jStride=level->my_boxes[box].jStride;
int kStride;
kStride=level->my_boxes[box].kStride;
int ghosts;
ghosts=level->my_boxes[box].ghosts;
int dim;
dim=level->my_boxes[box].dim;
/* cell centered array noting which cells are actually present */
double * valid;
valid=level->my_boxes[box].vectors[11]+(ghosts*((1+jStride)+kStride));
/* #pragma omp parallel for private(k,j,i) OMP_THREAD_WITHIN_A_BOX(level->threads_per_box) */
#pragma omp parallel for if(level->threads_per_box>1) num_threads(level->threads_per_box) private(i, j, k) collapse(2)
for (k=( - ghosts); k<(dim+ghosts); k ++ )
{
for (j=( - ghosts); j<(dim+ghosts); j ++ )
{
for (i=( - ghosts); i<(dim+ghosts); i ++ )
{
int ijk;
ijk=(i+(j*jStride))+(k*kStride);
valid[ijk]=1.0;
/* i.e. all cells including ghosts are valid for periodic BC's */
if ((level->domain_boundary_condition==1))
{
/* cells outside the domain boundaries are not valid */
if (((i+level->my_boxes[box].low.i)<0))
{
valid[ijk]=0.0;
}
if (((j+level->my_boxes[box].low.j)<0))
{
valid[ijk]=0.0;
}
if (((k+level->my_boxes[box].low.k)<0))
{
valid[ijk]=0.0;
}
if (((i+level->my_boxes[box].low.i)>=level->dim.i))
{
valid[ijk]=0.0;
}
if (((j+level->my_boxes[box].low.j)>=level->dim.j))
{
valid[ijk]=0.0;
}
if (((k+level->my_boxes[box].low.k)>=level->dim.k))
{
valid[ijk]=0.0;
}
}
}
}
}
}
level->cycles.blas1+=((uint64_t)(CycleTime()-_timeStart));
return ;
}

/* ------------------------------------------------------------------------------------------------------------------------------ */
void initialize_grid_to_scalar(level_type * level, int component_id, double scalar)
{
/* initializes the grid to a scalar while zero'ing the ghost zones... */
uint64_t _timeStart;
_timeStart=CycleTime();
int box;
/* #pragma omp parallel for private(box) OMP_THREAD_ACROSS_BOXES(level->concurrent_boxes) */
#pragma omp parallel for if(level->concurrent_boxes>1) num_threads(level->concurrent_boxes) private(box)
for (box=0; box<level->num_my_boxes; box ++ )
{
int i;
int j;
int k;
int jStride;
jStride=level->my_boxes[box].jStride;
int kStride;
kStride=level->my_boxes[box].kStride;
int ghosts;
ghosts=level->my_boxes[box].ghosts;
int dim;
dim=level->my_boxes[box].dim;
double * grid;
grid=level->my_boxes[box].vectors[component_id]+(ghosts*((1+jStride)+kStride));
/* #pragma omp parallel for private(k,j,i) OMP_THREAD_WITHIN_A_BOX(level->threads_per_box) */
#pragma omp parallel for if(level->threads_per_box>1) num_threads(level->threads_per_box) private(i, j, k) collapse(2)
for (k=( - ghosts); k<(dim+ghosts); k ++ )
{
for (j=( - ghosts); j<(dim+ghosts); j ++ )
{
for (i=( - ghosts); i<(dim+ghosts); i ++ )
{
int ijk;
ijk=(i+(j*jStride))+(k*kStride);
int ghostZone;
ghostZone=(((((i<0)||(j<0))||(k<0))||(i>=dim))||(j>=dim))||(k>=dim);
grid[ijk]=(ghostZone ? 0.0 : scalar);
}
}
}
}
level->cycles.blas1+=((uint64_t)(CycleTime()-_timeStart));
return ;
}

/* ------------------------------------------------------------------------------------------------------------------------------ */
void add_vectors(level_type * level, int id_c, double scale_a, int id_a, double scale_b, int id_b)
{
/* c=scale_aid_a + scale_b*id_b */
uint64_t _timeStart;
_timeStart=CycleTime();
int box;
/* #pragma omp parallel for private(box) OMP_THREAD_ACROSS_BOXES(level->concurrent_boxes) */
#pragma omp parallel for if(level->concurrent_boxes>1) num_threads(level->concurrent_boxes) private(box)
for (box=0; box<level->num_my_boxes; box ++ )
{
int i;
int j;
int k;
int jStride;
jStride=level->my_boxes[box].jStride;
int kStride;
kStride=level->my_boxes[box].kStride;
int ghosts;
ghosts=level->my_boxes[box].ghosts;
int dim;
dim=level->my_boxes[box].dim;
double * grid_c;
grid_c=level->my_boxes[box].vectors[id_c]+(ghosts*((1+jStride)+kStride));
double * grid_a;
grid_a=level->my_boxes[box].vectors[id_a]+(ghosts*((1+jStride)+kStride));
double * grid_b;
grid_b=level->my_boxes[box].vectors[id_b]+(ghosts*((1+jStride)+kStride));
/* #pragma omp parallel for private(k,j,i) OMP_THREAD_WITHIN_A_BOX(level->threads_per_box) */
#pragma omp parallel for if(level->threads_per_box>1) num_threads(level->threads_per_box) private(i, j, k) collapse(2)
for (k=0; k<dim; k ++ )
{
for (j=0; j<dim; j ++ )
{
for (i=0; i<dim; i ++ )
{
int ijk;
ijk=(i+(j*jStride))+(k*kStride);
grid_c[ijk]=((scale_a*grid_a[ijk])+(scale_b*grid_b[ijk]));
}
}
}
}
level->cycles.blas1+=((uint64_t)(CycleTime()-_timeStart));
return ;
}

/* ------------------------------------------------------------------------------------------------------------------------------ */
void mul_vectors(level_type * level, int id_c, double scale, int id_a, int id_b)
{
/* id_c=scaleid_a*id_b */
uint64_t _timeStart;
_timeStart=CycleTime();
int box;
/* #pragma omp parallel for private(box) OMP_THREAD_ACROSS_BOXES(level->concurrent_boxes) */
#pragma omp parallel for if(level->concurrent_boxes>1) num_threads(level->concurrent_boxes) private(box)
for (box=0; box<level->num_my_boxes; box ++ )
{
int i;
int j;
int k;
int jStride;
jStride=level->my_boxes[box].jStride;
int kStride;
kStride=level->my_boxes[box].kStride;
int ghosts;
ghosts=level->my_boxes[box].ghosts;
int dim;
dim=level->my_boxes[box].dim;
double * grid_c;
grid_c=level->my_boxes[box].vectors[id_c]+(ghosts*((1+jStride)+kStride));
double * grid_a;
grid_a=level->my_boxes[box].vectors[id_a]+(ghosts*((1+jStride)+kStride));
double * grid_b;
grid_b=level->my_boxes[box].vectors[id_b]+(ghosts*((1+jStride)+kStride));
/* #pragma omp parallel for private(k,j,i) OMP_THREAD_WITHIN_A_BOX(level->threads_per_box) */
#pragma omp parallel for if(level->threads_per_box>1) num_threads(level->threads_per_box) private(i, j, k) collapse(2)
for (k=0; k<dim; k ++ )
{
for (j=0; j<dim; j ++ )
{
for (i=0; i<dim; i ++ )
{
int ijk;
ijk=(i+(j*jStride))+(k*kStride);
grid_c[ijk]=((scale*grid_a[ijk])*grid_b[ijk]);
}
}
}
}
level->cycles.blas1+=((uint64_t)(CycleTime()-_timeStart));
return ;
}

void mul_vectors_gpu(level_type * level, int id_c, double scale, int id_a, int id_b)
{
/* id_c=scaleid_a*id_b */
uint64_t _timeStart;
int _ti_100_0;
_timeStart=CycleTime();
int box;
/* #pragma omp parallel for private(box) OMP_THREAD_ACROSS_BOXES(level->concurrent_boxes) */
/* #pragma omp parallel for private(box) if(level->concurrent_boxes>1) num_threads(level->concurrent_boxes) */
for (box=0; box<level->num_my_boxes; box ++ )
{
int i;
int j;
int k;
int jStride;
int k_0;
double * gpu__grid_a;
double * gpu__grid_b;
double * gpu__grid_c;
jStride=level->my_boxes[box].jStride;
int kStride;
kStride=level->my_boxes[box].kStride;
int ghosts;
ghosts=level->my_boxes[box].ghosts;
int dim;
dim=level->my_boxes[box].dim;
const uint64_t size = level->my_boxes[box].volume;
double * grid_c;
grid_c=level->my_boxes[box].vectors[id_c];
/* + ghosts(1+jStride+kStride); */
double * grid_a;
grid_a=level->my_boxes[box].vectors[id_a];
/* + ghosts(1+jStride+kStride); */
double * grid_b;
grid_b=level->my_boxes[box].vectors[id_b];
/* + ghosts(1+jStride+kStride); */
/* [DEBUG] Below will not be needed if all computations are on GPU. */
HI_set_async(box);
if ((HI_get_device_address(grid_a, ((void * *)( & gpu__grid_a)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, grid_a, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  update async(box) device(grid_a[0:size], grid_b[0:size]) \n");
exit(1);
}
gpuBytes=(sizeof (double)*size);
HI_memcpy_async(gpu__grid_a, grid_a, gpuBytes, HI_MemcpyHostToDevice, 0, box);
if ((HI_get_device_address(grid_b, ((void * *)( & gpu__grid_b)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, grid_b, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  update async(box) device(grid_a[0:size], grid_b[0:size]) \n");
exit(1);
}
gpuBytes=(sizeof (double)*size);
HI_memcpy_async(gpu__grid_b, grid_b, gpuBytes, HI_MemcpyHostToDevice, 0, box);

/* #pragma omp parallel for private(k,j,i) OMP_THREAD_WITHIN_A_BOX(level->threads_per_box) */
/* #pragma omp parallel for private(k,j,i) if(level->threads_per_box>1) num_threads(level->threads_per_box) collapse(2) */
HI_set_async(box);
if ((HI_get_device_address(grid_a, ((void * *)( & gpu__grid_a)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, grid_a, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  kernels loop async(box) collapse(3) gang(((int)ceil((((float)((dim*dim)*dim))/64.0F)))) worker(64) copyin(dim, ghosts, jStride, kStride, scale) present(grid_a[0:size], grid_b[0:size], grid_c[0:size]) private(_ti_100_0, i, j, k_0) \n");
exit(1);
}
if ((HI_get_device_address(grid_b, ((void * *)( & gpu__grid_b)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, grid_b, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  kernels loop async(box) collapse(3) gang(((int)ceil((((float)((dim*dim)*dim))/64.0F)))) worker(64) copyin(dim, ghosts, jStride, kStride, scale) present(grid_a[0:size], grid_b[0:size], grid_c[0:size]) private(_ti_100_0, i, j, k_0) \n");
exit(1);
}
if ((HI_get_device_address(grid_c, ((void * *)( & gpu__grid_c)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, grid_c, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  kernels loop async(box) collapse(3) gang(((int)ceil((((float)((dim*dim)*dim))/64.0F)))) worker(64) copyin(dim, ghosts, jStride, kStride, scale) present(grid_a[0:size], grid_b[0:size], grid_c[0:size]) private(_ti_100_0, i, j, k_0) \n");
exit(1);
}
int dimGrid_mul_vectors_gpu_kernel0[3];
dimGrid_mul_vectors_gpu_kernel0[0]=((int)ceil((((float)((dim*dim)*dim))/64.0F)));
dimGrid_mul_vectors_gpu_kernel0[1]=1;
dimGrid_mul_vectors_gpu_kernel0[2]=1;
int dimBlock_mul_vectors_gpu_kernel0[3];
dimBlock_mul_vectors_gpu_kernel0[0]=64;
dimBlock_mul_vectors_gpu_kernel0[1]=1;
dimBlock_mul_vectors_gpu_kernel0[2]=1;
gpuNumBlocks=((int)ceil((((float)((dim*dim)*dim))/64.0F)));
gpuNumThreads=64;
totalGpuNumThreads=(((int)ceil((((float)((dim*dim)*dim))/64.0F)))*64);
HI_register_kernel_arg("mul_vectors_gpu_kernel0",0,sizeof(void*),( & gpu__grid_a));
HI_register_kernel_arg("mul_vectors_gpu_kernel0",1,sizeof(void*),( & gpu__grid_b));
HI_register_kernel_arg("mul_vectors_gpu_kernel0",2,sizeof(void*),( & gpu__grid_c));
HI_register_kernel_arg("mul_vectors_gpu_kernel0",3,sizeof(void*),( & dim));
HI_register_kernel_arg("mul_vectors_gpu_kernel0",4,sizeof(void*),( & ghosts));
HI_register_kernel_arg("mul_vectors_gpu_kernel0",5,sizeof(void*),( & jStride));
HI_register_kernel_arg("mul_vectors_gpu_kernel0",6,sizeof(void*),( & kStride));
HI_register_kernel_arg("mul_vectors_gpu_kernel0",7,sizeof(void*),( & scale));
HI_kernel_call("mul_vectors_gpu_kernel0",dimGrid_mul_vectors_gpu_kernel0,dimBlock_mul_vectors_gpu_kernel0,box);
;
gpuNumBlocks=((int)ceil((((float)((dim*dim)*dim))/64.0F)));
/* [DEBUG] Below will not be needed if all computations are on GPU. */
HI_set_async(box);

if ((HI_get_device_address(grid_c, ((void * *)( & gpu__grid_c)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, grid_c, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  update async(box) host(grid_c[0:size]) \n");
exit(1);
}
gpuBytes=(sizeof (double)*size);
HI_memcpy_async(grid_c, gpu__grid_c, gpuBytes, HI_MemcpyDeviceToHost, 0, box);
}
acc_wait_all();
level->cycles.blas1+=((uint64_t)(CycleTime()-_timeStart));
return ;
}

/* ------------------------------------------------------------------------------------------------------------------------------ */
void invert_vector(level_type * level, int id_c, double scale_a, int id_a)
{
/* c[]=scale_aa[] */
uint64_t _timeStart;
_timeStart=CycleTime();
int box;
/* #pragma omp parallel for private(box) OMP_THREAD_ACROSS_BOXES(level->concurrent_boxes) */
#pragma omp parallel for if(level->concurrent_boxes>1) num_threads(level->concurrent_boxes) private(box)
for (box=0; box<level->num_my_boxes; box ++ )
{
int i;
int j;
int k;
int jStride;
jStride=level->my_boxes[box].jStride;
int kStride;
kStride=level->my_boxes[box].kStride;
int ghosts;
ghosts=level->my_boxes[box].ghosts;
int dim;
dim=level->my_boxes[box].dim;
double * grid_c;
grid_c=level->my_boxes[box].vectors[id_c]+(ghosts*((1+jStride)+kStride));
double * grid_a;
grid_a=level->my_boxes[box].vectors[id_a]+(ghosts*((1+jStride)+kStride));
/* #pragma omp parallel for private(k,j,i) OMP_THREAD_WITHIN_A_BOX(level->threads_per_box) */
#pragma omp parallel for if(level->threads_per_box>1) num_threads(level->threads_per_box) private(i, j, k) collapse(2)
for (k=0; k<dim; k ++ )
{
for (j=0; j<dim; j ++ )
{
for (i=0; i<dim; i ++ )
{
int ijk;
ijk=(i+(j*jStride))+(k*kStride);
grid_c[ijk]=(scale_a/grid_a[ijk]);
}
}
}
}
level->cycles.blas1+=((uint64_t)(CycleTime()-_timeStart));
return ;
}

/* ------------------------------------------------------------------------------------------------------------------------------ */
void scale_vector(level_type * level, int id_c, double scale_a, int id_a)
{
/* c[]=scale_aa[] */
uint64_t _timeStart;
_timeStart=CycleTime();
int box;
/* #pragma omp parallel for private(box) OMP_THREAD_ACROSS_BOXES(level->concurrent_boxes) */
#pragma omp parallel for if(level->concurrent_boxes>1) num_threads(level->concurrent_boxes) private(box)
for (box=0; box<level->num_my_boxes; box ++ )
{
int i;
int j;
int k;
int jStride;
jStride=level->my_boxes[box].jStride;
int kStride;
kStride=level->my_boxes[box].kStride;
int ghosts;
ghosts=level->my_boxes[box].ghosts;
int dim;
dim=level->my_boxes[box].dim;
double * grid_c;
grid_c=level->my_boxes[box].vectors[id_c]+(ghosts*((1+jStride)+kStride));
double * grid_a;
grid_a=level->my_boxes[box].vectors[id_a]+(ghosts*((1+jStride)+kStride));
/* #pragma omp parallel for private(k,j,i) OMP_THREAD_WITHIN_A_BOX(level->threads_per_box) */
#pragma omp parallel for if(level->threads_per_box>1) num_threads(level->threads_per_box) private(i, j, k) collapse(2)
for (k=0; k<dim; k ++ )
{
for (j=0; j<dim; j ++ )
{
for (i=0; i<dim; i ++ )
{
int ijk;
ijk=(i+(j*jStride))+(k*kStride);
grid_c[ijk]=(scale_a*grid_a[ijk]);
}
}
}
}
level->cycles.blas1+=((uint64_t)(CycleTime()-_timeStart));
return ;
}

void scale_vector_gpu(level_type * level, int id_c, double scale_a, int id_a)
{
/* c[]=scale_aa[] */
uint64_t _timeStart;
int _ti_100_0;
_timeStart=CycleTime();
int box;
/* #pragma omp parallel for private(box) OMP_THREAD_ACROSS_BOXES(level->concurrent_boxes) */
/* #pragma omp parallel for private(box) if(level->concurrent_boxes>1) num_threads(level->concurrent_boxes) */
for (box=0; box<level->num_my_boxes; box ++ )
{
int i;
int j;
int k;
int jStride;
int k_0;
double * gpu__grid_a;
double * gpu__grid_c;
jStride=level->my_boxes[box].jStride;
int kStride;
kStride=level->my_boxes[box].kStride;
int ghosts;
ghosts=level->my_boxes[box].ghosts;
int dim;
dim=level->my_boxes[box].dim;
double * grid_c;
grid_c=level->my_boxes[box].vectors[id_c];
/* + ghosts(1+jStride+kStride); */
double * grid_a;
grid_a=level->my_boxes[box].vectors[id_a];
/* + ghosts(1+jStride+kStride); */
const uint64_t tsize = level->my_boxes[box].volume;
/* [DEBUG] below will not be needed if all computations are on GPU. */
HI_set_async(box);
if ((HI_get_device_address(grid_a, ((void * *)( & gpu__grid_a)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, grid_a, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  update async(box) device(grid_a[0:tsize]) \n");
exit(1);
}
gpuBytes=(sizeof (double)*tsize);
HI_memcpy_async(gpu__grid_a, grid_a, gpuBytes, HI_MemcpyHostToDevice, 0, box);

/* #pragma omp parallel for private(k,j,i) OMP_THREAD_WITHIN_A_BOX(level->threads_per_box) */
/* #pragma omp parallel for private(k,j,i) if(level->threads_per_box>1) num_threads(level->threads_per_box) collapse(2) */
HI_set_async(box);
if ((HI_get_device_address(grid_a, ((void * *)( & gpu__grid_a)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, grid_a, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  kernels loop async(box) collapse(3) gang(((int)ceil((((float)((dim*dim)*dim))/64.0F)))) worker(64) copyin(dim, ghosts, jStride, kStride, scale_a) present(grid_a[0:tsize], grid_c[0:tsize]) private(_ti_100_0, i, j, k_0) \n");
exit(1);
}
if ((HI_get_device_address(grid_c, ((void * *)( & gpu__grid_c)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, grid_c, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  kernels loop async(box) collapse(3) gang(((int)ceil((((float)((dim*dim)*dim))/64.0F)))) worker(64) copyin(dim, ghosts, jStride, kStride, scale_a) present(grid_a[0:tsize], grid_c[0:tsize]) private(_ti_100_0, i, j, k_0) \n");
exit(1);
}
int dimGrid_scale_vector_gpu_kernel0[3];
dimGrid_scale_vector_gpu_kernel0[0]=((int)ceil((((float)((dim*dim)*dim))/64.0F)));
dimGrid_scale_vector_gpu_kernel0[1]=1;
dimGrid_scale_vector_gpu_kernel0[2]=1;
int dimBlock_scale_vector_gpu_kernel0[3];
dimBlock_scale_vector_gpu_kernel0[0]=64;
dimBlock_scale_vector_gpu_kernel0[1]=1;
dimBlock_scale_vector_gpu_kernel0[2]=1;
gpuNumBlocks=((int)ceil((((float)((dim*dim)*dim))/64.0F)));
gpuNumThreads=64;
totalGpuNumThreads=(((int)ceil((((float)((dim*dim)*dim))/64.0F)))*64);
HI_register_kernel_arg("scale_vector_gpu_kernel0",0,sizeof(void*),( & gpu__grid_a));
HI_register_kernel_arg("scale_vector_gpu_kernel0",1,sizeof(void*),( & gpu__grid_c));
HI_register_kernel_arg("scale_vector_gpu_kernel0",2,sizeof(void*),( & dim));
HI_register_kernel_arg("scale_vector_gpu_kernel0",3,sizeof(void*),( & ghosts));
HI_register_kernel_arg("scale_vector_gpu_kernel0",4,sizeof(void*),( & jStride));
HI_register_kernel_arg("scale_vector_gpu_kernel0",5,sizeof(void*),( & kStride));
HI_register_kernel_arg("scale_vector_gpu_kernel0",6,sizeof(void*),( & scale_a));
HI_kernel_call("scale_vector_gpu_kernel0",dimGrid_scale_vector_gpu_kernel0,dimBlock_scale_vector_gpu_kernel0,box);
;
gpuNumBlocks=((int)ceil((((float)((dim*dim)*dim))/64.0F)));
/* [DEBUG] below will not be needed if all computations are on GPU. */
HI_set_async(box);

if ((HI_get_device_address(grid_c, ((void * *)( & gpu__grid_c)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, grid_c, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  update async(box) host(grid_c[0:tsize]) \n");
exit(1);
}
gpuBytes=(sizeof (double)*tsize);
HI_memcpy_async(grid_c, gpu__grid_c, gpuBytes, HI_MemcpyDeviceToHost, 0, box);
}
acc_wait_all();
level->cycles.blas1+=((uint64_t)(CycleTime()-_timeStart));
return ;
}

/* ------------------------------------------------------------------------------------------------------------------------------ */
double dot(level_type * level, int id_a, int id_b)
{
uint64_t _timeStart;
_timeStart=CycleTime();
int box;
double a_dot_b_level = 0.0;
/* FIX, schedule(static) is a stand in to guarantee reproducibility... */
/* #pragma omp parallel for private(box) OMP_THREAD_ACROSS_BOXES(level->concurrent_boxes) reduction(+:a_dot_b_level) schedule(static) */
#pragma omp parallel for if(level->concurrent_boxes>1) num_threads(level->concurrent_boxes) private(box) reduction(+: a_dot_b_level) schedule(static)
for (box=0; box<level->num_my_boxes; box ++ )
{
int i;
int j;
int k;
int jStride;
jStride=level->my_boxes[box].jStride;
int kStride;
kStride=level->my_boxes[box].kStride;
int ghosts;
ghosts=level->my_boxes[box].ghosts;
int dim;
dim=level->my_boxes[box].dim;
double * grid_a;
grid_a=level->my_boxes[box].vectors[id_a]+(ghosts*((1+jStride)+kStride));
/* i.e. [0] = first non ghost zone point */
double * grid_b;
grid_b=level->my_boxes[box].vectors[id_b]+(ghosts*((1+jStride)+kStride));
double a_dot_b_box = 0.0;
/* #pragma omp parallel for private(k,j,i) OMP_THREAD_WITHIN_A_BOX(level->threads_per_box) reduction(+:a_dot_b_box) schedule(static) */
#pragma omp parallel for if(level->threads_per_box>1) num_threads(level->threads_per_box) private(i, j, k) reduction(+: a_dot_b_box) schedule(static) collapse(2)
for (k=0; k<dim; k ++ )
{
for (j=0; j<dim; j ++ )
{
for (i=0; i<dim; i ++ )
{
int ijk;
ijk=(i+(j*jStride))+(k*kStride);
a_dot_b_box+=(grid_a[ijk]*grid_b[ijk]);
}
}
}
a_dot_b_level+=a_dot_b_box;
}
level->cycles.blas1+=((uint64_t)(CycleTime()-_timeStart));
uint64_t _timeStartAllReduce;
_timeStartAllReduce=CycleTime();
double send;
send=a_dot_b_level;
MPI_Allreduce(( & send), ( & a_dot_b_level), 1, ((MPI_Datatype)((void *)( & ompi_mpi_double))), ((MPI_Op)((void *)( & ompi_mpi_op_sum))), level->MPI_COMM_ALLREDUCE);
uint64_t _timeEndAllReduce;
_timeEndAllReduce=CycleTime();
level->cycles.collectives+=((uint64_t)(_timeEndAllReduce-_timeStartAllReduce));
return a_dot_b_level;
}

/* ------------------------------------------------------------------------------------------------------------------------------ */
double norm(level_type * level, int component_id)
{
/* implements the max norm */
uint64_t _timeStart;
_timeStart=CycleTime();
int box;
double max_norm = 0.0;
/* FIX, schedule(static) is a stand in to guarantee reproducibility... */
for (box=0; box<level->num_my_boxes; box ++ )
{
int i;
int j;
int k;
int jStride;
jStride=level->my_boxes[box].jStride;
int kStride;
kStride=level->my_boxes[box].kStride;
int ghosts;
ghosts=level->my_boxes[box].ghosts;
int dim;
dim=level->my_boxes[box].dim;
double * grid;
grid=level->my_boxes[box].vectors[component_id]+(ghosts*((1+jStride)+kStride));
/* i.e. [0] = first non ghost zone point */
double box_norm = 0.0;
for (k=0; k<dim; k ++ )
{
for (j=0; j<dim; j ++ )
{
for (i=0; i<dim; i ++ )
{
int ijk;
ijk=(i+(j*jStride))+(k*kStride);
double fabs_grid_ijk;
fabs_grid_ijk=fabs(grid[ijk]);
if ((fabs_grid_ijk>box_norm))
{
box_norm=fabs_grid_ijk;
}
/* max norm */
}
}
}
if ((box_norm>max_norm))
{
max_norm=box_norm;
}
}
/* box list */
level->cycles.blas1+=((uint64_t)(CycleTime()-_timeStart));
uint64_t _timeStartAllReduce;
_timeStartAllReduce=CycleTime();
double send;
send=max_norm;
MPI_Allreduce(( & send), ( & max_norm), 1, ((MPI_Datatype)((void *)( & ompi_mpi_double))), ((MPI_Op)((void *)( & ompi_mpi_op_max))), level->MPI_COMM_ALLREDUCE);
uint64_t _timeEndAllReduce;
_timeEndAllReduce=CycleTime();
level->cycles.collectives+=((uint64_t)(_timeEndAllReduce-_timeStartAllReduce));
return max_norm;
}

double norm_gpu(level_type * level, int component_id)
{
/* implements the max norm */
uint64_t _timeStart;
int _ti_100_0;
double * ggred__box_norm = 0;
double * extred__box_norm = 0;
int _ti_100_1000;
_timeStart=CycleTime();
int box;
double max_norm = 0.0;
/* FIX, schedule(static) is a stand in to guarantee reproducibility... */
for (box=0; box<level->num_my_boxes; box ++ )
{
int i;
int j;
int k;
int jStride;
double * gpu__grid;
jStride=level->my_boxes[box].jStride;
int kStride;
kStride=level->my_boxes[box].kStride;
int ghosts;
ghosts=level->my_boxes[box].ghosts;
int dim;
dim=level->my_boxes[box].dim;
const uint64_t size = level->my_boxes[box].volume;
double * grid;
grid=level->my_boxes[box].vectors[component_id];
/* + ghosts(1+jStride+kStride); i.e. [0] = first non ghost zone point */
double box_norm = 0.0;
if ((HI_get_device_address(grid, ((void * *)( & gpu__grid)), DEFAULT_QUEUE)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, grid, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  kernels loop collapse(3) gang(((int)ceil((((float)((dim*dim)*dim))/64.0F)))) worker(64) independent reduction(max: box_norm) copyin(dim, ghosts, jStride, kStride) present(grid[0:size]) private(_ti_100_0, i, j, k) \n");
exit(1);
}
int dimGrid_norm_gpu_kernel0[3];
dimGrid_norm_gpu_kernel0[0]=((int)ceil((((float)((dim*dim)*dim))/64.0F)));
dimGrid_norm_gpu_kernel0[1]=1;
dimGrid_norm_gpu_kernel0[2]=1;
int dimBlock_norm_gpu_kernel0[3];
dimBlock_norm_gpu_kernel0[0]=64;
dimBlock_norm_gpu_kernel0[1]=1;
dimBlock_norm_gpu_kernel0[2]=1;
gpuNumBlocks=((int)ceil((((float)((dim*dim)*dim))/64.0F)));
gpuNumThreads=64;
totalGpuNumThreads=(((int)ceil((((float)((dim*dim)*dim))/64.0F)))*64);
gpuBytes=(gpuNumBlocks*sizeof (double));
HI_tempMalloc1D(((void * *)( & ggred__box_norm)), gpuBytes, acc_device_current);
HI_tempMalloc1D(((void * *)( & extred__box_norm)), gpuBytes, acc_device_host);
HI_register_kernel_arg("norm_gpu_kernel0",0,sizeof(void*),( & ggred__box_norm));
HI_register_kernel_arg("norm_gpu_kernel0",1,sizeof(void*),( & gpu__grid));
HI_register_kernel_arg("norm_gpu_kernel0",2,sizeof(void*),( & dim));
HI_register_kernel_arg("norm_gpu_kernel0",3,sizeof(void*),( & ghosts));
HI_register_kernel_arg("norm_gpu_kernel0",4,sizeof(void*),( & jStride));
HI_register_kernel_arg("norm_gpu_kernel0",5,sizeof(void*),( & kStride));
HI_kernel_call("norm_gpu_kernel0",dimGrid_norm_gpu_kernel0,dimBlock_norm_gpu_kernel0);
HI_synchronize();
gpuNumBlocks=((int)ceil((((float)((dim*dim)*dim))/64.0F)));
gpuBytes=(gpuNumBlocks*sizeof (double));
HI_memcpy(extred__box_norm, ggred__box_norm, gpuBytes, HI_MemcpyDeviceToHost, 0);
for (_ti_100_1000=0; _ti_100_1000<gpuNumBlocks; _ti_100_1000 ++ )
{
box_norm=MAX(box_norm, extred__box_norm[_ti_100_1000]);
}
HI_tempFree(((void * *)( & extred__box_norm)), acc_device_host);
HI_tempFree(((void * *)( & ggred__box_norm)), acc_device_current);
if ((box_norm>max_norm))
{
max_norm=box_norm;
}
}
/* box list */
level->cycles.blas1+=((uint64_t)(CycleTime()-_timeStart));
uint64_t _timeStartAllReduce;
_timeStartAllReduce=CycleTime();
double send;
send=max_norm;
MPI_Allreduce(( & send), ( & max_norm), 1, ((MPI_Datatype)((void *)( & ompi_mpi_double))), ((MPI_Op)((void *)( & ompi_mpi_op_max))), level->MPI_COMM_ALLREDUCE);
uint64_t _timeEndAllReduce;
_timeEndAllReduce=CycleTime();
level->cycles.collectives+=((uint64_t)(_timeEndAllReduce-_timeStartAllReduce));
return max_norm;
}

/* ------------------------------------------------------------------------------------------------------------------------------ */
double mean(level_type * level, int id_a)
{
uint64_t _timeStart;
_timeStart=CycleTime();
int box;
double sum_level = 0.0;
/* #pragma omp parallel for private(box) OMP_THREAD_ACROSS_BOXES(level->concurrent_boxes) reduction(+:sum_level) schedule(static) */
#pragma omp parallel for if(level->concurrent_boxes>1) num_threads(level->concurrent_boxes) private(box) reduction(+: sum_level) schedule(static)
for (box=0; box<level->num_my_boxes; box ++ )
{
int i;
int j;
int k;
int jStride;
jStride=level->my_boxes[box].jStride;
int kStride;
kStride=level->my_boxes[box].kStride;
int ghosts;
ghosts=level->my_boxes[box].ghosts;
int dim;
dim=level->my_boxes[box].dim;
double * grid_a;
grid_a=level->my_boxes[box].vectors[id_a]+(ghosts*((1+jStride)+kStride));
/* i.e. [0] = first non ghost zone point */
double sum_box = 0.0;
/* #pragma omp parallel for private(k,j,i) OMP_THREAD_WITHIN_A_BOX(level->threads_per_box) reduction(+:sum_box) schedule(static) */
#pragma omp parallel for if(level->threads_per_box>1) num_threads(level->threads_per_box) private(i, j, k) reduction(+: sum_box) schedule(static) collapse(2)
for (k=0; k<dim; k ++ )
{
for (j=0; j<dim; j ++ )
{
for (i=0; i<dim; i ++ )
{
int ijk;
ijk=(i+(j*jStride))+(k*kStride);
sum_box+=grid_a[ijk];
}
}
}
sum_level+=sum_box;
}
level->cycles.blas1+=((uint64_t)(CycleTime()-_timeStart));
double ncells_level;
ncells_level=(((double)level->dim.i)*((double)level->dim.j))*((double)level->dim.k);
uint64_t _timeStartAllReduce;
_timeStartAllReduce=CycleTime();
double send;
send=sum_level;
MPI_Allreduce(( & send), ( & sum_level), 1, ((MPI_Datatype)((void *)( & ompi_mpi_double))), ((MPI_Op)((void *)( & ompi_mpi_op_sum))), level->MPI_COMM_ALLREDUCE);
uint64_t _timeEndAllReduce;
_timeEndAllReduce=CycleTime();
level->cycles.collectives+=((uint64_t)(_timeEndAllReduce-_timeStartAllReduce));
double mean_level;
mean_level=sum_level/ncells_level;
return mean_level;
}

double mean_gpu(level_type * level, int id_a)
{
uint64_t _timeStart;
int _ti_100_0;
double * ggred__sum_box = 0;
double * extred__sum_box = 0;
int _ti_100_1000;
_timeStart=CycleTime();
int box;
double sum_level = 0.0;
for (box=0; box<level->num_my_boxes; box ++ )
{
int i;
int j;
int k;
int jStride;
double * gpu__grid_a;
jStride=level->my_boxes[box].jStride;
int kStride;
kStride=level->my_boxes[box].kStride;
int ghosts;
ghosts=level->my_boxes[box].ghosts;
int dim;
dim=level->my_boxes[box].dim;
const uint64_t size = level->my_boxes[box].volume;
double * grid_a;
grid_a=level->my_boxes[box].vectors[id_a];
/* + ghosts(1+jStride+kStride); i.e. [0] = first non ghost zone point */
double sum_box = 0.0;
if ((HI_get_device_address(grid_a, ((void * *)( & gpu__grid_a)), DEFAULT_QUEUE)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, grid_a, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  kernels loop collapse(3) gang(((int)ceil((((float)((dim*dim)*dim))/64.0F)))) worker(64) independent reduction(+: sum_box) copyin(dim, ghosts, jStride, kStride) present(grid_a[0:size]) private(_ti_100_0, i, j, k) \n");
exit(1);
}
int dimGrid_mean_gpu_kernel0[3];
dimGrid_mean_gpu_kernel0[0]=((int)ceil((((float)((dim*dim)*dim))/64.0F)));
dimGrid_mean_gpu_kernel0[1]=1;
dimGrid_mean_gpu_kernel0[2]=1;
int dimBlock_mean_gpu_kernel0[3];
dimBlock_mean_gpu_kernel0[0]=64;
dimBlock_mean_gpu_kernel0[1]=1;
dimBlock_mean_gpu_kernel0[2]=1;
gpuNumBlocks=((int)ceil((((float)((dim*dim)*dim))/64.0F)));
gpuNumThreads=64;
totalGpuNumThreads=(((int)ceil((((float)((dim*dim)*dim))/64.0F)))*64);
gpuBytes=(gpuNumBlocks*sizeof (double));
HI_tempMalloc1D(((void * *)( & ggred__sum_box)), gpuBytes, acc_device_current);
HI_tempMalloc1D(((void * *)( & extred__sum_box)), gpuBytes, acc_device_host);
HI_register_kernel_arg("mean_gpu_kernel0",0,sizeof(void*),( & ggred__sum_box));
HI_register_kernel_arg("mean_gpu_kernel0",1,sizeof(void*),( & gpu__grid_a));
HI_register_kernel_arg("mean_gpu_kernel0",2,sizeof(void*),( & dim));
HI_register_kernel_arg("mean_gpu_kernel0",3,sizeof(void*),( & ghosts));
HI_register_kernel_arg("mean_gpu_kernel0",4,sizeof(void*),( & jStride));
HI_register_kernel_arg("mean_gpu_kernel0",5,sizeof(void*),( & kStride));
HI_kernel_call("mean_gpu_kernel0",dimGrid_mean_gpu_kernel0,dimBlock_mean_gpu_kernel0);
HI_synchronize();
gpuNumBlocks=((int)ceil((((float)((dim*dim)*dim))/64.0F)));
gpuBytes=(gpuNumBlocks*sizeof (double));
HI_memcpy(extred__sum_box, ggred__sum_box, gpuBytes, HI_MemcpyDeviceToHost, 0);
for (_ti_100_1000=0; _ti_100_1000<gpuNumBlocks; _ti_100_1000 ++ )
{
sum_box+=extred__sum_box[_ti_100_1000];
}
HI_tempFree(((void * *)( & extred__sum_box)), acc_device_host);
HI_tempFree(((void * *)( & ggred__sum_box)), acc_device_current);
sum_level+=sum_box;
}
level->cycles.blas1+=((uint64_t)(CycleTime()-_timeStart));
double ncells_level;
ncells_level=(((double)level->dim.i)*((double)level->dim.j))*((double)level->dim.k);
uint64_t _timeStartAllReduce;
_timeStartAllReduce=CycleTime();
double send;
send=sum_level;
MPI_Allreduce(( & send), ( & sum_level), 1, ((MPI_Datatype)((void *)( & ompi_mpi_double))), ((MPI_Op)((void *)( & ompi_mpi_op_sum))), level->MPI_COMM_ALLREDUCE);
uint64_t _timeEndAllReduce;
_timeEndAllReduce=CycleTime();
level->cycles.collectives+=((uint64_t)(_timeEndAllReduce-_timeStartAllReduce));
double mean_level;
mean_level=sum_level/ncells_level;
return mean_level;
}

void shift_vector(level_type * level, int id_c, int id_a, double shift_a)
{
uint64_t _timeStart;
_timeStart=CycleTime();
int box;
/* #pragma omp parallel for private(box) OMP_THREAD_ACROSS_BOXES(level->concurrent_boxes) */
#pragma omp parallel for if(level->concurrent_boxes>1) num_threads(level->concurrent_boxes) private(box)
for (box=0; box<level->num_my_boxes; box ++ )
{
int i;
int j;
int k;
int jStride;
jStride=level->my_boxes[box].jStride;
int kStride;
kStride=level->my_boxes[box].kStride;
int ghosts;
ghosts=level->my_boxes[box].ghosts;
int dim;
dim=level->my_boxes[box].dim;
double * grid_c;
grid_c=level->my_boxes[box].vectors[id_c]+(ghosts*((1+jStride)+kStride));
/* i.e. [0] = first non ghost zone point */
double * grid_a;
grid_a=level->my_boxes[box].vectors[id_a]+(ghosts*((1+jStride)+kStride));
/* i.e. [0] = first non ghost zone point */
/* #pragma omp parallel for private(k,j,i) OMP_THREAD_WITHIN_A_BOX(level->threads_per_box) */
#pragma omp parallel for if(level->threads_per_box>1) num_threads(level->threads_per_box) private(i, j, k) collapse(2)
for (k=0; k<dim; k ++ )
{
for (j=0; j<dim; j ++ )
{
for (i=0; i<dim; i ++ )
{
int ijk;
ijk=(i+(j*jStride))+(k*kStride);
grid_c[ijk]=(grid_a[ijk]+shift_a);
}
}
}
}
level->cycles.blas1+=((uint64_t)(CycleTime()-_timeStart));
return ;
}

void shift_vector_gpu(level_type * level, int id_c, int id_a, double shift_a)
{
uint64_t _timeStart;
int _ti_100_0;
_timeStart=CycleTime();
int box;
for (box=0; box<level->num_my_boxes; box ++ )
{
int i;
int j;
int k;
int jStride;
double * gpu__grid_a;
double * gpu__grid_c;
jStride=level->my_boxes[box].jStride;
int kStride;
kStride=level->my_boxes[box].kStride;
int ghosts;
ghosts=level->my_boxes[box].ghosts;
int dim;
dim=level->my_boxes[box].dim;
const uint64_t size = level->my_boxes[box].volume;
double * grid_c;
grid_c=level->my_boxes[box].vectors[id_c];
/* + ghosts(1+jStride+kStride); i.e. [0] = first non ghost zone point */
double * grid_a;
grid_a=level->my_boxes[box].vectors[id_a];
/* + ghosts(1+jStride+kStride); i.e. [0] = first non ghost zone point */
/* [DEBUG] Below will not be needed if all computations are on GPU. */
HI_set_async(box);
if ((HI_get_device_address(grid_a, ((void * *)( & gpu__grid_a)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, grid_a, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  update async(box) device(grid_a[0:size]) \n");
exit(1);
}
gpuBytes=(sizeof (double)*size);
HI_memcpy_async(gpu__grid_a, grid_a, gpuBytes, HI_MemcpyHostToDevice, 0, box);

HI_set_async(box);
if ((HI_get_device_address(grid_a, ((void * *)( & gpu__grid_a)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, grid_a, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  kernels loop async(box) collapse(3) gang(((int)ceil((((float)((dim*dim)*dim))/64.0F)))) worker(64) independent copyin(dim, ghosts, jStride, kStride, shift_a) present(grid_a[0:size], grid_c[0:size]) private(_ti_100_0, i, j, k) \n");
exit(1);
}
if ((HI_get_device_address(grid_c, ((void * *)( & gpu__grid_c)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, grid_c, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  kernels loop async(box) collapse(3) gang(((int)ceil((((float)((dim*dim)*dim))/64.0F)))) worker(64) independent copyin(dim, ghosts, jStride, kStride, shift_a) present(grid_a[0:size], grid_c[0:size]) private(_ti_100_0, i, j, k) \n");
exit(1);
}
int dimGrid_shift_vector_gpu_kernel0[3];
dimGrid_shift_vector_gpu_kernel0[0]=((int)ceil((((float)((dim*dim)*dim))/64.0F)));
dimGrid_shift_vector_gpu_kernel0[1]=1;
dimGrid_shift_vector_gpu_kernel0[2]=1;
int dimBlock_shift_vector_gpu_kernel0[3];
dimBlock_shift_vector_gpu_kernel0[0]=64;
dimBlock_shift_vector_gpu_kernel0[1]=1;
dimBlock_shift_vector_gpu_kernel0[2]=1;
gpuNumBlocks=((int)ceil((((float)((dim*dim)*dim))/64.0F)));
gpuNumThreads=64;
totalGpuNumThreads=(((int)ceil((((float)((dim*dim)*dim))/64.0F)))*64);
HI_register_kernel_arg("shift_vector_gpu_kernel0",0,sizeof(void*),( & gpu__grid_a));
HI_register_kernel_arg("shift_vector_gpu_kernel0",1,sizeof(void*),( & gpu__grid_c));
HI_register_kernel_arg("shift_vector_gpu_kernel0",2,sizeof(void*),( & dim));
HI_register_kernel_arg("shift_vector_gpu_kernel0",3,sizeof(void*),( & ghosts));
HI_register_kernel_arg("shift_vector_gpu_kernel0",4,sizeof(void*),( & jStride));
HI_register_kernel_arg("shift_vector_gpu_kernel0",5,sizeof(void*),( & kStride));
HI_register_kernel_arg("shift_vector_gpu_kernel0",6,sizeof(void*),( & shift_a));
HI_kernel_call("shift_vector_gpu_kernel0",dimGrid_shift_vector_gpu_kernel0,dimBlock_shift_vector_gpu_kernel0,box);
;
gpuNumBlocks=((int)ceil((((float)((dim*dim)*dim))/64.0F)));
/* [DEBUG] Below will not be needed if all computations are on GPU. */
HI_set_async(box);

if ((HI_get_device_address(grid_c, ((void * *)( & gpu__grid_c)), box)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, grid_c, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  update async(box) host(grid_c[0:size]) \n");
exit(1);
}
gpuBytes=(sizeof (double)*size);
HI_memcpy_async(grid_c, gpu__grid_c, gpuBytes, HI_MemcpyDeviceToHost, 0, box);
}
acc_wait_all();
level->cycles.blas1+=((uint64_t)(CycleTime()-_timeStart));
return ;
}

/* ------------------------------------------------------------------------------------------------------------------------------ */
void project_cell_to_face(level_type * level, int id_cell, int id_face, int dir)
{
uint64_t _timeStart;
_timeStart=CycleTime();
int box;
/* #pragma omp parallel for private(box) OMP_THREAD_ACROSS_BOXES(level->concurrent_boxes) */
#pragma omp parallel for if(level->concurrent_boxes>1) num_threads(level->concurrent_boxes) private(box)
for (box=0; box<level->num_my_boxes; box ++ )
{
int i;
int j;
int k;
int jStride;
jStride=level->my_boxes[box].jStride;
int kStride;
kStride=level->my_boxes[box].kStride;
int ghosts;
ghosts=level->my_boxes[box].ghosts;
int dim;
dim=level->my_boxes[box].dim;
double * grid_cell;
grid_cell=level->my_boxes[box].vectors[id_cell]+(ghosts*((1+jStride)+kStride));
double * grid_face;
grid_face=level->my_boxes[box].vectors[id_face]+(ghosts*((1+jStride)+kStride));
int stride;
switch (dir)
{
case 0:
stride=1;
break;
/* i-direction */
case 1:
stride=jStride;
break;
/* j-direction */
case 2:
stride=kStride;
break;
/* k-direction */
}
/* #pragma omp parallel for private(k,j,i) OMP_THREAD_WITHIN_A_BOX(level->threads_per_box) */
#pragma omp parallel for if(level->threads_per_box>1) num_threads(level->threads_per_box) private(i, j, k) collapse(2)
for (k=0; k<=dim; k ++ )
{
/* <= to ensure you do low and high faces */
for (j=0; j<=dim; j ++ )
{
for (i=0; i<=dim; i ++ )
{
int ijk;
ijk=(i+(j*jStride))+(k*kStride);
grid_face[ijk]=(0.5*(grid_cell[(ijk-stride)]+grid_cell[ijk]));
/* simple linear interpolation */
}
}
}
}
level->cycles.blas1+=((uint64_t)(CycleTime()-_timeStart));
return ;
}

/* ------------------------------------------------------------------------------------------------------------------------------ */
double error(level_type * level, int id_a, int id_b)
{
double h3;
double _ret_val_0;
h3=(level->h*level->h)*level->h;
/*  */
add_vectors(level, 0, 1.0, id_a, ( - 1.0), id_b);
/* VECTOR_TEMP = id_a - id_b */
/*  */
double max;
max=norm(level, 0);
return max;
/* max norm of error function */
/*  */
/*  */
double L2;
L2=sqrt((dot(level, 0, 0)*h3));
return L2;
/* normalized L2 error ? */
return _ret_val_0;
}

