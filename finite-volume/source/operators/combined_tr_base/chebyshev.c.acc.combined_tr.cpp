/* ------------------------------------------------------------------------------------------------------------------------------ */
/* Samuel Williams */
/* SWWilliams@lbl.gov */
/* Lawrence Berkeley National Lab */
/* ------------------------------------------------------------------------------------------------------------------------------ */
/* Based on Yousef Saad's Iterative Methods for Sparse Linear Algebra, Algorithm 12.1, page 399 */
/* ------------------------------------------------------------------------------------------------------------------------------ */
#include "../level.h"

#ifndef __O2G_HEADER__ 

#define __O2G_HEADER__ 

/*******************************************/
/* Added codes for OpenACC2GPU translation */
/*******************************************/
#include <openacc.h>
#include <openaccrt.h>
#include <math.h>
#include <float.h>
#include <limits.h>
#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#ifdef __cplusplus
#define restrict __restrict__
#endif

/**********************************************************/
/* Maximum width of linear memory bound to texture memory */
/**********************************************************/
/* width in bytes */
#define LMAX_WIDTH    134217728
/**********************************/
/* Maximum memory pitch (in bytes)*/
/**********************************/
#define MAX_PITCH   262144
/****************************************/
/* Maximum allowed GPU global memory    */
/* (should be less than actual size ) */
/****************************************/
#define MAX_GMSIZE  1600000000
/****************************************/
/* Maximum allowed GPU shared memory    */
/****************************************/
#define MAX_SMSIZE  16384
/********************************************/
/* Maximum size of each dimension of a grid */
/********************************************/
#define MAX_GDIMENSION  65535

#define NUM_WORKERS  64


static int gpuNumThreads = NUM_WORKERS;
static int totalGpuNumThreads;
static int gpuNumBlocks;
static unsigned int gpuBytes = 0;

#endif 
/* End of __O2G_HEADER__ */



void smooth(level_type * level, int x_id, int rhs_id, double a, double b)
{

////////////////////////////////
// CUDA Device Initialization //
////////////////////////////////

if (((CHEBYSHEV_DEGREE*NUM_SMOOTHS)&1))
{
printf("error... CHEBYSHEV_DEGREE*NUM_SMOOTHS must be even for the chebyshev smoother...\n");
exit(0);
}
if (((level->dominant_eigenvalue_of_DinvA<=0.0)&&(level->my_rank==0)))
{
printf("dominant_eigenvalue_of_DinvA <= 0.0 !\n");
}
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
int box;
int s;
/* compute the Chebyshev coefficients... */
double beta;
beta=1.0*level->dominant_eigenvalue_of_DinvA;
/* double alpha    = 0.300000beta; */
/* double alpha    = 0.250000beta; */
/* double alpha    = 0.166666beta; */
double alpha;
alpha=0.125*beta;
double theta;
theta=0.5*(beta+alpha);
/* center of the spectral ellipse */
double delta;
delta=0.5*(beta-alpha);
/* major axis? */
double sigma;
sigma=theta/delta;
double rho_n;
rho_n=1/sigma;
/* rho_0 */
double chebyshev_c1[CHEBYSHEV_DEGREE];
/* + c1(x_n-x_nm1) == rho_n*rho_nm1 */
double chebyshev_c2[CHEBYSHEV_DEGREE];
/* + c2(b-Ax_n) */
chebyshev_c1[0]=0.0;
chebyshev_c2[0]=(1/theta);
for (s=1; s<CHEBYSHEV_DEGREE; s ++ )
{
double rho_nm1;
rho_nm1=rho_n;
rho_n=(1.0/((2.0*sigma)-rho_nm1));
chebyshev_c1[s]=(rho_n*rho_nm1);
chebyshev_c2[s]=((rho_n*2.0)/delta);
}
for (s=0; s<(CHEBYSHEV_DEGREE*NUM_SMOOTHS); s ++ )
{
/* get ghost zone data... Chebyshev ping pongs between x_id and VECTOR_TEMP */
if (((s&1)==0))
{
exchange_boundary(level, x_id, stencil_is_star_shaped());
apply_BCs(level, x_id);
}
else
{
exchange_boundary(level, VECTOR_TEMP, stencil_is_star_shaped());
apply_BCs(level, VECTOR_TEMP);
}
/* apply the smoother... Chebyshev ping pongs between x_id and VECTOR_TEMP */
uint64_t _timeStart;
_timeStart=CycleTime();
#pragma omp parallel for if(level->concurrent_boxes) num_threads(level->concurrent_boxes) private(box)
for (box=0; box<level->num_my_boxes; box ++ )
{
int ghosts;
double * gpu__vectors;
ghosts=level->box_ghosts;
const int jStride = level->my_boxes[box].jStride;
const int kStride = level->my_boxes[box].kStride;
const int dim = level->my_boxes[box].dim;
int dimGrid_smooth_kernel0[3];
dimGrid_smooth_kernel0[0]=dim;
dimGrid_smooth_kernel0[1]=1;
dimGrid_smooth_kernel0[2]=1;
int dimBlock_smooth_kernel0[3];
dimBlock_smooth_kernel0[0]=64;
dimBlock_smooth_kernel0[1]=1;
dimBlock_smooth_kernel0[2]=1;
gpuNumBlocks=dim;
gpuNumThreads=64;
totalGpuNumThreads=(64*dim);
/*      const int    size = (dim+2ghosts) + ((dim + 2*ghosts)*jStride) + ((dim + 2*ghosts)*kStride); */
const uint64_t size = level->my_boxes[box].volume;
const double h2inv = 1.0/(level->h*level->h);
/*

      const double __restrict__ rhs      = level->my_boxes[box].vectors[       rhs_id]; + ghosts*(1+jStride+kStride);
      const double * __restrict__ alpha    = level->my_boxes[box].vectors[VECTOR_ALPHA ]; + ghosts*(1+jStride+kStride);
      const double * __restrict__ beta_i   = level->my_boxes[box].vectors[VECTOR_BETA_I]; + ghosts*(1+jStride+kStride);
      const double * __restrict__ beta_j   = level->my_boxes[box].vectors[VECTOR_BETA_J]; + ghosts*(1+jStride+kStride);
      const double * __restrict__ beta_k   = level->my_boxes[box].vectors[VECTOR_BETA_K]; + ghosts*(1+jStride+kStride);
      const double * __restrict__ Dinv     = level->my_boxes[box].vectors[VECTOR_DINV  ]; + ghosts*(1+jStride+kStride);
      const double * __restrict__ valid    = level->my_boxes[box].vectors[VECTOR_VALID ]; + ghosts*(1+jStride+kStride); cell is inside the domain

*/
/*            double __restrict__ x_np1; */
/*      const double __restrict__ x_n; */
/*      const double __restrict__ x_nm1; */
/*                       if((s&1)==0){x_n    = level->my_boxes[box].vectors[         x_id];+ ghosts(1+jStride+kStride); */
/* x_nm1  = level->my_boxes[box].vectors[VECTOR_TEMP  ];+ ghosts(1+jStride+kStride);  */
/*                                    x_np1  = level->my_boxes[box].vectors[VECTOR_TEMP  ];}+ ghosts(1+jStride+kStride);} */
/*                               else{x_n    = level->my_boxes[box].vectors[VECTOR_TEMP  ];+ ghosts(1+jStride+kStride); */
/* x_nm1  = level->my_boxes[box].vectors[         x_id];+ ghosts(1+jStride+kStride);  */
/*                                    x_np1  = level->my_boxes[box].vectors[         x_id];}+ ghosts(1+jStride+kStride);} */
const double c1 = chebyshev_c1[(s%CHEBYSHEV_DEGREE)];
/* limit polynomial to degree CHEBYSHEV_DEGREE. */
const double c2 = chebyshev_c2[(s%CHEBYSHEV_DEGREE)];
/* limit polynomial to degree CHEBYSHEV_DEGREE. */
double * vectors;
vectors=level->my_boxes[box].vectors[0];
int malloc_size;
malloc_size=size*level->my_boxes[box].numVectors;
/* #pragma omp parallel for private(k,j,i) OMP_THREAD_WITHIN_A_BOX(level->threads_per_box) */
/* #pragma acc parallel loop gang copyin(x_n[0:size],c1, c2, x_nm1[0:size], rhs[0:size], beta_i[0:size], beta_j[0:size], beta_k[0:size], alpha[0:size], valid[0:size]) copy(x_np1[0:size]) */
/* #pragma acc parallel loop gang copyin(vectors[0:malloc_size],c1, c2) copy(x_np1[0:size]) */
gpuBytes=(sizeof (double)*malloc_size);
HI_malloc1D(vectors, ((void * *)( & gpu__vectors)), gpuBytes, DEFAULT_QUEUE);
HI_memcpy(gpu__vectors, vectors, gpuBytes, HI_MemcpyHostToDevice, 0);
HI_register_kernel_arg("smooth_kernel0",0,sizeof(void*),( & gpu__vectors));
HI_register_kernel_arg("smooth_kernel0",1,sizeof (double),( & a));
HI_register_kernel_arg("smooth_kernel0",2,sizeof (double),( & b));
HI_register_kernel_arg("smooth_kernel0",3,sizeof (double),((void *)( & c1)));
HI_register_kernel_arg("smooth_kernel0",4,sizeof (double),((void *)( & c2)));
HI_register_kernel_arg("smooth_kernel0",5,sizeof (int),((void *)( & dim)));
HI_register_kernel_arg("smooth_kernel0",6,sizeof (double),((void *)( & h2inv)));
HI_register_kernel_arg("smooth_kernel0",7,sizeof (int),((void *)( & jStride)));
HI_register_kernel_arg("smooth_kernel0",8,sizeof (int),((void *)( & kStride)));
HI_register_kernel_arg("smooth_kernel0",9,sizeof (int),( & rhs_id));
HI_register_kernel_arg("smooth_kernel0",10,sizeof (int),( & s));
HI_register_kernel_arg("smooth_kernel0",11,sizeof (uint64_t),((void *)( & size)));
HI_register_kernel_arg("smooth_kernel0",12,sizeof (int),( & x_id));
HI_kernel_call("smooth_kernel0",dimGrid_smooth_kernel0,dimBlock_smooth_kernel0);
HI_synchronize();
gpuNumBlocks=dim;
gpuBytes=(sizeof (double)*malloc_size);
HI_memcpy(vectors, gpu__vectors, gpuBytes, HI_MemcpyDeviceToHost, 0);
HI_free(vectors, DEFAULT_QUEUE);
}
/* box-loop */
level->cycles.smooth+=((uint64_t)(CycleTime()-_timeStart));
}
/* s-loop */
//printf("/***********************/ \n/* Input Configuration */ \n/***********************/ \n");
//printf("====> Default Number of Workers per Gang: 64 \n");
//printf("/**********************/ \n/* Used Optimizations */ \n/**********************/ \n");
//printf("====> CPU-GPU Mem Transfer Opt Level: 2\n");
//printf("====> GPU Malloc Opt Level: 0\n");
//printf("====> local array reduction variable configuration = 1\n");
//printf("====> AccPrivatization Opt. Level: 1\n");
//printf("====> AccReduction Opt. Level: 1\n");
//printf("====> AccParallelization Opt. Level: 0\n");
//printf("====> Cache shared scalar variables onto GPU shared memory.\n");
//printf("====> Cache shared scalar variables onto GPU registers.\n      (Because shrdSclrCachingOnSM is on, R/O shared scalar variables\n       are cached on shared memory, instead of registers.)\n");
//printf("====> Cache shared array elements onto GPU registers.\n");
return ;
}

