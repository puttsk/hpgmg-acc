
#ifndef __CUDA_KERNELHEADER__ 

#define __CUDA_KERNELHEADER__ 

/********************************************/
/* Added codes for OpenACC2CUDA translation */
/********************************************/
#ifdef __cplusplus
#define restrict __restrict__
#endif
#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#ifndef DBL_MAX
#define DBL_MAX 1.7976931348623158e+308
#endif
#ifndef DBL_MIN
#define DBL_MIN 2.2250738585072014e-308
#endif
#ifndef FLT_MAX
#define FLT_MAX 3.402823466e+38
#endif
#ifndef FLT_MIN
#define FLT_MIN 1.175494351e-38
#endif

#endif


typedef unsigned long int uint64_t;
extern "C" __global__ void smooth_kernel0(double * vectors, double a, double b, double c1, double c2, int dim, double h2inv, int jStride, int kStride, int rhs_id, int s, uint64_t size, int x_id)
{
int lgpriv__i;
int lgpriv__j;
int lgpriv__k;
int _ti_100_201;
lgpriv__k=(blockIdx.x+1);
#pragma acc  parallel loop num_workers(64) gang copy(vectors[0:malloc_size]) copyin(a, b, c1, c2, dim, h2inv, jStride, kStride, rhs_id, s, size, x_id) private(i, j, k) num_gangs(dim)
#pragma acc  ainfo kernelid(0) procname(smooth)
#pragma acc  cuda sharedRO(a, b, c1, c2, dim, h2inv, jStride, kStride, rhs_id, s, size, x_id) registerRO(vectors[(size*VECTOR_TEMP):1], vectors[(size*x_id):1])
if (lgpriv__k<(dim+1))
{
_ti_100_201=threadIdx.x;
#pragma acc  loop worker
{
for (lgpriv__j=(_ti_100_201+1); lgpriv__j<(dim+1); (lgpriv__j+=64))
{
for (lgpriv__i=1; lgpriv__i<(dim+1); lgpriv__i ++ )
{
double * rhs;
rhs= & vectors[(size*rhs_id)];
/* + ghosts(1+jStride+kStride); */
double * alpha;
alpha= & vectors[(size*VECTOR_ALPHA)];
/* + ghosts(1+jStride+kStride); */
double * beta_i;
beta_i= & vectors[(size*VECTOR_BETA_I)];
/* + ghosts(1+jStride+kStride); */
double * beta_j;
beta_j= & vectors[(size*VECTOR_BETA_J)];
/* + ghosts(1+jStride+kStride); */
double * beta_k;
beta_k= & vectors[(size*VECTOR_BETA_K)];
/* + ghosts(1+jStride+kStride); */
double * Dinv;
Dinv= & vectors[(size*VECTOR_DINV)];
/* + ghosts(1+jStride+kStride); */
double * valid;
valid= & vectors[(size*VECTOR_VALID)];
/* + ghosts(1+jStride+kStride); cell is inside the domain */
double * x_np1;
double * x_n;
double * x_nm1;
/* + ghosts(1+jStride+kStride);  */
if (((s&1)==0))
{
x_n=( & vectors[(size*x_id)]);
/* + ghosts(1+jStride+kStride); */
x_np1=( & vectors[(size*VECTOR_TEMP)]);
/* + ghosts(1+jStride+kStride);} */
x_nm1=( & vectors[(size*VECTOR_TEMP)]);
}
else
{
x_n=( & vectors[(size*VECTOR_TEMP)]);
/* + ghosts(1+jStride+kStride); */
x_np1=( & vectors[(size*VECTOR_TEMP)]);
/* + ghosts(1+jStride+kStride);} */
x_nm1=( & vectors[(size*x_id)]);
}
/* + ghosts(1+jStride+kStride);  */
int ijk;
ijk=(lgpriv__i+(lgpriv__j*jStride))+(lgpriv__k*kStride);
/* According to Saad... but his was missing a Dinv[ijk] == D^{-1} !!! */
/*  x_{n+1} = x_{n} + rho_{n} [ rho_{n-1}(x_{n} - x_{n-1}) + (2delta)(b-Ax_{n}) ] */
/*  x_temp[ijk] = x_n[ijk] + c1(x_n[ijk]-x_temp[ijk]) + c2*Dinv[ijk]*(rhs[ijk]-Ax_n); */
double Ax_n;
Ax_n=((a*alpha[ijk])*x_n[ijk])-((b*h2inv)*((((((( + beta_i[ijk])*((valid[(ijk-1)]*(x_n[ijk]+x_n[(ijk-1)]))-(2.0*x_n[ijk])))+(beta_j[ijk]*((valid[(ijk-jStride)]*(x_n[ijk]+x_n[(ijk-jStride)]))-(2.0*x_n[ijk]))))+(beta_k[ijk]*((valid[(ijk-kStride)]*(x_n[ijk]+x_n[(ijk-kStride)]))-(2.0*x_n[ijk]))))+(beta_i[(ijk+1)]*((valid[(ijk+1)]*(x_n[ijk]+x_n[(ijk+1)]))-(2.0*x_n[ijk]))))+(beta_j[(ijk+jStride)]*((valid[(ijk+jStride)]*(x_n[ijk]+x_n[(ijk+jStride)]))-(2.0*x_n[ijk]))))+(beta_k[(ijk+kStride)]*((valid[(ijk+kStride)]*(x_n[ijk]+x_n[(ijk+kStride)]))-(2.0*x_n[ijk])))));
double lambda;
lambda=1.0/((a*alpha[ijk])-((b*h2inv)*((((((( + beta_i[ijk])*(valid[(ijk-1)]-2.0))+(beta_j[ijk]*(valid[(ijk-jStride)]-2.0)))+(beta_k[ijk]*(valid[(ijk-kStride)]-2.0)))+(beta_i[(ijk+1)]*(valid[(ijk+1)]-2.0)))+(beta_j[(ijk+jStride)]*(valid[(ijk+jStride)]-2.0)))+(beta_k[(ijk+kStride)]*(valid[(ijk+kStride)]-2.0)))));
x_np1[ijk]=((x_n[ijk]+(c1*(x_n[ijk]-x_nm1[ijk])))+((c2*lambda)*(rhs[ijk]-Ax_n)));
}
}
}
}
}

