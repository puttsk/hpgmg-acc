/* ------------------------------------------------------------------------------------------------------------------------------ */
/* Copyright Notice  */
/* ------------------------------------------------------------------------------------------------------------------------------ */
/* HPGMG, Copyright (c) 2014, The Regents of the University of */
/* California, through Lawrence Berkeley National Laboratory (subject to */
/* receipt of any required approvals from the U.S. Dept. of Energy).  All */
/* rights reserved. */
/*  */
/* If you have questions about your rights to use or distribute this */
/* software, please contact Berkeley Lab's Technology Transfer Department */
/* at  TTD@lbl.gov. */
/*  */
/* NOTICE.  This software is owned by the U.S. Department of Energy.  As */
/* such, the U.S. Government has been granted for itself and others */
/* acting on its behalf a paid-up, nonexclusive, irrevocable, worldwide */
/* license in the Software to reproduce, prepare derivative works, and */
/* perform publicly and display publicly.  Beginning five (5) years after */
/* the date permission to assert copyright is obtained from the U.S. */
/* Department of Energy, and subject to any subsequent five (5) year */
/* renewals, the U.S. Government is granted for itself and others acting */
/* on its behalf a paid-up, nonexclusive, irrevocable, worldwide license */
/* in the Software to reproduce, prepare derivative works, distribute */
/* copies to the public, perform publicly and display publicly, and to */
/* permit others to do so. */
/* ------------------------------------------------------------------------------------------------------------------------------ */
/* Samuel Williams */
/* SWWilliams@lbl.gov */
/* Lawrence Berkeley National Lab */
/* ------------------------------------------------------------------------------------------------------------------------------ */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <openacc.h>
/* ------------------------------------------------------------------------------------------------------------------------------ */
#include <omp.h>
#include <mpi.h>
/* ------------------------------------------------------------------------------------------------------------------------------ */
#include "defines.h"
#include "level.h"
#include "mg.h"
#include "operators.h"
#include "solvers.h"

#ifndef __O2G_HEADER__ 

#define __O2G_HEADER__ 

/*******************************************/
/* Added codes for OpenACC2GPU translation */
/*******************************************/
#include <openacc.h>
#include <openaccrt.h>
#include <math.h>
#include <float.h>
#include <limits.h>
#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#ifdef __cplusplus
#define restrict __restrict__
#endif

/**********************************************************/
/* Maximum width of linear memory bound to texture memory */
/**********************************************************/
/* width in bytes */
#define LMAX_WIDTH    134217728
/**********************************/
/* Maximum memory pitch (in bytes)*/
/**********************************/
#define MAX_PITCH   262144
/****************************************/
/* Maximum allowed GPU global memory    */
/* (should be less than actual size ) */
/****************************************/
#define MAX_GMSIZE  1600000000
/****************************************/
/* Maximum allowed GPU shared memory    */
/****************************************/
#define MAX_SMSIZE  16384
/********************************************/
/* Maximum size of each dimension of a grid */
/********************************************/
#define MAX_GDIMENSION  65535

#define NUM_WORKERS  64


static int gpuNumThreads = NUM_WORKERS;
static int totalGpuNumThreads;
static int gpuNumBlocks;
static unsigned int gpuBytes = 0;

#endif 
/* End of __O2G_HEADER__ */



int count__createVectorCache_RN1 = 0;
size_t cacheSize__createVectorCache_RN0 = 0;
/* [DEBUG] added by Seyong */
int org_box_dim;
double * read_buffer;
uint64_t read_buffer_size;
void createVectorCache(mg_type * all_grids, int vector_id, int initcopyin)
{
int i;
double * gpu__temp;
count__createVectorCache_RN1 ++ ;
double * temp;
uint64_t tsize;
for (i=0; i<all_grids->num_levels; i ++ )
{
int j;
cacheSize__createVectorCache_RN0+=(sizeof (double *)*all_grids->levels[i]->num_my_boxes);
for (j=0; j<all_grids->levels[i]->num_my_boxes; j ++ )
{
/* acc_copyin(all_grids->levels[i]->my_boxes[j].vectors[vector_id], sizeof(double) all_grids->levels[i]->my_boxes[j].volume); */
temp=all_grids->levels[i]->my_boxes[j].vectors[vector_id];
tsize=all_grids->levels[i]->my_boxes[j].volume;
acc_create(temp, (sizeof (double)*tsize));
HI_set_async(j);
if (initcopyin)
{
if ((HI_get_device_address(temp, ((void * *)( & gpu__temp)), j)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, temp, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  update if(initcopyin) async(j) device(temp[0:tsize]) \n");
exit(1);
}
gpuBytes=(sizeof (double)*tsize);
HI_memcpy_async(gpu__temp, temp, gpuBytes, HI_MemcpyHostToDevice, 0, j);
}

cacheSize__createVectorCache_RN0+=(sizeof (double)*all_grids->levels[i]->my_boxes[j].volume);
}
}
/* printf("Cache %d: Total Data Transfer: %lf GB\n", count, cacheSize1024.0/1024.0/1024.0); */
return ;
}

void freeVectorCache(mg_type * all_grids, int vector_id)
{
int i;
int j;
for (i=0; i<all_grids->num_levels; i ++ )
{
for (j=0; j<all_grids->levels[i]->num_my_boxes; j ++ )
{
acc_delete(all_grids->levels[i]->my_boxes[j].vectors[vector_id], (sizeof (double)*all_grids->levels[i]->my_boxes[j].volume));
}
}
/* printf("passed\n"); */
return ;
}

/* ------------------------------------------------------------------------------------------------------------------------------ */
int main(int argc, char * * argv)
{
int MPI_Rank = 0;
int MPI_Tasks = 1;
int OMP_Threads = 1;
int OMP_Nested = 0;
int DEVICE_Number = 0;
int _ret_val_0 = 0;

////////////////////////////////
// CUDA Device Initialization //
////////////////////////////////

#pragma omp parallel
{
#pragma omp master
{
OMP_Threads=omp_get_num_threads();
OMP_Nested=omp_get_nested();
}
}
/* #warning Compiling for MPI... */
/* FIX... replace with ifdefs or env variables... */
int MPI_threadingModel;
MPI_threadingModel= - 1;
/* int MPI_threadingModelRequested = MPI_THREAD_SINGLE; */
/* int MPI_threadingModelRequested = MPI_THREAD_SERIALIZED; */
int MPI_threadingModelRequested;
MPI_threadingModelRequested=MPI_THREAD_FUNNELED;
/* int MPI_threadingModelRequested = MPI_THREAD_MULTIPLE; */
MPI_Init_thread(( & argc), ( & argv), MPI_threadingModelRequested, ( & MPI_threadingModel));
MPI_Comm_size(((MPI_Comm)((void *)( & ompi_mpi_comm_world))), ( & MPI_Tasks));
MPI_Comm_rank(((MPI_Comm)((void *)( & ompi_mpi_comm_world))), ( & MPI_Rank));
DEVICE_Number=(MPI_Rank%3);
acc_set_device_num(DEVICE_Number, acc_device_default);
if ((MPI_threadingModel>MPI_threadingModelRequested))
{
MPI_threadingModel=MPI_threadingModelRequested;
}
if ((MPI_Rank==0))
{
if ((MPI_threadingModelRequested==MPI_THREAD_MULTIPLE))
{
printf("Requested MPI_THREAD_MULTIPLE, ");
}
else
{
if ((MPI_threadingModelRequested==MPI_THREAD_SINGLE))
{
printf("Requested MPI_THREAD_SINGLE, ");
}
else
{
if ((MPI_threadingModelRequested==MPI_THREAD_FUNNELED))
{
printf("Requested MPI_THREAD_FUNNELED, ");
}
else
{
if ((MPI_threadingModelRequested==MPI_THREAD_SERIALIZED))
{
printf("Requested MPI_THREAD_SERIALIZED, ");
}
else
{
if ((MPI_threadingModelRequested==MPI_THREAD_MULTIPLE))
{
printf("Requested MPI_THREAD_MULTIPLE, ");
}
else
{
printf("Requested Unknown MPI Threading Model (%d), ", MPI_threadingModelRequested);
}
}
}
}
}
if ((MPI_threadingModel==MPI_THREAD_MULTIPLE))
{
printf("got MPI_THREAD_MULTIPLE\n");
}
else
{
if ((MPI_threadingModel==MPI_THREAD_SINGLE))
{
printf("got MPI_THREAD_SINGLE\n");
}
else
{
if ((MPI_threadingModel==MPI_THREAD_FUNNELED))
{
printf("got MPI_THREAD_FUNNELED\n");
}
else
{
if ((MPI_threadingModel==MPI_THREAD_SERIALIZED))
{
printf("got MPI_THREAD_SERIALIZED\n");
}
else
{
if ((MPI_threadingModel==MPI_THREAD_MULTIPLE))
{
printf("got MPI_THREAD_MULTIPLE\n");
}
else
{
printf("got Unknown MPI Threading Model (%d)\n", MPI_threadingModel);
}
}
}
}
}
fflush(stdout);
}
std::string kernel_str[24];
kernel_str[23]="RestrictBlockLocal_kernel0";
kernel_str[22]="RestrictBlockLocal_kernel1";
kernel_str[21]="RestrictBlockLocal_kernel2";
kernel_str[20]="RestrictBlockLocal_kernel3";
kernel_str[19]="RestrictBlockFromGPU_kernel0";
kernel_str[18]="RestrictBlockFromGPU_kernel1";
kernel_str[17]="RestrictBlockFromGPU_kernel2";
kernel_str[16]="RestrictBlockFromGPU_kernel3";
kernel_str[15]="residual_gpu_kernel0";
kernel_str[14]="shift_vector_gpu_kernel0";
kernel_str[13]="mean_gpu_kernel0";
kernel_str[12]="norm_gpu_kernel0";
kernel_str[11]="mul_vectors_gpu_kernel0";
kernel_str[10]="scale_vector_gpu_kernel0";
kernel_str[9]="zero_vector_gpu_kernel0";
kernel_str[8]="smooth_gpu_kernel0";
kernel_str[7]="CopyBlockLocal_kernel3";
kernel_str[6]="CopyBlockLocal_kernel2";
kernel_str[5]="CopyBlockLocal_kernel1";
kernel_str[4]="CopyBlockLocal_kernel0";
kernel_str[3]="CopyBlockToGPU_kernel3";
kernel_str[2]="CopyBlockToGPU_kernel2";
kernel_str[1]="CopyBlockToGPU_kernel1";
kernel_str[0]="CopyBlockToGPU_kernel0";;
acc_init(acc_device_default, 24, kernel_str);
int log2_box_dim = 6;
int target_boxes_per_rank = 1;
if ((argc==3))
{
log2_box_dim=atoi(argv[1]);
target_boxes_per_rank=atoi(argv[2]);
}
else
{
if ((MPI_Rank==0))
{
printf("usage: ./a.out  [log2_box_dim]  [target_boxes_per_rank]\n");
}
MPI_Finalize();
exit(0);
}
if ((log2_box_dim<4))
{
if ((MPI_Rank==0))
{
printf("log2_box_dim must be at least 4\n");
}
MPI_Finalize();
exit(0);
}
if ((target_boxes_per_rank<1))
{
if ((MPI_Rank==0))
{
printf("target_boxes_per_rank must be at least 1\n");
}
MPI_Finalize();
exit(0);
}
if ((MPI_Rank==0))
{
if (OMP_Nested)
{
printf("%d MPI Tasks of %d threads (OMP_NESTED=TRUE)\n\n", MPI_Tasks, OMP_Threads);
}
else
{
printf("%d MPI Tasks of %d threads (OMP_NESTED=FALSE)\n\n", MPI_Tasks, OMP_Threads);
}
}
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -   */
/* calculate the problem size... */
int box_dim;
box_dim=1<<log2_box_dim;
int target_boxes;
target_boxes=target_boxes_per_rank*MPI_Tasks;
int boxes_in_i = 1000;
/* FIX, int64_t?  could we really have >2e9 boxes? */
int total_boxes;
total_boxes=(boxes_in_i*boxes_in_i)*boxes_in_i;
org_box_dim=box_dim;
while (total_boxes>target_boxes)
{
boxes_in_i -- ;
total_boxes=((boxes_in_i*boxes_in_i)*boxes_in_i);
}
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
/* create the fine level... */
int ghosts = 1;
level_type fine_grid;
/* create_level(&fine_grid,boxes_in_i,box_dim,ghosts,VECTORS_RESERVED,BC_PERIODIC ,MPI_Rank,MPI_Tasks);double h0=1.0( (double)boxes_in_i*(double)box_dim );double a=2.0;double b=1.0; Helmholtz w/Periodic */
/* create_level(&fine_grid,boxes_in_i,box_dim,ghosts,VECTORS_RESERVED,BC_PERIODIC ,MPI_Rank,MPI_Tasks);double h0=1.0( (double)boxes_in_i*(double)box_dim );double a=0.0;double b=1.0;   Poisson w/Periodic */
/* create_level(&fine_grid,boxes_in_i,box_dim,ghosts,VECTORS_RESERVED,BC_DIRICHLET,MPI_Rank,MPI_Tasks);double h0=1.0( (double)boxes_in_i*(double)box_dim );double a=2.0;double b=1.0; Helmholtz w/Dirichlet */
/* total number of grids and the starting location for any auxillary bottom solver grids */
create_level(( & fine_grid), boxes_in_i, box_dim, ghosts, 12, 1, MPI_Rank, MPI_Tasks);
double h0;
h0=1.0/(((double)boxes_in_i)*((double)box_dim));
double a = 0.0;
double b = 1.0;
/*   Poisson wDirichlet */
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
initialize_problem(( & fine_grid), h0, a, b);
rebuild_operator(( & fine_grid), 0, a, b);
/* i.e. calculate Dinv and lambda_max */
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
mg_type all_grids;
int minCoarseDim = 1;
MGBuild(( & all_grids), ( & fine_grid), a, b, minCoarseDim);
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
int doTiming;
for (doTiming=0; doTiming<=1; doTiming ++ )
{
/* first pass warms up, second times */
MGResetTimers(( & all_grids));
/* cell centered coefficient */
createVectorCache(( & all_grids), 5, 1);
/* face centered coefficient (n.b. element 0 is the left face of the ghost zone element) */
createVectorCache(( & all_grids), 6, 1);
/* face centered coefficient (n.b. element 0 is the back face of the ghost zone element) */
createVectorCache(( & all_grids), 7, 1);
/* face centered coefficient (n.b. element 0 is the bottom face of the ghost zone element) */
createVectorCache(( & all_grids), 8, 1);
/* cell centered array noting which cells are actually present */
createVectorCache(( & all_grids), 11, 1);
/* numerical solution */
createVectorCache(( & all_grids), 4, 1);
/*  */
createVectorCache(( & all_grids), 0, 1);
/* Allocate a read_buffer for both CPU and GPU to be used in blockCopy(). */
read_buffer_size=(((sizeof (double)*org_box_dim)*org_box_dim)*org_box_dim);
read_buffer=((double *)malloc(read_buffer_size));
acc_create(read_buffer, read_buffer_size);
/* [DEBUG] Below caches are used to port the whole computations. */
/* original right-hand side (Au=f), cell centered */
createVectorCache(( & all_grids), 3, 1);
/* cell centered residual (f-Av) */
createVectorCache(( & all_grids), 2, 0);
/* cell centered relaxation parameter (e.g. inverse of the diagonal) */
createVectorCache(( & all_grids), 9, 1);
acc_wait_all();
int trial;
for (trial=0; trial<10; trial ++ )
{
/* numerical solution */
zero_vector_gpu(all_grids.levels[0], 4);
/* numerical solution */
/* original right-hand side (Au=f), cell centered */
FMGSolve(( & all_grids), 4, 3, a, b, 1.0E-15);
}
/* cell centered coefficient */
freeVectorCache(( & all_grids), 5);
/* face centered coefficient (n.b. element 0 is the left face of the ghost zone element) */
freeVectorCache(( & all_grids), 6);
/* face centered coefficient (n.b. element 0 is the back face of the ghost zone element) */
freeVectorCache(( & all_grids), 7);
/* face centered coefficient (n.b. element 0 is the bottom face of the ghost zone element) */
freeVectorCache(( & all_grids), 8);
/* cell centered array noting which cells are actually present */
freeVectorCache(( & all_grids), 11);
/* numerical solution */
freeVectorCache(( & all_grids), 4);
/*  */
freeVectorCache(( & all_grids), 0);
/* original right-hand side (Au=f), cell centered */
freeVectorCache(( & all_grids), 3);
/* cell centered residual (f-Av) */
freeVectorCache(( & all_grids), 2);
/* cell centered relaxation parameter (e.g. inverse of the diagonal) */
freeVectorCache(( & all_grids), 9);
}
MGPrintTiming(( & all_grids));
/* don't include the error check in the timing results */
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
if ((MPI_Rank==0))
{
printf("calculating error...\n");
}
/* numerical solution */
/* exact solution used to generate f */
double fine_error;
fine_error=error(( & fine_grid), 4, 1);
if ((MPI_Rank==0))
{
printf(" h = %22.15e  ||error|| = %22.15e\n\n", h0, fine_error);
fflush(stdout);
}
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
/* MGDestroy() */
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
acc_shutdown(acc_device_default);
MPI_Finalize();
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
_ret_val_0=0;
printf("/***********************/ \n/* Input Configuration */ \n/***********************/ \n");
printf("====> Default Number of Workers per Gang: 64 \n");
printf("/**********************/ \n/* Used Optimizations */ \n/**********************/ \n");
printf("====> CPU-GPU Mem Transfer Opt Level: 2\n");
printf("====> GPU Malloc Opt Level: 0\n");
printf("====> Do not remove unused symbols or procedures.\n");
printf("====> local array reduction variable configuration = 1\n");
printf("====> AccPrivatization Opt. Level: 1\n");
printf("====> AccReduction Opt. Level: 1\n");
printf("====> AccParallelization Opt. Level: 0\n");
printf("====> Cache shared scalar variables onto GPU shared memory.\n");
printf("====> Cache shared scalar variables onto GPU registers.\n      (Because shrdSclrCachingOnSM is on, R/O shared scalar variables\n       are cached on shared memory, instead of registers.)\n");
return _ret_val_0;
}

