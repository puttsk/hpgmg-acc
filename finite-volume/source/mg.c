/* ------------------------------------------------------------------------------------------------------------------------------ */
/* Samuel Williams */
/* SWWilliams@lbl.gov */
/* Lawrence Berkeley National Lab */
/* ------------------------------------------------------------------------------------------------------------------------------ */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
/* ------------------------------------------------------------------------------------------------------------------------------ */
#include <mpi.h>
#include <omp.h>
/* ------------------------------------------------------------------------------------------------------------------------------ */
#include "timer.h"
#include "defines.h"
#include "level.h"
#include "operators.h"
#include "solvers.h"
#include "mg.h"
#include "openacc.h"

#ifndef __O2G_HEADER__ 

#define __O2G_HEADER__ 

/*******************************************/
/* Added codes for OpenACC2GPU translation */
/*******************************************/
#include <openacc.h>
#include <openaccrt.h>
#include <math.h>
#include <float.h>
#include <limits.h>
#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#ifdef __cplusplus
#define restrict __restrict__
#endif

/**********************************************************/
/* Maximum width of linear memory bound to texture memory */
/**********************************************************/
/* width in bytes */
#define LMAX_WIDTH    134217728
/**********************************/
/* Maximum memory pitch (in bytes)*/
/**********************************/
#define MAX_PITCH   262144
/****************************************/
/* Maximum allowed GPU global memory    */
/* (should be less than actual size ) */
/****************************************/
#define MAX_GMSIZE  1600000000
/****************************************/
/* Maximum allowed GPU shared memory    */
/****************************************/
#define MAX_SMSIZE  16384
/********************************************/
/* Maximum size of each dimension of a grid */
/********************************************/
#define MAX_GDIMENSION  65535

#define NUM_WORKERS  64


static int gpuNumThreads = NUM_WORKERS;
static int totalGpuNumThreads;
static int gpuNumBlocks;
static unsigned int gpuBytes = 0;

#endif 
/* End of __O2G_HEADER__ */



/* ------------------------------------------------------------------------------------------------------------------------------ */
struct named_mg_acc_c_48146
{
int sendRank;
int sendBoxID;
int sendBox;
int recvRank;
int recvBoxID;
int recvBox;
int i;
int j;
int k;
};

/* offsets used to index into the coarse box */
typedef struct named_mg_acc_c_48146 RP_type;
int qsortRP(const void * a, const void * b)
{
RP_type * rpa;
int _ret_val_0;
rpa=(RP_type *)a;
RP_type * rpb;
rpb=(RP_type *)b;
/* sort first by sendRank */
if ((rpa->sendRank<rpb->sendRank))
{
_ret_val_0=( - 1);
return _ret_val_0;
}
if ((rpa->sendRank>rpb->sendRank))
{
_ret_val_0=1;
return _ret_val_0;
}
/* then by sendBoxID */
if ((rpa->sendBoxID<rpb->sendBoxID))
{
_ret_val_0=( - 1);
return _ret_val_0;
}
if ((rpa->sendBoxID>rpb->sendBoxID))
{
_ret_val_0=1;
return _ret_val_0;
}
_ret_val_0=0;
return _ret_val_0;
}

/* ---------------------------------------------------------------------------------------------------------------------------------------------------- */
void MGPrintTiming(mg_type * all_grids)
{
int level;
int num_levels;
num_levels=all_grids->num_levels;
uint64_t _timeStart;
_timeStart=CycleTime();
sleep(1);
uint64_t _timeEnd;
_timeEnd=CycleTime();
double SecondsPerCycle;
SecondsPerCycle=((double)1.0)/((double)(_timeEnd-_timeStart));
double scale;
scale=SecondsPerCycle/((double)all_grids->MGSolves_performed);
/* prints average performance per MGSolve */
if ((all_grids->my_rank!=0))
{
return ;
}
double _this;
double total;
printf("                          ");
for (level=0; level<num_levels; level ++ )
{
printf("%12d ", level);
}
printf("\n");
printf("v-cycles initiated        ");
for (level=0; level<num_levels; level ++ )
{
printf("%12d ", (all_grids->levels[level]->vcycles_from_this_level/all_grids->MGSolves_performed));
}
printf("\n");
printf("box dimension             ");
for (level=0; level<num_levels; level ++ )
{
printf("%10d^3 ", all_grids->levels[level]->box_dim);
}
printf("       total\n");
total=0;
printf("------------------        ");
for (level=0; level<(num_levels+1); level ++ )
{
printf("------------ ");
}
printf("\n");
total=0;
printf("smooth                    ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.smooth));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
total=0;
printf("residual                  ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.residual));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
total=0;
printf("applyOp                   ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.apply_op));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
total=0;
printf("BLAS1                     ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.blas1));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
total=0;
printf("BLAS3                     ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.blas3));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
total=0;
printf("Boundary Conditions       ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.boundary_conditions));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
total=0;
printf("Restriction               ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.restriction_total));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
total=0;
printf("  local restriction       ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.restriction_local));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
total=0;
printf("  pack MPI buffers        ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.restriction_pack));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
total=0;
printf("  unpack MPI buffers      ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.restriction_unpack));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
total=0;
printf("  MPI_Isend               ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.restriction_send));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
total=0;
printf("  MPI_Irecv               ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.restriction_recv));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
total=0;
printf("  MPI_Waitall             ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.restriction_wait));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
total=0;
printf("Interpolation             ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.interpolation_total));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
total=0;
printf("  local interpolation     ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.interpolation_local));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
total=0;
printf("  pack MPI buffers        ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.interpolation_pack));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
total=0;
printf("  unpack MPI buffers      ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.interpolation_unpack));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
total=0;
printf("  MPI_Isend               ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.interpolation_send));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
total=0;
printf("  MPI_Irecv               ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.interpolation_recv));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
total=0;
printf("  MPI_Waitall             ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.interpolation_wait));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
total=0;
printf("Ghost Zone Exchange       ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.ghostZone_total));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
total=0;
printf("  local exchange          ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.ghostZone_local));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
total=0;
printf("  pack MPI buffers        ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.ghostZone_pack));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
total=0;
printf("  unpack MPI buffers      ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.ghostZone_unpack));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
total=0;
printf("  MPI_Isend               ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.ghostZone_send));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
total=0;
printf("  MPI_Irecv               ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.ghostZone_recv));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
total=0;
printf("  MPI_Waitall             ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.ghostZone_wait));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
total=0;
printf("MPI_collectives           ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.collectives));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
total=0;
printf("------------------        ");
for (level=0; level<(num_levels+1); level ++ )
{
printf("------------ ");
}
printf("\n");
total=0;
printf("Total by level            ");
for (level=0; level<num_levels; level ++ )
{
_this=(scale*((double)all_grids->levels[level]->cycles.Total));
total+=_this;
printf("%12.6f ", _this);
}
printf("%12.6f\n", total);
printf("\n");
printf("   Total time in MGBuild  %12.6f seconds\n", (SecondsPerCycle*((double)all_grids->cycles.MGBuild)));
printf("   Total time in MGSolve  %12.6f seconds\n", (scale*((double)all_grids->cycles.MGSolve)));
printf("      number of v-cycles  %12d\n", (all_grids->levels[0]->vcycles_from_this_level/all_grids->MGSolves_performed));
printf("Bottom solver iterations  %12d\n", (all_grids->levels[(num_levels-1)]->Krylov_iterations/all_grids->MGSolves_performed));
printf("\n");
double numDOF;
numDOF=(((double)all_grids->levels[0]->dim.i)*((double)all_grids->levels[0]->dim.j))*((double)all_grids->levels[0]->dim.k);
printf("            Performance   %12.3e DOF/s\n", (numDOF/(scale*((double)all_grids->cycles.MGSolve))));
printf("\n\n");
fflush(stdout);
return ;
}

/* ---------------------------------------------------------------------------------------------------------------------------------------------------- */
void MGResetTimers(mg_type * all_grids)
{
int level;
for (level=0; level<all_grids->num_levels; level ++ )
{
reset_level_timers(all_grids->levels[level]);
}
/* all_grids->cycles.MGBuild     = 0; */
all_grids->cycles.MGSolve=0;
all_grids->MGSolves_performed=0;
return ;
}

/* ---------------------------------------------------------------------------------------------------------------------------------------------------- */
void build_interpolation(mg_type * all_grids)
{
int level;
for (level=0; level<all_grids->num_levels; level ++ )
{
all_grids->levels[level]->interpolation.num_recvs=0;
all_grids->levels[level]->interpolation.num_sends=0;
all_grids->levels[level]->interpolation.blocks[0]=0;
all_grids->levels[level]->interpolation.blocks[1]=0;
all_grids->levels[level]->interpolation.blocks[2]=0;
all_grids->levels[level]->interpolation.num_blocks[0]=0;
all_grids->levels[level]->interpolation.num_blocks[1]=0;
all_grids->levels[level]->interpolation.num_blocks[2]=0;
all_grids->levels[level]->interpolation.allocated_blocks[0]=0;
all_grids->levels[level]->interpolation.allocated_blocks[1]=0;
all_grids->levels[level]->interpolation.allocated_blocks[2]=0;
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
/* construct pack, send(to level-1), and local... */
if (((level>0)&&(all_grids->levels[level]->num_my_boxes>0)))
{
/* not topand*  I have boxes to send */
/* construct a list of fine boxes to be coarsened and sent to me... */
int numFineBoxes;
numFineBoxes=(((all_grids->levels[(level-1)]->boxes_in.i/all_grids->levels[level]->boxes_in.i)*(all_grids->levels[(level-1)]->boxes_in.j/all_grids->levels[level]->boxes_in.j))*(all_grids->levels[(level-1)]->boxes_in.k/all_grids->levels[level]->boxes_in.k))*all_grids->levels[level]->num_my_boxes;
int * fineRanks;
fineRanks=(int *)malloc((numFineBoxes*sizeof (int)));
/* high water mark (assumes every neighboring box is a different process) */
RP_type * fineBoxes;
fineBoxes=(RP_type *)malloc((numFineBoxes*sizeof (RP_type)));
numFineBoxes=0;
int numFineBoxesLocal = 0;
int numFineBoxesRemote = 0;
int coarseBox;
for (coarseBox=0; coarseBox<all_grids->levels[level]->num_my_boxes; coarseBox ++ )
{
int bi;
int bj;
int bk;
int coarseBoxID;
coarseBoxID=all_grids->levels[level]->my_boxes[coarseBox].global_box_id;
int coarseBox_i;
coarseBox_i=all_grids->levels[level]->my_boxes[coarseBox].low.i/all_grids->levels[level]->box_dim;
int coarseBox_j;
coarseBox_j=all_grids->levels[level]->my_boxes[coarseBox].low.j/all_grids->levels[level]->box_dim;
int coarseBox_k;
coarseBox_k=all_grids->levels[level]->my_boxes[coarseBox].low.k/all_grids->levels[level]->box_dim;
for (bk=0; bk<(all_grids->levels[(level-1)]->boxes_in.k/all_grids->levels[level]->boxes_in.k); bk ++ )
{
for (bj=0; bj<(all_grids->levels[(level-1)]->boxes_in.j/all_grids->levels[level]->boxes_in.j); bj ++ )
{
for (bi=0; bi<(all_grids->levels[(level-1)]->boxes_in.i/all_grids->levels[level]->boxes_in.i); bi ++ )
{
int fineBox_i;
fineBox_i=((all_grids->levels[(level-1)]->boxes_in.i/all_grids->levels[level]->boxes_in.i)*coarseBox_i)+bi;
int fineBox_j;
fineBox_j=((all_grids->levels[(level-1)]->boxes_in.j/all_grids->levels[level]->boxes_in.j)*coarseBox_j)+bj;
int fineBox_k;
fineBox_k=((all_grids->levels[(level-1)]->boxes_in.k/all_grids->levels[level]->boxes_in.k)*coarseBox_k)+bk;
int fineBoxID;
fineBoxID=(fineBox_i+(fineBox_j*all_grids->levels[(level-1)]->boxes_in.i))+((fineBox_k*all_grids->levels[(level-1)]->boxes_in.i)*all_grids->levels[(level-1)]->boxes_in.j);
int fineBox;
fineBox= - 1;
int f;
for (f=0; f<all_grids->levels[(level-1)]->num_my_boxes; f ++ )
{
if ((all_grids->levels[(level-1)]->my_boxes[f].global_box_id==fineBoxID))
{
fineBox=f;
}
}
/* try and find the index of a fineBox global_box_id == fineBoxID */
fineBoxes[numFineBoxes].sendRank=all_grids->levels[level]->rank_of_box[coarseBoxID];
fineBoxes[numFineBoxes].sendBoxID=coarseBoxID;
fineBoxes[numFineBoxes].sendBox=coarseBox;
fineBoxes[numFineBoxes].recvRank=all_grids->levels[(level-1)]->rank_of_box[fineBoxID];
fineBoxes[numFineBoxes].recvBoxID=fineBoxID;
fineBoxes[numFineBoxes].recvBox=fineBox;
fineBoxes[numFineBoxes].i=((bi*all_grids->levels[(level-1)]->box_dim)/2);
fineBoxes[numFineBoxes].j=((bj*all_grids->levels[(level-1)]->box_dim)/2);
fineBoxes[numFineBoxes].k=((bk*all_grids->levels[(level-1)]->box_dim)/2);
numFineBoxes ++ ;
if ((all_grids->levels[(level-1)]->rank_of_box[fineBoxID]!=all_grids->levels[level]->my_rank))
{
fineRanks[(numFineBoxesRemote ++ )]=all_grids->levels[(level-1)]->rank_of_box[fineBoxID];
}
else
{
numFineBoxesLocal ++ ;
}
}
}
}
}
/* my (coarse) boxes */
/* sort boxes by sendRank(==my rank) then by sendBoxID... ensures the sends and receive buffers are always sorted by sendBoxID... */
qsort(fineBoxes, numFineBoxes, sizeof (RP_type), qsortRP);
/* sort the lists of neighboring ranks and remove duplicates... */
qsort(fineRanks, numFineBoxesRemote, sizeof (int), qsortInt);
int numFineRanks = 0;
int _rank;
_rank= - 1;
int neighbor = 0;
for (neighbor=0; neighbor<numFineBoxesRemote; neighbor ++ )
{
if ((fineRanks[neighbor]!=_rank))
{
_rank=fineRanks[neighbor];
fineRanks[(numFineRanks ++ )]=fineRanks[neighbor];
}
}
/* allocate structures... */
all_grids->levels[level]->interpolation.num_sends=numFineRanks;
all_grids->levels[level]->interpolation.send_ranks=((int *)malloc((numFineRanks*sizeof (int))));
all_grids->levels[level]->interpolation.send_sizes=((int *)malloc((numFineRanks*sizeof (int))));
all_grids->levels[level]->interpolation.send_buffers=((double * *)malloc((numFineRanks*sizeof (double *))));
if ((numFineRanks>0))
{
if ((all_grids->levels[level]->interpolation.send_ranks==0))
{
printf("malloc failed - all_grids->levels[%d]->interpolation.send_ranks\n", level);
fflush(stdout);
exit(0);
}
if ((all_grids->levels[level]->interpolation.send_sizes==0))
{
printf("malloc failed - all_grids->levels[%d]->interpolation.send_sizes\n", level);
fflush(stdout);
exit(0);
}
if ((all_grids->levels[level]->interpolation.send_buffers==0))
{
printf("malloc failed - all_grids->levels[%d]->interpolation.send_buffers\n", level);
fflush(stdout);
exit(0);
}
}
int elementSize;
elementSize=(all_grids->levels[(level-1)]->box_dim*all_grids->levels[(level-1)]->box_dim)*all_grids->levels[(level-1)]->box_dim;
double * all_send_buffers;
all_send_buffers=(double *)malloc(((numFineBoxesRemote*elementSize)*sizeof (double)));
if (((numFineBoxesRemote*elementSize)>0))
{
if ((all_send_buffers==0))
{
printf("malloc failed - interpolation/all_send_buffers\n");
fflush(stdout);
exit(0);
}
}
memset(all_send_buffers, 0, ((numFineBoxesRemote*elementSize)*sizeof (double)));
/* DO NOT DELETE... you must initialize to 0 to avoid getting something like 0.0NaN and corrupting the solve */
/* printf("level=%d, rank=%2d, send_buffers=%6d\n",level,all_grids->my_rank,numFineBoxesRemoteelementSize*sizeof(double)); */
/* for each neighbor, construct the pack list and allocate the MPI send buffer...  */
for (neighbor=0; neighbor<numFineRanks; neighbor ++ )
{
int fineBox;
int offset = 0;
all_grids->levels[level]->interpolation.send_buffers[neighbor]=all_send_buffers;
for (fineBox=0; fineBox<numFineBoxes; fineBox ++ )
{
if ((fineBoxes[fineBox].recvRank==fineRanks[neighbor]))
{
/* pack the MPI send buffer... */
/* dim.i         = */
/* dim.j         = */
/* dim.k         = */
/* read.box      = */
/* read.ptr      = */
/* read.i        = */
/* read.j        = */
/* read.k        = */
/* read.jStride  = */
/* read.kStride  = */
/* read.scale    = */
/* write.box     = */
/* write.ptr     = */
/* write.i       = */
/* write.j       = */
/* write.k       = */
/* write.jStride = */
/* write.kStride = */
/* write.scale   = */
append_block_to_list(( & all_grids->levels[level]->interpolation.blocks[0]), ( & all_grids->levels[level]->interpolation.allocated_blocks[0]), ( & all_grids->levels[level]->interpolation.num_blocks[0]), (all_grids->levels[(level-1)]->box_dim/2), (all_grids->levels[(level-1)]->box_dim/2), (all_grids->levels[(level-1)]->box_dim/2), fineBoxes[fineBox].sendBox, 0, fineBoxes[fineBox].i, fineBoxes[fineBox].j, fineBoxes[fineBox].k, all_grids->levels[level]->my_boxes[fineBoxes[fineBox].sendBox].jStride, all_grids->levels[level]->my_boxes[fineBoxes[fineBox].sendBox].kStride, 1, ( - 1), all_grids->levels[level]->interpolation.send_buffers[neighbor], offset, 0, 0, all_grids->levels[(level-1)]->box_dim, (all_grids->levels[(level-1)]->box_dim*all_grids->levels[(level-1)]->box_dim), 2);
offset+=elementSize;
}
}
all_grids->levels[level]->interpolation.send_ranks[neighbor]=fineRanks[neighbor];
all_grids->levels[level]->interpolation.send_sizes[neighbor]=offset;
all_send_buffers+=offset;
}
/* neighbor */
{
int fineBox;
for (fineBox=0; fineBox<numFineBoxes; fineBox ++ )
{
if ((fineBoxes[fineBox].recvRank==all_grids->my_rank))
{
/* local interpolations... */
/* dim.i         = */
/* dim.j         = */
/* dim.k         = */
/* read.box      = */
/* read.ptr      = */
/* read.i        = */
/* read.j        = */
/* read.k        = */
/* read.jStride  = */
/* read.kStride  = */
/* read.scale    = */
/* write.box     = */
/* write.ptr     = */
/* write.i       = */
/* write.j       = */
/* write.k       = */
/* write.jStride = */
/* write.kStride = */
/* write.scale   = */
append_block_to_list(( & all_grids->levels[level]->interpolation.blocks[1]), ( & all_grids->levels[level]->interpolation.allocated_blocks[1]), ( & all_grids->levels[level]->interpolation.num_blocks[1]), (all_grids->levels[(level-1)]->box_dim/2), (all_grids->levels[(level-1)]->box_dim/2), (all_grids->levels[(level-1)]->box_dim/2), fineBoxes[fineBox].sendBox, 0, fineBoxes[fineBox].i, fineBoxes[fineBox].j, fineBoxes[fineBox].k, all_grids->levels[level]->my_boxes[fineBoxes[fineBox].sendBox].jStride, all_grids->levels[level]->my_boxes[fineBoxes[fineBox].sendBox].kStride, 1, fineBoxes[fineBox].recvBox, 0, 0, 0, 0, all_grids->levels[(level-1)]->my_boxes[fineBoxes[fineBox].recvBox].jStride, all_grids->levels[(level-1)]->my_boxes[fineBoxes[fineBox].recvBox].kStride, 2);
}
}
}
/* local to local interpolation */
/* free temporary storage... */
free(fineBoxes);
free(fineRanks);
}
/* packsend/local */
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
/* construct recv(from level+1) and unpack... */
if (((level<(all_grids->num_levels-1))&&(all_grids->levels[level]->num_my_boxes>0)))
{
/* not bottomand*  I have boxes to receive */
/* construct the list of coarsened boxes and neighboring ranks that will be interpolated and sent to me... */
int numCoarseBoxes;
numCoarseBoxes=all_grids->levels[level]->num_my_boxes;
/* I may receive a block for each of my boxes */
int * coarseRanks;
coarseRanks=(int *)malloc((numCoarseBoxes*sizeof (int)));
/* high water mark (assumes every neighboring box is a different process) */
RP_type * coarseBoxes;
coarseBoxes=(RP_type *)malloc((numCoarseBoxes*sizeof (RP_type)));
numCoarseBoxes=0;
int fineBox;
for (fineBox=0; fineBox<all_grids->levels[level]->num_my_boxes; fineBox ++ )
{
int fineBoxID;
fineBoxID=all_grids->levels[level]->my_boxes[fineBox].global_box_id;
int fineBox_i;
fineBox_i=all_grids->levels[level]->my_boxes[fineBox].low.i/all_grids->levels[level]->box_dim;
int fineBox_j;
fineBox_j=all_grids->levels[level]->my_boxes[fineBox].low.j/all_grids->levels[level]->box_dim;
int fineBox_k;
fineBox_k=all_grids->levels[level]->my_boxes[fineBox].low.k/all_grids->levels[level]->box_dim;
int coarseBox_i;
coarseBox_i=(fineBox_i*all_grids->levels[(level+1)]->boxes_in.i)/all_grids->levels[level]->boxes_in.i;
int coarseBox_j;
coarseBox_j=(fineBox_j*all_grids->levels[(level+1)]->boxes_in.j)/all_grids->levels[level]->boxes_in.j;
int coarseBox_k;
coarseBox_k=(fineBox_k*all_grids->levels[(level+1)]->boxes_in.k)/all_grids->levels[level]->boxes_in.k;
int coarseBoxID;
coarseBoxID=(coarseBox_i+(coarseBox_j*all_grids->levels[(level+1)]->boxes_in.i))+((coarseBox_k*all_grids->levels[(level+1)]->boxes_in.i)*all_grids->levels[(level+1)]->boxes_in.j);
if ((all_grids->levels[level]->my_rank!=all_grids->levels[(level+1)]->rank_of_box[coarseBoxID]))
{
coarseBoxes[numCoarseBoxes].sendRank=all_grids->levels[(level+1)]->rank_of_box[coarseBoxID];
coarseBoxes[numCoarseBoxes].sendBoxID=coarseBoxID;
coarseBoxes[numCoarseBoxes].sendBox=( - 1);
coarseBoxes[numCoarseBoxes].recvRank=all_grids->levels[level]->rank_of_box[fineBoxID];
coarseBoxes[numCoarseBoxes].recvBoxID=fineBoxID;
coarseBoxes[numCoarseBoxes].recvBox=fineBox;
coarseRanks[numCoarseBoxes]=all_grids->levels[(level+1)]->rank_of_box[coarseBoxID];
numCoarseBoxes ++ ;
}
}
/* my (fine) boxes */
/* sort boxes by sendRank(==my rank) then by sendBoxID... ensures the sends and receive buffers are always sorted by sendBoxID... */
qsort(coarseBoxes, numCoarseBoxes, sizeof (RP_type), qsortRP);
/* sort the lists of neighboring ranks and remove duplicates... */
qsort(coarseRanks, numCoarseBoxes, sizeof (int), qsortInt);
int numCoarseRanks = 0;
int _rank;
_rank= - 1;
int neighbor = 0;
for (neighbor=0; neighbor<numCoarseBoxes; neighbor ++ )
{
if ((coarseRanks[neighbor]!=_rank))
{
_rank=coarseRanks[neighbor];
coarseRanks[(numCoarseRanks ++ )]=coarseRanks[neighbor];
}
}
/* allocate structures... */
all_grids->levels[level]->interpolation.num_recvs=numCoarseRanks;
all_grids->levels[level]->interpolation.recv_ranks=((int *)malloc((numCoarseRanks*sizeof (int))));
all_grids->levels[level]->interpolation.recv_sizes=((int *)malloc((numCoarseRanks*sizeof (int))));
all_grids->levels[level]->interpolation.recv_buffers=((double * *)malloc((numCoarseRanks*sizeof (double *))));
if ((numCoarseRanks>0))
{
if ((all_grids->levels[level]->interpolation.recv_ranks==0))
{
printf("malloc failed - all_grids->levels[%d]->interpolation.recv_ranks\n", level);
fflush(stdout);
exit(0);
}
if ((all_grids->levels[level]->interpolation.recv_sizes==0))
{
printf("malloc failed - all_grids->levels[%d]->interpolation.recv_sizes\n", level);
fflush(stdout);
exit(0);
}
if ((all_grids->levels[level]->interpolation.recv_buffers==0))
{
printf("malloc failed - all_grids->levels[%d]->interpolation.recv_buffers\n", level);
fflush(stdout);
exit(0);
}
}
int elementSize;
elementSize=(all_grids->levels[level]->box_dim*all_grids->levels[level]->box_dim)*all_grids->levels[level]->box_dim;
double * all_recv_buffers;
all_recv_buffers=(double *)malloc(((numCoarseBoxes*elementSize)*sizeof (double)));
if (((numCoarseBoxes*elementSize)>0))
{
if ((all_recv_buffers==0))
{
printf("malloc failed - interpolation/all_recv_buffers\n");
fflush(stdout);
exit(0);
}
}
memset(all_recv_buffers, 0, ((numCoarseBoxes*elementSize)*sizeof (double)));
/* DO NOT DELETE... you must initialize to 0 to avoid getting something like 0.0NaN and corrupting the solve */
/* printf("level=%d, rank=%2d, recv_buffers=%6d\n",level,all_grids->my_rank,numCoarseBoxeselementSize*sizeof(double)); */
/* for each neighbor, construct the unpack list and allocate the MPI recv buffer...  */
for (neighbor=0; neighbor<numCoarseRanks; neighbor ++ )
{
int coarseBox;
int offset = 0;
all_grids->levels[level]->interpolation.recv_buffers[neighbor]=all_recv_buffers;
for (coarseBox=0; coarseBox<numCoarseBoxes; coarseBox ++ )
{
if ((coarseBoxes[coarseBox].sendRank==coarseRanks[neighbor]))
{
/* unpack MPI recv buffer... */
/* dim.i         = */
/* dim.j         = */
/* dim.k         = */
/* read.box      = */
/* read.ptr      = */
/* read.i        = */
/* read.j        = */
/* read.k        = */
/* read.jStride  = */
/* read.kStride  = */
/* read.scale    = */
/* write.box     = */
/* write.ptr     = */
/* write.i       = */
/* write.j       = */
/* write.k       = */
/* write.jStride = */
/* write.kStride = */
/* write.scale   = */
append_block_to_list(( & all_grids->levels[level]->interpolation.blocks[2]), ( & all_grids->levels[level]->interpolation.allocated_blocks[2]), ( & all_grids->levels[level]->interpolation.num_blocks[2]), all_grids->levels[level]->box_dim, all_grids->levels[level]->box_dim, all_grids->levels[level]->box_dim, ( - 1), all_grids->levels[level]->interpolation.recv_buffers[neighbor], offset, 0, 0, all_grids->levels[level]->box_dim, (all_grids->levels[level]->box_dim*all_grids->levels[level]->box_dim), 1, coarseBoxes[coarseBox].recvBox, 0, 0, 0, 0, all_grids->levels[level]->my_boxes[coarseBoxes[coarseBox].recvBox].jStride, all_grids->levels[level]->my_boxes[coarseBoxes[coarseBox].recvBox].kStride, 1);
offset+=elementSize;
}
}
all_grids->levels[level]->interpolation.recv_ranks[neighbor]=coarseRanks[neighbor];
all_grids->levels[level]->interpolation.recv_sizes[neighbor]=offset;
all_recv_buffers+=offset;
}
/* neighbor */
/* free temporary storage... */
free(coarseBoxes);
free(coarseRanks);
}
/* recvunpack */
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
/* MPI_Barrier(MPI_COMM_WORLD); */
/* if(all_grids->my_rank==0){printf("================================================================================\n");} */
/* print_communicator(0x7,all_grids->my_rank,level,&all_grids->levels[level]->interpolation); */
/* if((all_grids->my_rank==0)&&(level==all_grids->num_levels-1))print_communicator(2,all_grids->my_rank,level,&all_grids->levels[level]->interpolation); */
/* MPI_Barrier(MPI_COMM_WORLD); */
}
/* all levels */
for (level=0; level<all_grids->num_levels; level ++ )
{
/* malloc MPI requestsstatus arrays */
/* FIX, shouldn't it be the max of sends or recvs ??? */
all_grids->levels[level]->interpolation.requests=((MPI_Request *)malloc(((all_grids->levels[level]->interpolation.num_sends+all_grids->levels[level]->interpolation.num_recvs)*sizeof (MPI_Request))));
all_grids->levels[level]->interpolation.status=((MPI_Status *)malloc(((all_grids->levels[level]->interpolation.num_sends+all_grids->levels[level]->interpolation.num_recvs)*sizeof (MPI_Status))));
}
return ;
}

/* ---------------------------------------------------------------------------------------------------------------------------------------------------- */
void build_restriction(mg_type * all_grids)
{
int level;
for (level=0; level<all_grids->num_levels; level ++ )
{
all_grids->levels[level]->restriction.num_recvs=0;
all_grids->levels[level]->restriction.num_sends=0;
all_grids->levels[level]->restriction.blocks[0]=0;
all_grids->levels[level]->restriction.blocks[1]=0;
all_grids->levels[level]->restriction.blocks[2]=0;
all_grids->levels[level]->restriction.num_blocks[0]=0;
all_grids->levels[level]->restriction.num_blocks[1]=0;
all_grids->levels[level]->restriction.num_blocks[2]=0;
all_grids->levels[level]->restriction.allocated_blocks[0]=0;
all_grids->levels[level]->restriction.allocated_blocks[1]=0;
all_grids->levels[level]->restriction.allocated_blocks[2]=0;
/* all_grids->levels[level]->restriction.num_blocks[0] = number of unpackinsert operations  = number of boxes on level+1 that I don't own and restrict to  */
/* all_grids->levels[level]->restriction.num_blocks[1] = number of unpackinsert operations  = number of boxes on level+1 that I own and restrict to */
/* all_grids->levels[level]->restriction.num_blocks[2] = number of unpackinsert operations  = number of boxes on level-1 that I don't own that restrict to me */
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
/* construct pack, send, and local... */
if (((level<(all_grids->num_levels-1))&&(all_grids->levels[level]->num_my_boxes>0)))
{
/* not bottomand*  I have boxes to send */
/* construct the list of coarsened boxes and neighboring ranks... */
int numCoarseBoxes;
numCoarseBoxes=(((all_grids->levels[level]->boxes_in.i/all_grids->levels[(level+1)]->boxes_in.i)*(all_grids->levels[level]->boxes_in.j/all_grids->levels[(level+1)]->boxes_in.j))*(all_grids->levels[level]->boxes_in.k/all_grids->levels[(level+1)]->boxes_in.k))*all_grids->levels[level]->num_my_boxes;
int * coarseRanks;
coarseRanks=(int *)malloc((numCoarseBoxes*sizeof (int)));
/* high water mark (assumes every neighboring box is a different process) */
RP_type * coarseBoxes;
coarseBoxes=(RP_type *)malloc((numCoarseBoxes*sizeof (RP_type)));
numCoarseBoxes=0;
int numCoarseBoxesLocal = 0;
int numCoarseBoxesRemote = 0;
int fineBox;
for (fineBox=0; fineBox<all_grids->levels[level]->num_my_boxes; fineBox ++ )
{
int fineBoxID;
fineBoxID=all_grids->levels[level]->my_boxes[fineBox].global_box_id;
int fineBox_i;
fineBox_i=all_grids->levels[level]->my_boxes[fineBox].low.i/all_grids->levels[level]->box_dim;
int fineBox_j;
fineBox_j=all_grids->levels[level]->my_boxes[fineBox].low.j/all_grids->levels[level]->box_dim;
int fineBox_k;
fineBox_k=all_grids->levels[level]->my_boxes[fineBox].low.k/all_grids->levels[level]->box_dim;
int coarseBox_i;
coarseBox_i=(fineBox_i*all_grids->levels[(level+1)]->boxes_in.i)/all_grids->levels[level]->boxes_in.i;
int coarseBox_j;
coarseBox_j=(fineBox_j*all_grids->levels[(level+1)]->boxes_in.j)/all_grids->levels[level]->boxes_in.j;
int coarseBox_k;
coarseBox_k=(fineBox_k*all_grids->levels[(level+1)]->boxes_in.k)/all_grids->levels[level]->boxes_in.k;
int coarseBoxID;
coarseBoxID=(coarseBox_i+(coarseBox_j*all_grids->levels[(level+1)]->boxes_in.i))+((coarseBox_k*all_grids->levels[(level+1)]->boxes_in.i)*all_grids->levels[(level+1)]->boxes_in.j);
int coarseBox;
coarseBox= - 1;
int c;
for (c=0; c<all_grids->levels[(level+1)]->num_my_boxes; c ++ )
{
if ((all_grids->levels[(level+1)]->my_boxes[c].global_box_id==coarseBoxID))
{
coarseBox=c;
}
}
/* try and find the coarseBox index of a box with global_box_id == coaseBoxID */
coarseBoxes[numCoarseBoxes].sendRank=all_grids->levels[level]->rank_of_box[fineBoxID];
coarseBoxes[numCoarseBoxes].sendBoxID=fineBoxID;
coarseBoxes[numCoarseBoxes].sendBox=fineBox;
coarseBoxes[numCoarseBoxes].recvRank=all_grids->levels[(level+1)]->rank_of_box[coarseBoxID];
coarseBoxes[numCoarseBoxes].recvBoxID=coarseBoxID;
coarseBoxes[numCoarseBoxes].recvBox=coarseBox;
/* -1 if off-node */
coarseBoxes[numCoarseBoxes].i=((all_grids->levels[level]->box_dim/2)*(fineBox_i%(all_grids->levels[level]->boxes_in.i/all_grids->levels[(level+1)]->boxes_in.i)));
coarseBoxes[numCoarseBoxes].j=((all_grids->levels[level]->box_dim/2)*(fineBox_j%(all_grids->levels[level]->boxes_in.j/all_grids->levels[(level+1)]->boxes_in.j)));
coarseBoxes[numCoarseBoxes].k=((all_grids->levels[level]->box_dim/2)*(fineBox_k%(all_grids->levels[level]->boxes_in.k/all_grids->levels[(level+1)]->boxes_in.k)));
numCoarseBoxes ++ ;
if ((all_grids->levels[level]->my_rank!=all_grids->levels[(level+1)]->rank_of_box[coarseBoxID]))
{
coarseRanks[(numCoarseBoxesRemote ++ )]=all_grids->levels[(level+1)]->rank_of_box[coarseBoxID];
}
else
{
numCoarseBoxesLocal ++ ;
}
}
/* my (fine) boxes */
/* sort boxes by sendRank(==my rank) then by sendBoxID... ensures the sends and receive buffers are always sorted by sendBoxID... */
qsort(coarseBoxes, numCoarseBoxes, sizeof (RP_type), qsortRP);
/* sort the lists of neighboring ranks and remove duplicates... */
qsort(coarseRanks, numCoarseBoxesRemote, sizeof (int), qsortInt);
int numCoarseRanks = 0;
int _rank;
_rank= - 1;
int neighbor = 0;
for (neighbor=0; neighbor<numCoarseBoxesRemote; neighbor ++ )
{
if ((coarseRanks[neighbor]!=_rank))
{
_rank=coarseRanks[neighbor];
coarseRanks[(numCoarseRanks ++ )]=coarseRanks[neighbor];
}
}
/* allocate structures... */
all_grids->levels[level]->restriction.num_sends=numCoarseRanks;
all_grids->levels[level]->restriction.send_ranks=((int *)malloc((numCoarseRanks*sizeof (int))));
all_grids->levels[level]->restriction.send_sizes=((int *)malloc((numCoarseRanks*sizeof (int))));
all_grids->levels[level]->restriction.send_buffers=((double * *)malloc((numCoarseRanks*sizeof (double *))));
if ((numCoarseRanks>0))
{
if ((all_grids->levels[level]->restriction.send_ranks==0))
{
printf("malloc failed - all_grids->levels[%d]->restriction.send_ranks\n", level);
fflush(stdout);
exit(0);
}
if ((all_grids->levels[level]->restriction.send_sizes==0))
{
printf("malloc failed - all_grids->levels[%d]->restriction.send_sizes\n", level);
fflush(stdout);
exit(0);
}
if ((all_grids->levels[level]->restriction.send_buffers==0))
{
printf("malloc failed - all_grids->levels[%d]->restriction.send_buffers\n", level);
fflush(stdout);
exit(0);
}
}
int elementSize;
elementSize=((all_grids->levels[level]->box_dim*all_grids->levels[level]->box_dim)*all_grids->levels[level]->box_dim)/8;
double * all_send_buffers;
all_send_buffers=(double *)malloc(((numCoarseBoxes*elementSize)*sizeof (double)));
if (((numCoarseBoxes*elementSize)>0))
{
if ((all_send_buffers==0))
{
printf("malloc failed - restriction/all_send_buffers\n");
fflush(stdout);
exit(0);
}
}
memset(all_send_buffers, 0, ((numCoarseBoxes*elementSize)*sizeof (double)));
/* DO NOT DELETE... you must initialize to 0 to avoid getting something like 0.0NaN and corrupting the solve */
/* for each neighbor, construct the pack list and allocate the MPI send buffer...  */
for (neighbor=0; neighbor<numCoarseRanks; neighbor ++ )
{
int coarseBox;
int offset = 0;
all_grids->levels[level]->restriction.send_buffers[neighbor]=all_send_buffers;
for (coarseBox=0; coarseBox<numCoarseBoxes; coarseBox ++ )
{
if ((coarseBoxes[coarseBox].recvRank==coarseRanks[neighbor]))
{
/* restrict to MPI send buffer... */
/* dim.i         = */
/* dim.j         = */
/* dim.k         = */
/* read.box      = */
/* read.ptr      = */
/* read.i        = */
/* read.j        = */
/* read.k        = */
/* read.jStride  = */
/* read.kStride  = */
/* read.scale    = */
/* write.box     = */
/* write.ptr     = */
/* write.i       = */
/* write.j       = */
/* write.k       = */
/* write.jStride = */
/* write.kStride = */
/* write.scale   = */
append_block_to_list(( & all_grids->levels[level]->restriction.blocks[0]), ( & all_grids->levels[level]->restriction.allocated_blocks[0]), ( & all_grids->levels[level]->restriction.num_blocks[0]), (all_grids->levels[level]->box_dim/2), (all_grids->levels[level]->box_dim/2), (all_grids->levels[level]->box_dim/2), coarseBoxes[coarseBox].sendBox, 0, 0, 0, 0, all_grids->levels[level]->my_boxes[coarseBoxes[coarseBox].sendBox].jStride, all_grids->levels[level]->my_boxes[coarseBoxes[coarseBox].sendBox].kStride, 2, ( - 1), all_grids->levels[level]->restriction.send_buffers[neighbor], offset, 0, 0, (all_grids->levels[level]->box_dim/2), ((all_grids->levels[level]->box_dim*all_grids->levels[level]->box_dim)/4), 1);
offset+=elementSize;
}
}
all_grids->levels[level]->restriction.send_ranks[neighbor]=coarseRanks[neighbor];
all_grids->levels[level]->restriction.send_sizes[neighbor]=offset;
all_send_buffers+=offset;
}
/* for construct the local restriction list...  */
{
int coarseBox;
for (coarseBox=0; coarseBox<numCoarseBoxes; coarseBox ++ )
{
if ((coarseBoxes[coarseBox].recvRank==all_grids->levels[(level+1)]->my_rank))
{
/* restrict to local... */
/* dim.i         = */
/* dim.j         = */
/* dim.k         = */
/* read.box      = */
/* read.ptr      = */
/* read.i        = */
/* read.j        = */
/* read.k        = */
/* read.jStride  = */
/* read.kStride  = */
/* read.scale    = */
/* write.box     = */
/* write.ptr     = */
/* write.i       = */
/* write.j       = */
/* write.k       = */
/* write.jStride = */
/* write.kStride = */
/* write.scale   = */
append_block_to_list(( & all_grids->levels[level]->restriction.blocks[1]), ( & all_grids->levels[level]->restriction.allocated_blocks[1]), ( & all_grids->levels[level]->restriction.num_blocks[1]), (all_grids->levels[level]->box_dim/2), (all_grids->levels[level]->box_dim/2), (all_grids->levels[level]->box_dim/2), coarseBoxes[coarseBox].sendBox, 0, 0, 0, 0, all_grids->levels[level]->my_boxes[coarseBoxes[coarseBox].sendBox].jStride, all_grids->levels[level]->my_boxes[coarseBoxes[coarseBox].sendBox].kStride, 2, coarseBoxes[coarseBox].recvBox, 0, coarseBoxes[coarseBox].i, coarseBoxes[coarseBox].j, coarseBoxes[coarseBox].k, all_grids->levels[(level+1)]->my_boxes[coarseBoxes[coarseBox].recvBox].jStride, all_grids->levels[(level+1)]->my_boxes[coarseBoxes[coarseBox].recvBox].kStride, 1);
}
}
}
/* local to local */
/* free temporary storage... */
free(coarseBoxes);
free(coarseRanks);
}
/* sendpack/local */
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
/* construct recv and unpack... */
if (((level>0)&&(all_grids->levels[level]->num_my_boxes>0)))
{
/* not topand*  I have boxes to receive */
/* construct a list of fine boxes to be coarsened and sent to me... */
int numFineBoxesMax;
numFineBoxesMax=(((all_grids->levels[(level-1)]->boxes_in.i/all_grids->levels[level]->boxes_in.i)*(all_grids->levels[(level-1)]->boxes_in.j/all_grids->levels[level]->boxes_in.j))*(all_grids->levels[(level-1)]->boxes_in.k/all_grids->levels[level]->boxes_in.k))*all_grids->levels[level]->num_my_boxes;
int * fineRanks;
fineRanks=(int *)malloc((numFineBoxesMax*sizeof (int)));
/* high water mark (assumes every neighboring box is a different process) */
RP_type * fineBoxes;
fineBoxes=(RP_type *)malloc((numFineBoxesMax*sizeof (RP_type)));
int numFineBoxesRemote = 0;
int coarseBox;
for (coarseBox=0; coarseBox<all_grids->levels[level]->num_my_boxes; coarseBox ++ )
{
int bi;
int bj;
int bk;
int coarseBoxID;
coarseBoxID=all_grids->levels[level]->my_boxes[coarseBox].global_box_id;
int coarseBox_i;
coarseBox_i=all_grids->levels[level]->my_boxes[coarseBox].low.i/all_grids->levels[level]->box_dim;
int coarseBox_j;
coarseBox_j=all_grids->levels[level]->my_boxes[coarseBox].low.j/all_grids->levels[level]->box_dim;
int coarseBox_k;
coarseBox_k=all_grids->levels[level]->my_boxes[coarseBox].low.k/all_grids->levels[level]->box_dim;
for (bk=0; bk<(all_grids->levels[(level-1)]->boxes_in.k/all_grids->levels[level]->boxes_in.k); bk ++ )
{
for (bj=0; bj<(all_grids->levels[(level-1)]->boxes_in.j/all_grids->levels[level]->boxes_in.j); bj ++ )
{
for (bi=0; bi<(all_grids->levels[(level-1)]->boxes_in.i/all_grids->levels[level]->boxes_in.i); bi ++ )
{
int fineBox_i;
fineBox_i=((all_grids->levels[(level-1)]->boxes_in.i/all_grids->levels[level]->boxes_in.i)*coarseBox_i)+bi;
int fineBox_j;
fineBox_j=((all_grids->levels[(level-1)]->boxes_in.j/all_grids->levels[level]->boxes_in.j)*coarseBox_j)+bj;
int fineBox_k;
fineBox_k=((all_grids->levels[(level-1)]->boxes_in.k/all_grids->levels[level]->boxes_in.k)*coarseBox_k)+bk;
int fineBoxID;
fineBoxID=(fineBox_i+(fineBox_j*all_grids->levels[(level-1)]->boxes_in.i))+((fineBox_k*all_grids->levels[(level-1)]->boxes_in.i)*all_grids->levels[(level-1)]->boxes_in.j);
if ((all_grids->levels[(level-1)]->rank_of_box[fineBoxID]!=all_grids->levels[level]->my_rank))
{
fineBoxes[numFineBoxesRemote].sendRank=all_grids->levels[(level-1)]->rank_of_box[fineBoxID];
fineBoxes[numFineBoxesRemote].sendBoxID=fineBoxID;
fineBoxes[numFineBoxesRemote].sendBox=( - 1);
/* I don't know the off-node box index */
fineBoxes[numFineBoxesRemote].recvRank=all_grids->levels[level]->rank_of_box[coarseBoxID];
fineBoxes[numFineBoxesRemote].recvBoxID=coarseBoxID;
fineBoxes[numFineBoxesRemote].recvBox=coarseBox;
fineBoxes[numFineBoxesRemote].i=((bi*all_grids->levels[(level-1)]->box_dim)/2);
fineBoxes[numFineBoxesRemote].j=((bj*all_grids->levels[(level-1)]->box_dim)/2);
fineBoxes[numFineBoxesRemote].k=((bk*all_grids->levels[(level-1)]->box_dim)/2);
fineRanks[numFineBoxesRemote]=all_grids->levels[(level-1)]->rank_of_box[fineBoxID];
numFineBoxesRemote ++ ;
}
}
}
}
}
/* my (coarse) boxes */
/* sort boxes by sendRank(==my rank) then by sendBoxID... ensures the sends and receive buffers are always sorted by sendBoxID... */
qsort(fineBoxes, numFineBoxesRemote, sizeof (RP_type), qsortRP);
/* sort the lists of neighboring ranks and remove duplicates... */
qsort(fineRanks, numFineBoxesRemote, sizeof (int), qsortInt);
int numFineRanks = 0;
int _rank;
_rank= - 1;
int neighbor = 0;
for (neighbor=0; neighbor<numFineBoxesRemote; neighbor ++ )
{
if ((fineRanks[neighbor]!=_rank))
{
_rank=fineRanks[neighbor];
fineRanks[(numFineRanks ++ )]=fineRanks[neighbor];
}
}
/* allocate structures... */
all_grids->levels[level]->restriction.num_recvs=numFineRanks;
all_grids->levels[level]->restriction.recv_ranks=((int *)malloc((numFineRanks*sizeof (int))));
all_grids->levels[level]->restriction.recv_sizes=((int *)malloc((numFineRanks*sizeof (int))));
all_grids->levels[level]->restriction.recv_buffers=((double * *)malloc((numFineRanks*sizeof (double *))));
if ((numFineRanks>0))
{
if ((all_grids->levels[level]->restriction.recv_ranks==0))
{
printf("malloc failed - all_grids->levels[%d]->restriction.recv_ranks  \n", level);
fflush(stdout);
exit(0);
}
if ((all_grids->levels[level]->restriction.recv_sizes==0))
{
printf("malloc failed - all_grids->levels[%d]->restriction.recv_sizes  \n", level);
fflush(stdout);
exit(0);
}
if ((all_grids->levels[level]->restriction.recv_buffers==0))
{
printf("malloc failed - all_grids->levels[%d]->restriction.recv_buffers\n", level);
fflush(stdout);
exit(0);
}
}
int elementSize;
elementSize=((all_grids->levels[(level-1)]->box_dim*all_grids->levels[(level-1)]->box_dim)*all_grids->levels[(level-1)]->box_dim)/8;
double * all_recv_buffers;
all_recv_buffers=(double *)malloc(((numFineBoxesRemote*elementSize)*sizeof (double)));
if (((numFineBoxesRemote*elementSize)>0))
{
if ((all_recv_buffers==0))
{
printf("malloc failed - restriction/all_recv_buffers\n");
fflush(stdout);
exit(0);
}
}
memset(all_recv_buffers, 0, ((numFineBoxesRemote*elementSize)*sizeof (double)));
/* DO NOT DELETE... you must initialize to 0 to avoid getting something like 0.0NaN and corrupting the solve */
/* printf("level=%d, rank=%2d, recv_buffers=%6d\n",level,all_grids->my_rank,numFineBoxesRemoteelementSize*sizeof(double)); */
/* for each neighbor, construct the unpack list and allocate the MPI recv buffer...  */
for (neighbor=0; neighbor<numFineRanks; neighbor ++ )
{
int fineBox;
int offset = 0;
all_grids->levels[level]->restriction.recv_buffers[neighbor]=all_recv_buffers;
for (fineBox=0; fineBox<numFineBoxesRemote; fineBox ++ )
{
if ((fineBoxes[fineBox].sendRank==fineRanks[neighbor]))
{
/* unpack MPI recv buffer... */
/* dim.i         = */
/* dim.j         = */
/* dim.k         = */
/* read.box      = */
/* read.ptr      = */
/* read.i        = */
/* read.j        = */
/* read.k        = */
/* read.jStride  = */
/* read.kStride  = */
/* read.scale    = */
/* write.box     = */
/* write.ptr     = */
/* write.i       = */
/* write.j       = */
/* write.k       = */
/* write.jStride = */
/* write.kStride = */
/* write.scale   = */
append_block_to_list(( & all_grids->levels[level]->restriction.blocks[2]), ( & all_grids->levels[level]->restriction.allocated_blocks[2]), ( & all_grids->levels[level]->restriction.num_blocks[2]), (all_grids->levels[(level-1)]->box_dim/2), (all_grids->levels[(level-1)]->box_dim/2), (all_grids->levels[(level-1)]->box_dim/2), ( - 1), all_grids->levels[level]->restriction.recv_buffers[neighbor], offset, 0, 0, (all_grids->levels[(level-1)]->box_dim/2), ((all_grids->levels[(level-1)]->box_dim*all_grids->levels[(level-1)]->box_dim)/4), 1, fineBoxes[fineBox].recvBox, 0, fineBoxes[fineBox].i, fineBoxes[fineBox].j, fineBoxes[fineBox].k, all_grids->levels[level]->my_boxes[fineBoxes[fineBox].recvBox].jStride, all_grids->levels[level]->my_boxes[fineBoxes[fineBox].recvBox].kStride, 1);
offset+=elementSize;
}
}
all_grids->levels[level]->restriction.recv_ranks[neighbor]=fineRanks[neighbor];
all_grids->levels[level]->restriction.recv_sizes[neighbor]=offset;
all_recv_buffers+=offset;
}
/* neighbor */
/* free temporary storage... */
free(fineBoxes);
free(fineRanks);
}
/* recvunpack */
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
/* MPI_Barrier(MPI_COMM_WORLD); */
/* if(all_grids->my_rank==0){printf("================================================================================\n");} */
/* if(                         (level==all_grids->num_levels-2))print_communicator(2,all_grids->my_rank,level,&all_grids->levels[level]->restriction); */
/* if((all_grids->my_rank==0)&&(level==all_grids->num_levels-1))print_communicator(1,all_grids->my_rank,level,&all_grids->levels[level]->restriction); */
/* MPI_Barrier(MPI_COMM_WORLD); */
}
/* level loop */
for (level=0; level<all_grids->num_levels; level ++ )
{
/* malloc MPI requestsstatus arrays */
/* FIX shouldn't it be max of sends or recvs */
all_grids->levels[level]->restriction.requests=((MPI_Request *)malloc(((all_grids->levels[level]->restriction.num_sends+all_grids->levels[level]->restriction.num_recvs)*sizeof (MPI_Request))));
all_grids->levels[level]->restriction.status=((MPI_Status *)malloc(((all_grids->levels[level]->restriction.num_sends+all_grids->levels[level]->restriction.num_recvs)*sizeof (MPI_Status))));
}
return ;
}

/* ------------------------------------------------------------------------------------------------------------------------------ */
void MGBuild(mg_type * all_grids, level_type * fine_grid, double a, double b, int minCoarseGridDim)
{
int maxLevels = 100;
int nProcs[100];
int dim_i[100];
int boxes_in_i[100];
int box_dim[100];
int box_ghosts[100];
all_grids->my_rank=fine_grid->my_rank;
all_grids->cycles.MGBuild=0;
uint64_t _timeStartMGBuild;
_timeStartMGBuild=CycleTime();
/* calculate how deep we can make the v-cycle... */
int level = 1;
int coarse_dim;
coarse_dim=fine_grid->dim.i;
/* if(fine_grid->dim.j<coarse_dim)coarse_dim = fine_grid->dim.j; */
/* if(fine_grid->dim.k<coarse_dim)coarse_dim = fine_grid->dim.k; */
while ((coarse_dim>=(2*minCoarseGridDim))&&((coarse_dim&1)==0))
{
/* grid dimension is even and big enough... */
level ++ ;
coarse_dim=(coarse_dim/2);
}
if ((level<maxLevels))
{
maxLevels=level;
}
nProcs[0]=fine_grid->num_ranks;
dim_i[0]=fine_grid->dim.i;
boxes_in_i[0]=fine_grid->boxes_in.i;
box_dim[0]=fine_grid->box_dim;
box_ghosts[0]=fine_grid->box_ghosts;
/* build the list of levels... */
all_grids->levels=((level_type * *)malloc((maxLevels*sizeof (level_type *))));
if ((all_grids->levels==0))
{
printf("malloc failed - MGBuild/all_grids->levels\n");
fflush(stdout);
exit(0);
}
all_grids->num_levels=1;
all_grids->levels[0]=fine_grid;
/* build a table to guide the construction of the v-cycle... */
int doRestrict = 1;
if ((maxLevels<2))
{
doRestrict=0;
}
/* i.e. can't restrict if there is only one level !!! */
while (doRestrict)
{
level=all_grids->num_levels;
doRestrict=0;
int fine_box_dim;
fine_box_dim=box_dim[(level-1)];
int fine_nProcs;
fine_nProcs=nProcs[(level-1)];
int fine_dim_i;
fine_dim_i=dim_i[(level-1)];
int fine_boxes_in_i;
fine_boxes_in_i=boxes_in_i[(level-1)];
int stencil_radius;
stencil_radius=box_ghosts[(level-1)];
/* FIX tune the number of ghost zones... */
/* i.e. start the distributed v-cycle when boxes are smaller than 8^3 */
if ((((fine_box_dim%2)==0)&&(fine_box_dim>8)))
{
/* Boxes are too big to agglomerate */
nProcs[level]=fine_nProcs;
dim_i[level]=(fine_dim_i/2);
box_dim[level]=(fine_box_dim/2);
boxes_in_i[level]=fine_boxes_in_i;
box_ghosts[level]=stencil_radius;
doRestrict=1;
}
else
{
if (((fine_boxes_in_i%2)==0))
{
/* 8:1 box agglomeration */
nProcs[level]=fine_nProcs;
dim_i[level]=(fine_dim_i/2);
box_dim[level]=fine_box_dim;
boxes_in_i[level]=(fine_boxes_in_i/2);
box_ghosts[level]=stencil_radius;
doRestrict=1;
}
else
{
if (((coarse_dim!=1)&&(fine_dim_i==(2*coarse_dim))))
{
/* agglomerate everything */
nProcs[level]=1;
dim_i[level]=(fine_dim_i/2);
box_dim[level]=(fine_dim_i/2);
boxes_in_i[level]=1;
box_ghosts[level]=stencil_radius;
doRestrict=1;
}
else
{
if (((coarse_dim!=1)&&(fine_dim_i==(4*coarse_dim))))
{
/* restrict box dimension, and run on fewer ranks */
nProcs[level]=((coarse_dim<fine_nProcs) ? coarse_dim : fine_nProcs);
dim_i[level]=(fine_dim_i/2);
box_dim[level]=(fine_box_dim/2);
boxes_in_i[level]=fine_boxes_in_i;
box_ghosts[level]=stencil_radius;
doRestrict=1;
}
else
{
if (((coarse_dim!=1)&&(fine_dim_i==(8*coarse_dim))))
{
/* restrict box dimension, and run on fewer ranks */
nProcs[level]=(((coarse_dim*coarse_dim)<fine_nProcs) ? (coarse_dim*coarse_dim) : fine_nProcs);
dim_i[level]=(fine_dim_i/2);
box_dim[level]=(fine_box_dim/2);
boxes_in_i[level]=fine_boxes_in_i;
box_ghosts[level]=stencil_radius;
doRestrict=1;
}
else
{
if (((fine_box_dim%2)==0))
{
/* restrict box dimension, and run on the same number of ranks */
nProcs[level]=fine_nProcs;
dim_i[level]=(fine_dim_i/2);
box_dim[level]=(fine_box_dim/2);
boxes_in_i[level]=fine_boxes_in_i;
box_ghosts[level]=stencil_radius;
doRestrict=1;
}
}
}
}
}
}
if ((box_dim[level]<stencil_radius))
{
doRestrict=0;
}
if (doRestrict)
{
all_grids->num_levels ++ ;
}
}
/* if(all_grids->my_rank==0){for(level=0;level<all_grids->num_levels;level++){ */
/*  printf("level %2d: %4d^3 using %4d^3.%4d^3 spread over %4d processes\n",level,dim_i[level],boxes_in_i[level],box_dim[level],nProcs[level]); */
/* }fflush(stdout);} */
/* now build all the coarsened levels... */
for (level=1; level<all_grids->num_levels; level ++ )
{
all_grids->levels[level]=((level_type *)malloc(sizeof (level_type)));
if ((all_grids->levels[level]==0))
{
printf("malloc failed - MGBuild/doRestrict\n");
fflush(stdout);
exit(0);
}
create_level(all_grids->levels[level], boxes_in_i[level], box_dim[level], box_ghosts[level], all_grids->levels[(level-1)]->box_vectors, all_grids->levels[(level-1)]->domain_boundary_condition, all_grids->levels[(level-1)]->my_rank, nProcs[level]);
all_grids->levels[level]->h=(2.0*all_grids->levels[(level-1)]->h);
}
/* bottom solver gets extra grids... */
level=(all_grids->num_levels-1);
int box;
int numAdditionalVectors;
numAdditionalVectors=IterativeSolver_NumVectors();
all_grids->levels[level]->box_vectors+=numAdditionalVectors;
if (numAdditionalVectors)
{
for (box=0; box<all_grids->levels[level]->num_my_boxes; box ++ )
{
add_vectors_to_box((all_grids->levels[level]->my_boxes+box), numAdditionalVectors);
all_grids->levels[level]->memory_allocated+=((numAdditionalVectors*all_grids->levels[level]->my_boxes[box].volume)*sizeof (double));
}
}
/* build the restriction and interpolation communicators... */
build_restriction(all_grids);
build_interpolation(all_grids);
/* build subcommunicators... */
/* rebuild various coefficients for the operator... must occur after build_restriction !!! */
for (level=1; level<all_grids->num_levels; level ++ )
{
rebuild_operator(all_grids->levels[level], ((level>0) ? all_grids->levels[(level-1)] : 0), a, b);
}
if ((all_grids->my_rank==0))
{
printf("\n");
}
/* used for quick test for poisson */
for (level=0; level<all_grids->num_levels; level ++ )
{
/* cell centered coefficient */
/* cell centered coefficient */
all_grids->levels[level]->alpha_is_zero=(dot(all_grids->levels[level], 5, 5)==0.0);
}
all_grids->cycles.MGBuild+=((uint64_t)(CycleTime()-_timeStartMGBuild));
return ;
}

void pcreateLevelVectorCache(mg_type * all_grids, int level, int vector_id)
{
static int count = 0;
int j;

////////////////////////////////
// CUDA Device Initialization //
////////////////////////////////

double * gpu__temp;
count ++ ;
static size_t cacheSize = 0;
double * temp;
uint64_t tsize;
cacheSize+=(sizeof (double *)*all_grids->levels[level]->num_my_boxes);
for (j=0; j<all_grids->levels[level]->num_my_boxes; j ++ )
{
/* acc_copyin(all_grids->levels[level]->my_boxes[j].vectors[vector_id], sizeof(double) all_grids->levels[level]->my_boxes[j].volume); */
temp=all_grids->levels[level]->my_boxes[j].vectors[vector_id];
tsize=all_grids->levels[level]->my_boxes[j].volume;
acc_present_or_create(temp, (sizeof (double)*tsize));
HI_set_async(j);
if ((HI_get_device_address(temp, ((void * *)( & gpu__temp)), j)!=HI_success))
{
printf("[ERROR] GPU memory for the host variable, temp, does not exist. \n");
printf("Enclosing annotation: \n#pragma acc  update async(j) device(temp[0:tsize]) \n");
exit(1);
}
gpuBytes=(sizeof (double)*tsize);
HI_memcpy_async(gpu__temp, temp, gpuBytes, HI_MemcpyHostToDevice, 0, j);

cacheSize+=(sizeof (double)*all_grids->levels[level]->my_boxes[j].volume);
}
acc_wait_all();
printf("Cache %d: Total Data Transfer: %lf GB\n", count, (((cacheSize/1024.0)/1024.0)/1024.0));
return ;
}

void freeLevelVectorCache(mg_type * all_grids, int level, int vector_id)
{
int j;
for (j=0; j<all_grids->levels[level]->num_my_boxes; j ++ )
{
acc_delete(all_grids->levels[level]->my_boxes[j].vectors[vector_id], (sizeof (double)*all_grids->levels[level]->my_boxes[j].volume));
}
/* printf("passed\n"); */
return ;
}

/* ------------------------------------------------------------------------------------------------------------------------------ */
void MGVCycle(mg_type * all_grids, int e_id, int R_id, double a, double b, int level)
{
if (( ! all_grids->levels[level]->active))
{
return ;
}
uint64_t _LevelStart;
/* bottom solve... */
if ((level==(all_grids->num_levels-1)))
{
uint64_t _timeBottomStart;
_timeBottomStart=CycleTime();
IterativeSolver(all_grids->levels[level], e_id, R_id, a, b, 0.001);
all_grids->levels[level]->cycles.Total+=((uint64_t)(CycleTime()-_timeBottomStart));
return ;
}
/* down... */
_LevelStart=CycleTime();
/* pcreateLevelVectorCache(all_grids, level, R_id); */
smooth_gpu(all_grids->levels[level], e_id, R_id, a, b);
/*  */
residual_gpu(all_grids->levels[level], 0, e_id, R_id, a, b);
/*  */
restriction_gpu(all_grids->levels[(level+1)], R_id, all_grids->levels[level], 0, 0);
zero_vector_gpu(all_grids->levels[(level+1)], e_id);
all_grids->levels[level]->cycles.Total+=((uint64_t)(CycleTime()-_LevelStart));
/* recursion... */
MGVCycle(all_grids, e_id, R_id, a, b, (level+1));
/* up... */
_LevelStart=CycleTime();
interpolation_vcycle(all_grids->levels[level], e_id, 1.0, all_grids->levels[(level+1)], e_id);
smooth_gpu(all_grids->levels[level], e_id, R_id, a, b);
/* freeLevelVectorCache(all_grids, level, R_id); */
all_grids->levels[level]->cycles.Total+=((uint64_t)(CycleTime()-_LevelStart));
return ;
}

/* ------------------------------------------------------------------------------------------------------------------------------ */
void MGSolve(mg_type * all_grids, int u_id, int F_id, double a, double b, double desired_mg_norm)
{
all_grids->MGSolves_performed ++ ;
if (( ! all_grids->levels[0]->active))
{
return ;
}
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
int e_id;
e_id=u_id;
/* __u FIX */
/* cell centered residual (f-Av) */
int R_id = 2;
int v;
int maxVCycles = 20;
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
if ((all_grids->levels[0]->my_rank==0))
{
printf("MGSolve...\n");
fflush(stdout);
}
uint64_t _timeStartMGSolve;
_timeStartMGSolve=CycleTime();
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
/* make initial guess for e (=0) and setup the RHS */
/* double norm_of_r0 = norm(all_grids->levels[0],F_id); */
zero_vector_gpu(all_grids->levels[0], e_id);
/* ee = 0 */
scale_vector_gpu(all_grids->levels[0], R_id, 1.0, F_id);
/* R_id = F_id */
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
/* now do v-cycles to calculate the correction... */
for (v=0; v<maxVCycles; v ++ )
{
int level = 0;
all_grids->levels[level]->vcycles_from_this_level ++ ;
/* do the v-cycle... */
MGVCycle(all_grids, e_id, R_id, a, b, level);
/* now calculate the norm of the residual... */
uint64_t _timeStart;
_timeStart=CycleTime();
if (((all_grids->levels[level]->domain_boundary_condition==0)&&((a==0)||(all_grids->levels[level]->alpha_is_zero==1))))
{
/* Poisson with Periodic Boundary Conditions... by convention, we assume the solution sums to zero... so eliminate any constants from the solution... */
double average_value_of_e;
average_value_of_e=mean_gpu(all_grids->levels[level], e_id);
shift_vector_gpu(all_grids->levels[level], e_id, e_id, ( - average_value_of_e));
}
/*  */
residual_gpu(all_grids->levels[level], 0, e_id, F_id, a, b);
/*  */
/*  */
/* cell centered relaxation parameter (e.g. inverse of the diagonal) */
mul_vectors_gpu(all_grids->levels[level], 0, 1.0, 0, 9);
/*  Using ||D^{-1}(b-Ax)||_{inf} as convergence criteria... */
/*  */
double norm_of_residual;
norm_of_residual=norm_gpu(all_grids->levels[level], 0);
/* norm_of_residual = norm_of_residual norm_of_r0; */
uint64_t _timeNorm;
_timeNorm=CycleTime();
all_grids->levels[level]->cycles.Total+=((uint64_t)(_timeNorm-_timeStart));
if ((all_grids->levels[level]->my_rank==0))
{
printf("v-cycle=%2d, norm=%22.20f (%1.15e)\n", (v+1), norm_of_residual, norm_of_residual);
fflush(stdout);
}
if ((norm_of_residual<desired_mg_norm))
{
break;
}
}
/* maxVCycles */
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
all_grids->cycles.MGSolve+=((uint64_t)(CycleTime()-_timeStartMGSolve));
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
if ((all_grids->levels[0]->my_rank==0))
{
printf("done\n");
fflush(stdout);
}
return ;
}

/* ------------------------------------------------------------------------------------------------------------------------------ */
void FMGSolve(mg_type * all_grids, int u_id, int F_id, double a, double b, double desired_mg_norm)
{
all_grids->MGSolves_performed ++ ;
if (( ! all_grids->levels[0]->active))
{
return ;
}
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
int maxVCycles = 0;
int v;
int level;
int e_id;
e_id=u_id;
/* cell centered residual (f-Av) */
int R_id = 2;
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
if ((all_grids->levels[0]->my_rank==0))
{
printf("FMGSolve...\n");
fflush(stdout);
}
uint64_t _timeStartMGSolve;
_timeStartMGSolve=CycleTime();
for (level=0; level<all_grids->num_levels; level ++ )
{
pcreateLevelVectorCache(all_grids, level, R_id);
}
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
/* double norm_of_r0 = norm(all_grids->levels[0],F_id); */
/* initialize the RHS for the f-cycle to f... */
uint64_t _LevelStart;
_LevelStart=CycleTime();
level=0;
/* zero_vector(all_grids->levels[level],e_id);                       ee  = 0 */
scale_vector_gpu(all_grids->levels[level], R_id, 1.0, F_id);
/* R_id = F_id */
all_grids->levels[level]->cycles.Total+=((uint64_t)(CycleTime()-_LevelStart));
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
/* restrict RHS to bottom (coarsest grids) */
for (level=0; level<(all_grids->num_levels-1); level ++ )
{
uint64_t _LevelStart;
_LevelStart=CycleTime();
restriction_gpu(all_grids->levels[(level+1)], R_id, all_grids->levels[level], R_id, 0);
all_grids->levels[level]->cycles.Total+=((uint64_t)(CycleTime()-_LevelStart));
}
/* solve coarsest grid... */
uint64_t _timeBottomStart;
_timeBottomStart=CycleTime();
level=(all_grids->num_levels-1);
if ((level>0))
{
zero_vector_gpu(all_grids->levels[level], e_id);
}
/* else use whatever was the initial guess */
IterativeSolver(all_grids->levels[level], e_id, R_id, a, b, 0.001);
/* -1 == exact solution */
all_grids->levels[level]->cycles.Total+=((uint64_t)(CycleTime()-_timeBottomStart));
/* now do the F-cycle proper... */
for (level=(all_grids->num_levels-2); level>=0; level -- )
{
/* high-order interpolation */
_LevelStart=CycleTime();
interpolation_fcycle(all_grids->levels[level], e_id, 0.0, all_grids->levels[(level+1)], e_id);
all_grids->levels[level]->cycles.Total+=((uint64_t)(CycleTime()-_LevelStart));
/* v-cycle */
all_grids->levels[level]->vcycles_from_this_level ++ ;
MGVCycle(all_grids, e_id, R_id, a, b, level);
}
/* now do the post-F V-cycles */
for (v=( - 1); v<maxVCycles; v ++ )
{
int level = 0;
/* do the v-cycle... */
if ((v>=0))
{
all_grids->levels[level]->vcycles_from_this_level ++ ;
MGVCycle(all_grids, e_id, R_id, a, b, level);
}
/* now calculate the norm of the residual... */
uint64_t _timeStart;
_timeStart=CycleTime();
if (((all_grids->levels[level]->domain_boundary_condition==0)&&((a==0)||(all_grids->levels[level]->alpha_is_zero==1))))
{
/* Poisson with Periodic Boundary Conditions... by convention, we assume the solution sums to zero... so eliminate any constants from the solution... */
double average_value_of_e;
average_value_of_e=mean_gpu(all_grids->levels[level], e_id);
shift_vector_gpu(all_grids->levels[level], e_id, e_id, ( - average_value_of_e));
}
/*  */
residual_gpu(all_grids->levels[level], 0, e_id, F_id, a, b);
/*  */
/*  */
/* cell centered relaxation parameter (e.g. inverse of the diagonal) */
mul_vectors_gpu(all_grids->levels[level], 0, 1.0, 0, 9);
/*  Using ||D^{-1}(b-Ax)||_{inf} as convergence criteria... */
/*  */
double norm_of_residual;
norm_of_residual=norm_gpu(all_grids->levels[level], 0);
/* norm_of_residual = norm_of_residual norm_of_r0; */
uint64_t _timeNorm;
_timeNorm=CycleTime();
all_grids->levels[level]->cycles.Total+=((uint64_t)(_timeNorm-_timeStart));
if ((all_grids->levels[level]->my_rank==0))
{
if ((v>=0))
{
printf("v-cycle=%2d, norm=%22.20f (%1.15e)\n", (v+1), norm_of_residual, norm_of_residual);
}
else
{
printf("f-cycle,    norm=%22.20f (%1.15e)\n", norm_of_residual, norm_of_residual);
}
fflush(stdout);
}
if ((norm_of_residual<desired_mg_norm))
{
break;
}
}
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
all_grids->cycles.MGSolve+=((uint64_t)(CycleTime()-_timeStartMGSolve));
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
if ((all_grids->levels[0]->my_rank==0))
{
printf("done\n");
fflush(stdout);
}
return ;
}

