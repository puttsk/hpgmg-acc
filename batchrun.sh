#!/bin/bash

source ./batchrun.setting


PROFILER=nvprof

EXECUTABLE=hpgmg-fv
EXECUTABLE_PARAMS="7 8"
OUTPUT_DIR=output
WORK_DIR="$PWD"
VERSION_LIST=( )

if [ "$RECOMPILE_ACC" = "" ]; then
	RECOMPILE_ACC=0
fi
if [ "$OPENACC_LIBRARY" = "" ]; then
	OPENACC_LIBRARY="openaccrt_cuda"
fi
if [ "$MPIRUN" = "" ]; then
	MPIRUN=mpirun
fi

function usage()
{
    echo "./batchrun.sh"
	echo "List of options:"
    echo -e "\t-h --help"
    echo -e "\t-p --profile"
    echo -e "\t-np N"
    echo -e "\t[list of versions to test]"
    echo -e "\t-p [list of versions to test]"
    echo -e "\t-T --test-all-versions"
    echo -e "\t-c --compile"
#    echo -e "\t--environment=$ENVIRONMENT"
#    echo -e "\t--db-path=$DB_PATH"
    echo ""
}

while [ "$1" != "" ]; do
    PARAM=`echo $1 | awk -F= '{print $1}'`
    VALUE=`echo $1 | awk -F= '{print $2}'`
    case $PARAM in
        -h | --help)
            usage
            exit
            ;;
        -p | --profile)
            DO_PROFILE=1
            echo "Perform Profiling"
            ;;
        -c | --compile)
            COMPILE_ONLY=1
            echo "Compile and Exit"
            ;;
        -np)
            RUN_MPI=1
            echo "Run MPI versions"
			shift
    		NUM_MPIS=`echo $1 | awk -F= '{print $1}'`
            ;;
        -T | --test-all-versions)
            TEST_ALL_VERSIONS=1
			VERSION_LIST=( "master" "acc-base" "async-exec" "preallocate" "prefetch-readonly" "acc-opt" "acc-opt2" "baseCPU" )
            echo "Run All Versions"
            ;;
        acc-base | async-exec | preallocate | prefetch-readonly | master | acc-opt | acc-opt2 | baseCPU)
			if [ ! -n "$TEST_ALL_VERSIONS" ]; then
				VERSION_LIST=( "${VERSION_LIST[@]}" $PARAM )
			fi
			;;
        *)
            echo "ERROR: unknown parameter \"$PARAM\""
            usage
            exit 1
            ;;
    esac
    shift
done

if [ ${#VERSION_LIST[@]} -eq 0 ]; then
	VERSION_LIST=( "current" )
fi

#module load python/2.7.6
#module load python
mkdir -p $OUTPUT_DIR

prevVersion=`git rev-parse --abbrev-ref HEAD`
for VERSION in ${VERSION_LIST[@]}
do
	cd ${WORK_DIR}
	if [ "$VERSION" != "current" ]; then
    	git checkout $VERSION
	fi
	if [ "$VERSION" = "current" ]; then
		curVersion=`git rev-parse --abbrev-ref HEAD`
	else
		curVersion="$VERSION"
	fi
	echo "========== Current Version: $curVersion ============"
# Tag current development version
# git tag -a devel -m 'development version'
	mkdir -p $BIN_DIR
    rm -rf $BIN_DIR/*
	if [ "$curVersion" = "acc-base" ]; then
		KERNEL_DIR="acc_base_optimized"
	else
		KERNEL_DIR="acc_cache"
	fi 
	if [ "$RECOMPILE_ACC" = "1" ] && [ -f "./finite-volume/source/operators/${KERNEL_DIR}/O2GBuild.script" ]; then
		cd "./finite-volume/source/operators/${KERNEL_DIR}"
		./O2GBuild.script
		cd "$WORK_DIR"
		if [ "$curVersion" != "exchange-boundary-port" ]; then
			# Update chebyshev.c in master, acc-opt, acc-opt2, and acc_base versions.
    		cp ./finite-volume/source/operators/${KERNEL_DIR}/chebyshev.acc.cpp finite-volume/source/operators/chebyshev.c
		fi
		if [ -f "./finite-volume/source/operators/${KERNEL_DIR}/blockCopy.acc.cpp" ]; then
			# Update blockCopy.c in master or later version.
			#if [ "$curVersion" = "master" ]; then
    			cp ./finite-volume/source/operators/${KERNEL_DIR}/blockCopy.acc.cpp finite-volume/source/operators/blockCopy.c
			#fi
		fi
		if [ -f "./finite-volume/source/operators/${KERNEL_DIR}/misc.acc.cpp" ]; then
			# Update misc.c in master or later version.
			#if [ "$curVersion" = "master" ]; then
    			cp ./finite-volume/source/operators/${KERNEL_DIR}/misc.acc.cpp finite-volume/source/operators/misc.c
			#fi
		fi
		if [ -f "./finite-volume/source/operators/${KERNEL_DIR}/residual.acc.cpp" ]; then
			# Update residual.c in master or later version.
			#if [ "$curVersion" = "master" ]; then
    			cp ./finite-volume/source/operators/${KERNEL_DIR}/residual.acc.cpp finite-volume/source/operators/residual.c
			#fi
		fi
		if [ -f "./finite-volume/source/operators/${KERNEL_DIR}/restriction.acc.cpp" ]; then
			# Update restriction.c in master or later version.
			#if [ "$curVersion" = "master" ]; then
    			cp ./finite-volume/source/operators/${KERNEL_DIR}/restriction.acc.cpp finite-volume/source/operators/restriction.c
			#fi
		fi
		if [ -f "./finite-volume/source/O2GBuild.script" ]; then
			cd "./finite-volume/source"
			./O2GBuild.script
			cd "$WORK_DIR"
		fi
	fi

#	if [ "$curVersion" = "baseCPU" ]; then
#		echo "./configure --CC=$CC --CFLAGS=$OPENMPFLAG --LDLIBS=$OPENMPLIB"
#		./configure --CC=$CC --CFLAGS="$OPENMPFLAG" --LDLIBS="$OPENMPLIB"
#	else
#		echo "./configure --CC=$CC --CFLAGS=\"-I $OPENARC_INCLUDE -I $CUDA_INCLUDE $OPENMPFLAG\" --LDFLAGS=\"-L$OPENARC_LIB -L$CUDA_LIB\" --LDLIBS=\"-lcuda -lOpenCL -lcudart -l${OPENACC_LIBRARY} $OPENMPLIB\""
#		./configure --CC=$CC --CFLAGS="-I $OPENARC_INCLUDE -I $CUDA_INCLUDE $OPENMPFLAG" --LDFLAGS="-L$OPENARC_LIB -L$CUDA_LIB" --LDLIBS="-lcuda -lOpenCL -lcudart -l${OPENACC_LIBRARY} $OPENMPLIB"
#	fi

#	make -j3 -C build
	
	cd "./finite-volume/source"
	rm -rf *.o
	$CC -Ofast $OPENMPFLAG level.c operators.7pt.c mg.c solvers.c hpgmg.c timers.c -DUSE_MPI  -DUSE_SUBCOMM -DUSE_FCYCLES -DUSE_CHEBY -DUSE_BICGSTAB -DSTENCIL_FUSE_BC -DSTENCIL_FUSE_DINV -DDEVICES_P_NODE=1 -I$OPENARC_INCLUDE -L$OPENARC_LIB -l$OPENACC_LIBRARY  -o "${BIN_DIR}/$EXECUTABLE"
	#$CC -Ofast $OPENMPFLAG level.c operators.7pt.c mg.c solvers.c hpgmg.c timers.c -DUSE_MPI  -DUSE_SUBCOMM -DUSE_CHEBY -DUSE_BICGSTAB -DSTENCIL_FUSE_BC -DSTENCIL_FUSE_DINV -DDEVICES_P_NODE=1 -I$OPENARC_INCLUDE -L$OPENARC_LIB -l$OPENACC_LIBRARY  -o "${BIN_DIR}/$EXECUTABLE"
	cd "${WORK_DIR}"
	mkdir -p $OUTPUT_DIR/$curVersion

	cd $BIN_DIR
	if [ "$curVersion" != "baseCPU" ]; then
		if [ -f ${WORK_DIR}/finite-volume/source/operators/${KERNEL_DIR}/openarc_kernel.cu ]; then
			cp ${WORK_DIR}/finite-volume/source/operators/${KERNEL_DIR}/openarc_kernel.cu .
#			if [ -f $OPENARC_BIN/binBuilder_cuda ]; then
#				$OPENARC_BIN/binBuilder_cuda
#			fi
			nvcc -ptx "openarc_kernel.cu"
			mv "openarc_kernel.ptx" "openarc_kernel_35.ptx"
		elif [ -f ${WORK_DIR}/finite-volume/source/operators/${KERNEL_DIR}/openarc_kernel.cl ]; then
			cp ${WORK_DIR}/finite-volume/source/operators/${KERNEL_DIR}/openarc_kernel.cl .
			if [ -f $OPENARC_BIN/binBuilder_opencl ]; then
				$OPENARC_BIN/binBuilder_opencl
			fi
		fi
	fi
	cp ${WORK_DIR}/batchrun.setting ${BIN_DIR}
	cp ${WORK_DIR}/pbsBatch.bash ${BIN_DIR}
	
	if [ -n "$COMPILE_ONLY" ]; then
		exit
	fi

	if [ -n "$DO_PROFILE" ] 
	then 
    	$PROFILER ./$EXECUTABLE $EXECUTABLE_PARAMS &> ${WORK_DIR}/$OUTPUT_DIR/$curVersion/output-profile.out
	else
		if [ -n "$RUN_MPI" ]; then
    		$MPIRUN -np $NUM_MPIS ./$EXECUTABLE $EXECUTABLE_PARAMS &> ${WORK_DIR}/$OUTPUT_DIR/$curVersion/output.out
		else
    		./$EXECUTABLE $EXECUTABLE_PARAMS &> ${WORK_DIR}/$OUTPUT_DIR/$curVersion/output.out
		fi
	fi    

	cd $WORK_DIR
	if [ "$VERSION" != "current" ]; then
    	git checkout $VERSION
	fi
	echo "========== End Version: $curVersion ============"
    rm -rf bak
done
#Revert back to development tag
if [ "${VERSION_LIST[0]}" != "current" ]; then
	git checkout ${prevVersion}
fi
#Remove development tag
#git tag -d devel
exit
